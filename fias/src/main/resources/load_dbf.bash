#!/bin/bash

# Next 2 lines may be commented after first execution. An its take too long time.
#wget -c http://fias.nalog.ru/Public/Downloads/20170202/fias_dbf.rar
#unrar x fias_dbf.rar

set -e
set -o pipefail

{
# For ZIP archives we do not extract unneeded files. But for RAR (legacy) we axtract full, and then just ignore some on import stage:
ls -1 *.DBF | grep -vP '^(STEAD|ROOM|NORDOC|NDOCTYPE|D)' | \
	while read file; do
		echo "Process file [$file]"
		# By http://www.sql.ru/forum/actualutils.aspx?action=gotomsg&tid=1007115&msg=13986289
		time pgdbf -D -s CP866 "$file" | pv -trabeT -ps $( pgdbf -D -s CP866 "$file" | wc -c ) | PGPASSWORD=${POSTGRES_PASSWORD:-''} /usr/bin/psql -p "${POSTGRES_PORT:-5433}" -h  "${POSTGRES_HOST:-127.0.0.0}" -U "${POSTGRES_USERNAME:-postgres}" "${POSTGRES_DB:-fias}"
	done
} | tee $(dirname $0)/load_dbf.log
