/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.data.repository;

import java.util.UUID;

import org.springframework.stereotype.Service;

import com.taskdata.addressmaster.server.dao.data.entity.AddressObject;

/**
 * Copyright (c) 2011-2013 TaskData LLC
 */
@Service
public class AddressObjectDao extends AbstractDao<AddressObject, UUID> {

    public AddressObjectDao() {
        super();
    }

    @Override
    public AddressObject getById(UUID id) {
        return getEM().find(AddressObject.class, id);
    }

    @Override
    public void save(AddressObject entity) {
        getEM().persist(entity);
    }

}
