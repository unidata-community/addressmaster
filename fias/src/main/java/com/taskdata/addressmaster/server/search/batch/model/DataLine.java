/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.batch.model;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import lombok.ToString;
import org.apache.commons.lang.StringUtils;

/**
 * @author denis.vinnichek
 */
@ToString
public class DataLine extends CsvLine {

    private static Map<String, String> constructNamedItems(String[] lineItems, HeaderLine lineHeader) {
        Map<String, String> namedLineItems = new LinkedHashMap<>();
        for (int i = 0; i < lineItems.length; i++) {
            String columnName = lineHeader.lineItems[i];
            String columnValue = lineItems[i];
            namedLineItems.put(columnName, StringUtils.isEmpty(columnValue) ? null : columnValue.trim());
        }
        return namedLineItems;
    }

    private final HeaderLine headerLine;

    private final Map<String, String> namedLineItems;

    private DataLine(String[] lineItems, char itemsDelimiter, HeaderLine headerLine) {
        super(lineItems, itemsDelimiter);
        this.headerLine = headerLine;
        this.namedLineItems = constructNamedItems(this.lineItems, this.headerLine);
    }

    public DataLine(String[] lineItems, HeaderLine headerLine) {
        this(lineItems, DEFAULT_DELIMITER, headerLine);
    }

    public HeaderLine getHeaderLine() {
        return headerLine;
    }

    public Map<String, String> getNamedLineItems() {
        return Collections.unmodifiableMap(namedLineItems);
    }
}
