/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.search.highlight.Fragmenter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.apache.lucene.search.highlight.TokenGroup;
import org.apache.lucene.search.highlight.TokenSources;

import com.taskdata.addressmaster.common.LuceneHelper;

public class SearchInputImpl implements SearchInput {

    static public class AccumulateFormater implements Formatter {

        final private QueryScorer scorer;

        final private List<String> highlightedTerms = new ArrayList<>();

        final private List<String> allTerms = new ArrayList<>();

        public AccumulateFormater(QueryScorer scorer) {
            this.scorer = scorer;
        }

        public List<String> getAllTerms() {
            return allTerms;
        }

        public List<String> getHighlightedTerms() {
            return highlightedTerms;
        }

        @Override
        public String highlightTerm(String originalText, TokenGroup tokenGroup) {
            if (tokenGroup.getTotalScore() > 0) {
                highlightedTerms.add(originalText);
            }
            allTerms.add(originalText);
            return originalText;
        }

        public void reset() {
            allTerms.clear();
            highlightedTerms.clear();
        }
    }

    /**
     * Method is left here just to give an idea how to extract matched terms using native Lucene approach via "Highlighter"
     */
    static public void extractTerms(int i, Analyzer analyzer, IndexSearcher fObjectSearcher, SearchInput searchInput, TopDocs hits, Document doc, QueryScorer scorer) throws IOException, InvalidTokenOffsetsException {
        String inputTerms = sortAndConcat(searchInput.getMetaphonedTokens());

        AccumulateFormater accumulateFormater = new AccumulateFormater(scorer);
        Highlighter highlighter = new Highlighter(accumulateFormater, scorer);

        TokenStream stream = TokenSources.getAnyTokenStream(
            fObjectSearcher.getIndexReader(),
            hits.scoreDocs[i].doc,
            LuceneHelper.FIELD_FOBJECT_METAPHONE,
            doc,
            analyzer
        );

        String storedField = doc.get(LuceneHelper.FIELD_FOBJECT_METAPHONE);

        accumulateFormater.reset();
        Fragmenter fragmenter = new SimpleSpanFragmenter(scorer);
        highlighter.setTextFragmenter(fragmenter);
        highlighter.getBestFragments(stream, storedField, 20);

        String hitTermsString = sortAndConcat(accumulateFormater.getHighlightedTerms());
        String allTermsString = sortAndConcat(accumulateFormater.getAllTerms());
        int matchedTermsLevenstienDistance = StringUtils.getLevenshteinDistance(inputTerms, hitTermsString);
        int allTermsLevenstienDistance = StringUtils.getLevenshteinDistance(inputTerms, allTermsString);
        String resultToken = "Content [" + doc.get(LuceneHelper.FIELD_FOBJECT_CONTENT) + "] Metaphone [" + doc.get(LuceneHelper.FIELD_FOBJECT_METAPHONE) + "] Zip [" + doc.get(LuceneHelper.FIELD_FOBJECT_ZIPCODES) + ']';

    }

    static private String sortAndConcat(List<String> input) {
        Collections.sort(input);
        StringBuilder stringBuilder = new StringBuilder();
        for (String term : input) {
            stringBuilder.append(term).append(' ');
        }
        return stringBuilder.toString();
    }

    private String zipCode = null;

    final private String inputString;

    private String inputStringWithoutZip = null;

    private List<String> initialTokens = null;

    private List<String> metaphonedTokens = null;

    private Set<String> addressObjectTypes = null;

    private HouseNumberInput houseNumberInput = null;

    SearchInputImpl(String inputString) {
        this.inputString = inputString;
    }

    @Override
    public Set<String> getAddressObjectTypes() {
        return addressObjectTypes;
    }

    @Override
    public HouseNumberInput getHouseNumberInput() {
        return houseNumberInput;
    }

    @Override
    public List<String> getInitialTokens() {
        return initialTokens;
    }

    @Override
    public String getInputString() {
        return inputString;
    }

    @Override
    public String getInputStringWithoutZip() {
        return inputStringWithoutZip;
    }

    @Override
    public List<String> getMetaphonedTokens() {
        return metaphonedTokens;
    }

    @Override
    public String getNormalizedHouseNumberInput() {
        return houseNumberInput != null ? houseNumberInput.getNormalizedString() : "";
    }

    @Override
    public String getZipCode() {
        return zipCode;
    }

    public void setAddressObjectTypes(Set<String> addressObjectTypes) {
        this.addressObjectTypes = addressObjectTypes;
    }

    public void setHouseNumberInput(HouseNumberInput houseNumberInput) {
        this.houseNumberInput = houseNumberInput;
    }

    void setInitialTokens(List<String> initialTokens) {
        this.initialTokens = initialTokens;
    }

    void setInputStringWithoutZip(String inputStringWithoutZip) {
        this.inputStringWithoutZip = inputStringWithoutZip;
    }

    void setMetaphonedTokens(List<String> metaphonedTokens) {
        this.metaphonedTokens = metaphonedTokens;
    }

    void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

}
