/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.impl;

import java.util.AbstractSequentialList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.regex.Matcher;

import com.taskdata.addressmaster.common.Patterns;

public class HouseInputProcessor {

    static private enum ParserState {
        NONE,
        POTENTIAL_HOUSENUM,
        HOUSENUM,
        POTENTIAL_BUILDINGNUM,
        BUILDINGNUM,
        POTENTIAL_STRUCTURE
    }

    final private static Set<InputToken> HOUSE_PREFIXES = new HashSet<>();

    final private static Set<InputToken> BUILDING_PREFIXES = new HashSet<>();

    final private static Set<InputToken> STRUCTURE_PREFIXES = new HashSet<>();

    final private static Set<String> NUMERAL_ENDINGS = new HashSet<>();

    static {
        HOUSE_PREFIXES.add(new InputToken("д"));
        HOUSE_PREFIXES.add(new InputToken("дом"));

        BUILDING_PREFIXES.add(new InputToken("к"));
        BUILDING_PREFIXES.add(new InputToken("кор"));
        BUILDING_PREFIXES.add(new InputToken("корп"));
        BUILDING_PREFIXES.add(new InputToken("корпус"));

        STRUCTURE_PREFIXES.add(new InputToken("лит"));
        STRUCTURE_PREFIXES.add(new InputToken("литер"));
        STRUCTURE_PREFIXES.add(new InputToken("литера"));

        STRUCTURE_PREFIXES.add(new InputToken("стр"));
        STRUCTURE_PREFIXES.add(new InputToken("строение"));

        NUMERAL_ENDINGS.add("й");
        NUMERAL_ENDINGS.add("я");
    }

    static private boolean couldBeBuildingNumberToken(InputToken token) {
        Matcher matcher = Patterns.BUILDING_NUM_PATTERN.matcher(token.stringValue());
        return matcher.matches();
    }

    static private boolean couldBeStructureToken(InputToken token) {
        Matcher matcher = Patterns.structNumPattern.matcher(token.stringValue());
        return matcher.matches();
       /* int tokenLength = token.length();
        if (tokenLength <= 2) {
            String endings = String.valueOf(token.charAt(tokenLength - 1));
            return !NUMERAL_ENDINGS.contains(endings);
        }
        return false;*/
    }

    private HouseNumberInput candidate = new HouseNumberInput();

    private ParserState detectState = ParserState.NONE;

    private final AbstractSequentialList<HouseNumberInput> potentialHouseNumbers = new LinkedList<>();

    private HouseNumberInput bestGuess = null;

    final private String inputString;

    public HouseInputProcessor(String inputString) {
        this.inputString = inputString;
        extractHouseNumbers1();
    }

    private void extractHouseNumbers1() {
        HouseInputScanner scanner = new HouseInputScanner(inputString);

        while (scanner.hasNext()) {
            InputToken token = scanner.nextToken();

            switch (detectState) {
                case NONE:
                    handleState_None(token);
                    break;
                case POTENTIAL_HOUSENUM:
                    handleState_PotentialHouseNum(token);
                    break;
                case POTENTIAL_BUILDINGNUM:
                    handleState_PotentialBuildingNum(token);
                    break;
                case POTENTIAL_STRUCTURE:
                    handleState_PotentialStructure(token);
                    break;
                case HOUSENUM:
                    handleState_HouseNum(token);
                    break;
                case BUILDINGNUM:
                    handleState_BuildingNum(token);
                    break;
                default: {
                    throw new IllegalStateException("Unknown parser state: " + detectState);
                }
            }


        }

        // Let's register last candidate if any
        registerPotentialCandidate();

        // Process house number candidates
        if (!potentialHouseNumbers.isEmpty()) {
            // Let's take the very first one, as it should be the most recent
            bestGuess = potentialHouseNumbers.iterator().next();
        }
    }

    public HouseNumberInput getBestGuess() {
        return bestGuess;
    }

    private void handleState_BuildingNum(InputToken token) {
        if (token.isNumber()) { // token is a number
            candidate.setStructure(token);

            // We reached to the latest possible state, so let's reset for another potential candidate
            registerPotentialCandidate();
        } else {
            // Let's check if this NON-digit string is some prefix
            if (HOUSE_PREFIXES.contains(token)) {
                // This is a prefix for house number info
                registerPotentialCandidate();
                detectState = ParserState.POTENTIAL_HOUSENUM;
            } else if (STRUCTURE_PREFIXES.contains(token)) {
                // Let's check if this NON-digit string is a prefix for structure info
                detectState = ParserState.POTENTIAL_STRUCTURE;
            } else {
                // We potentially get structure literal, let's take into account only short literals
                if (couldBeStructureToken(token)) {
                    candidate.setStructure(token);

                    // We reached to the latest possible state, so let's reset for another potential candidate
                    registerPotentialCandidate();
                } else {

                    // Regular NON-digit string is detected after building number info, let's reset everything
                    registerPotentialCandidate();
                }
            }
        }
    }

    private void handleState_HouseNum(InputToken token) {
        if (token.isNumber()) { // token is a number
            candidate.setBuildingNumber(token);
            if (candidate.isFilled()) {
                registerPotentialCandidate();
            } else {
                detectState = ParserState.BUILDINGNUM;
            }
        } else {
            // Let's check if this NON-digit string is some prefix
            if (HOUSE_PREFIXES.contains(token)) {
                // This is a prefix for house number info
                registerPotentialCandidate();
                detectState = ParserState.POTENTIAL_HOUSENUM;
                candidate.setHouseNumberLabel(token);
            } else if (BUILDING_PREFIXES.contains(token)) {
                // This  is a prefix for building info
                detectState = ParserState.POTENTIAL_BUILDINGNUM;
                candidate.setBuildingNumberLabel(token);
            } else if (STRUCTURE_PREFIXES.contains(token)) {
                // This  is a prefix for structure info
                detectState = ParserState.POTENTIAL_STRUCTURE;
                candidate.setStructureLabel(token);
            } else {
                // We potentially get structure literal, let's take into account only short literals
                if (candidate.getStructure() == null && couldBeStructureToken(token)) {
                    candidate.setStructure(token);
                    if (candidate.isFilled()) {
                        registerPotentialCandidate();
                    }
                } else if (candidate.getBuildingNumber() == null && couldBeBuildingNumberToken(token)) {
                    candidate.setBuildingNumber(token);
                    if (candidate.isFilled()) {
                        registerPotentialCandidate();
                    }
                } else {
                    // Regular NON-digit string is detected after house number info, let's reset everything
                    registerPotentialCandidate();
                }
            }
        }
    }

    private void handleState_None(InputToken token) {
        if (token.isNumber()) { // token is a number
            candidate.setHouseNumber(token);
            detectState = ParserState.HOUSENUM;
            // We do not try to register potential candidate, as we expect building and/or structure info
        } else {
            // We have some NON-digit string and empty parser state,
            // so let's reset as we expect house information only in sequential manner
            candidate.reset();

            // Let's prepare for the next potential candidate
            if (HOUSE_PREFIXES.contains(token)) {
                detectState = ParserState.POTENTIAL_HOUSENUM;
                candidate.setHouseNumberLabel(token);
            }
        }
    }

    private void handleState_PotentialBuildingNum(InputToken token) {
        // Let's fill building string
        candidate.setBuildingNumber(token);

        if (candidate.isFilled()) {
            registerPotentialCandidate();
        } else {
            // In case address does not contain structure info we still might get it later
            detectState = ParserState.HOUSENUM;
        }
    }

    private void handleState_PotentialHouseNum(InputToken token) {
        if (token.isNumber()) { // token is a number
            candidate.setHouseNumber(token);
            detectState = ParserState.HOUSENUM;
            // We do not try to register potential candidate, as we expect building and/or structure info
        } else {
            // House number cannot be NON-numerical, so let's reset everything
            candidate.setHouseNumberLabel(null); // We need to reset house number label as it is not actually applicable
            registerPotentialCandidate();
        }
    }


    private void handleState_PotentialStructure(InputToken token) {
        // Let's fill structure string
        candidate.setStructure(token);

        if (candidate.isFilled()) {
            registerPotentialCandidate();
        } else {
            // In case address does not contain building info we still might get it later
            detectState = ParserState.HOUSENUM;
        }
    }

    private void registerPotentialCandidate() {
        detectState = ParserState.NONE;
        if (candidate.isValid()) {
            // Let's prepend candidate to make sure the most recent one comes first
            //candidate.normalize();
            potentialHouseNumbers.add(0, candidate);
            candidate = new HouseNumberInput();
        } else {
            candidate.reset();
        }
    }


}
