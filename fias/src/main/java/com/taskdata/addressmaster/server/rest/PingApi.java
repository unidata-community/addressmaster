package com.taskdata.addressmaster.server.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.taskdata.addressmaster.server.rest.pojo.Pong;

/**
 * @author Pavel Alexeev.
 * @since 2020-11-05 00:32.
 */
@Path("ping")
public class PingApi {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Pong ping() {
		return new Pong();
	}
}
