/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.taskdata.addressmaster.db.FlywayUpdater;

/**
 * do flyway migrations on app start
 *
 * @author Maria Chistyakova
 * @author Pavel Alexeev
 * @since 17.02.2017.
 */
@Slf4j
@Component("databaseVersionValidator")
public class DatabaseVersionValidator {
    @Autowired @Qualifier("dataSourceFias1")
    DataSource dataSourceFias1;

    @Autowired @Qualifier("dataSourceFias2")
    DataSource dataSourceFias2;

    @Autowired @Qualifier("dataSourceMaster")
    DataSource dataSourceMaster;

    /**
     * We must start on very early stage before any other bean initialized (they are already may use database in methods
     * annotated with @PostProcess.
     * Because that we cant just implement something like {@see org.springframework.context.ApplicationListener#onApplicationEvent(final ContextRefreshedEvent event)}
     */
    @PostConstruct
    public void updateAllSources() throws BeansException {
        String fias1 = FlywayUpdater.updateFias(dataSourceFias1);
        String fias2 = FlywayUpdater.updateFias(dataSourceFias2);
        String master = FlywayUpdater.updateMaster(dataSourceMaster);
        log.info("Database update is complete. Now versions are: master=[{}], fias1=[{}], fias2=[{}]", master, fias1, fias2);
    }
}
