/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.impl;

public class InputToken implements Comparable<InputToken> {

    final private String sourceString;

    final private int startIndex;

    final private int endIndex;

    final private String stringValueForOptimization;

    public InputToken(String sourceString) {
        this.sourceString = sourceString;
        this.startIndex = 0;
        this.endIndex = sourceString.length();
        this.stringValueForOptimization = sourceString.substring(startIndex, endIndex).toUpperCase();
    }

    public InputToken(String sourceString, int startIndex, int endIndex) {
        this.sourceString = sourceString;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.stringValueForOptimization = sourceString.substring(startIndex, endIndex).toUpperCase();
    }

    public char charAt(int index) {
        return sourceString.charAt(startIndex + index);
    }

    @Override
    public int compareTo(InputToken o) {
        return startIndex - o.startIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InputToken token = (InputToken) o;

        return stringValueForOptimization.equals(token.stringValueForOptimization);

    }

    public int getEndIndex() {
        return endIndex;
    }

    public int getStartIndex() {
        return startIndex;
    }

    @Override
    public int hashCode() {
        return stringValueForOptimization.hashCode();
    }

    public boolean isNumber() {
        return Character.isDigit(sourceString.charAt(startIndex));
    }

    public int length() {
        return endIndex - startIndex;
    }

    public String stringValue() {
        return stringValueForOptimization;
    }

    @Override
    public String toString() {
        return stringValueForOptimization;
    }
}
