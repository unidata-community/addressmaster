/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.common.JSONConstants;
import com.taskdata.addressmaster.common.model.CompositeAddressElement;

public class SearchResultItem implements JSONSerializable {

    private String searchResultItemId = null;

    private String searchResultItemGuid = null;

    private String zipCode = null;

    final private CompositeAddressElement addressInfo;

    private final HouseNumberObject houseInfo;

    public SearchResultItem(JSONObject jsonObject) {
        this.searchResultItemGuid = (String) jsonObject.get(JSONConstants.JSON_FIELD_ADDRESS_GUID);
        this.searchResultItemId = (String) jsonObject.get(JSONConstants.JSON_FIELD_ADDRESS_ID);
        this.zipCode = (String) jsonObject.get(JSONConstants.JSON_FIELD_ZIPCODE);

        this.addressInfo = new CompositeAddressElement((JSONArray) jsonObject.get(JSONConstants.JSON_FIELD_ADDRESS_INFO), true);

        JSONObject houseInfoObject = (JSONObject) jsonObject.get(JSONConstants.JSON_FIELD_HOUSE_INFO);
        if (houseInfoObject != null) {
            this.houseInfo = new HouseNumberObject(houseInfoObject);
        } else {
            this.houseInfo = null;
        }
    }

    SearchResultItem(String searchResultItemGuid, String searchResultItemId, CompositeAddressElement addressInfo, HouseNumberObject houseInfo) {
        this.searchResultItemGuid = searchResultItemGuid;
        this.searchResultItemId = searchResultItemId;
        this.addressInfo = addressInfo;
        this.zipCode = houseInfo != null ? houseInfo.getZipCode() : addressInfo.getZipCodesString();
        this.houseInfo = houseInfo;
    }

    public CompositeAddressElement getAddressInfo() {
        return addressInfo;
    }

    public HouseNumberObject getHouseInfo() {
        return houseInfo;
    }

    public String getHouseNumber() {
        return houseInfo != null ? houseInfo.getHouseNumber() : null;
    }

    public String getSearchResultItemGuid() {
        return searchResultItemGuid;
    }

    public String getSearchResultItemId() {
        return searchResultItemId;
    }

    public String getZipCode() {
        return zipCode;
    }

    public JSONAware toJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONConstants.JSON_FIELD_ADDRESS_ID, searchResultItemId);
        jsonObject.put(JSONConstants.JSON_FIELD_ADDRESS_GUID, searchResultItemGuid);
        jsonObject.put(JSONConstants.JSON_FIELD_ZIPCODE, zipCode);
        jsonObject.put(JSONConstants.JSON_FIELD_ADDRESS_INFO, addressInfo.toJSON());
        jsonObject.put(JSONConstants.JSON_FIELD_HOUSE_INFO, houseInfo != null ? houseInfo.toJSON() : null);
        return jsonObject;
    }
}
