/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.model;

import lombok.Data;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.common.JSONConstants;

/**
 * @author denis.vinnichek
 */
@Data
public class AddressDetailsObject implements JSONSerializable {

    public static class Builder {

        private String zipCode;
        private String houseNumber;
        private String buildingNumber;
        private String structure;
        private String structureLongDesc;
        private String structureShortDesc;
        private String okato;
        private String oktmo;
        private String ifnsfl;
        private String terrIfnsfl;
        private String ifnsul;
        private String fiasId;
        private String fiasGuid;
        private String kladr;

        public AddressDetailsObject build() {
            return new AddressDetailsObject(this);
        }

        public Builder setBuildingNumber(String buildingNumber) {
            this.buildingNumber = buildingNumber;
            return this;
        }

        public Builder setFiasGuid(String fiasGuid) {
            this.fiasGuid = fiasGuid;
            return this;
        }

        public Builder setFiasId(String fiasId) {
            this.fiasId = fiasId;
            return this;
        }

        public Builder setHouseNumber(String houseNumber) {
            this.houseNumber = houseNumber;
            return this;
        }

        public Builder setIfnsfl(String ifnsfl) {
            this.ifnsfl = ifnsfl;
            return this;
        }

        public Builder setIfnsul(String ifnsul) {
            this.ifnsul = ifnsul;
            return this;
        }

        public Builder setKladr(String kladr) {
            this.kladr = kladr;
            return this;
        }

        public Builder setOkato(String okato) {
            this.okato = okato;
            return this;
        }

        public Builder setOktmo(String oktmo) {
            this.oktmo = oktmo;
            return this;
        }

        public Builder setStructure(String structure) {
            this.structure = structure;
            return this;
        }

        public Builder setStructureLongDesc(String structureLongDesc) {
            this.structureLongDesc = structureLongDesc;
            return this;
        }

        public Builder setStructureShortDesc(String structureShortDesc) {
            this.structureShortDesc = structureShortDesc;
            return this;
        }

        public Builder setTerrIfnsfl(String terrIfnsfl) {
            this.terrIfnsfl = terrIfnsfl;
            return this;
        }

        public Builder setZipCode(String zipCode) {
            this.zipCode = zipCode;
            return this;
        }
    }

    final private String zipCode;
    final private String houseNumber;
    final private String buildingNumber;
    final private String structure;
    final private String structureLongDesc;
    final private String structureShortDesc;
    private final String okato;
    private final String oktmo;
    private final String ifnsfl;
    private final String terrIfnsfl;
    private final String ifnsul;
    private final String fiasId;
    private final String fiasGuid;

    private final String kladr;

    private AddressDetailsObject(Builder builder) {
        this.zipCode = builder.zipCode;
        this.houseNumber = builder.houseNumber;
        this.buildingNumber = builder.buildingNumber;
        this.structure = builder.structure;
        this.structureLongDesc = builder.structureLongDesc;
        this.structureShortDesc = builder.structureShortDesc;
        this.okato = builder.okato;
        this.oktmo = builder.oktmo;
        this.ifnsfl = builder.ifnsfl;
        this.terrIfnsfl = builder.terrIfnsfl;
        this.ifnsul = builder.ifnsul;
        this.fiasId = builder.fiasId;
        this.fiasGuid = builder.fiasGuid;
        this.kladr = builder.kladr;
    }

    public AddressDetailsObject(JSONObject jsonObject) {
        zipCode = (String) jsonObject.get(JSONConstants.JSON_FIELD_ZIPCODE);
        houseNumber = (String) jsonObject.get(JSONConstants.JSON_FIELD_HOUSE_NUMBER);
        buildingNumber = (String) jsonObject.get(JSONConstants.JSON_FIELD_BUILDING_NUMBER);
        structure = (String) jsonObject.get(JSONConstants.JSON_FIELD_STRUCTURE);
        structureLongDesc = (String) jsonObject.get(JSONConstants.JSON_FIELD_STRUCTURE_STATUS_LONG_DESC);
        structureShortDesc = (String) jsonObject.get(JSONConstants.JSON_FIELD_STRUCTURE_STATUS_SHORT_DESC);
        okato = (String) jsonObject.get(JSONConstants.JSON_FIELD_OKATO);
        oktmo = (String) jsonObject.get(JSONConstants.JSON_FIELD_OKTMO);
        ifnsfl = (String) jsonObject.get(JSONConstants.JSON_FIELD_IFNSFL);
        terrIfnsfl = (String) jsonObject.get(JSONConstants.JSON_FIELD_TERR_IFNSFL);
        ifnsul = (String) jsonObject.get(JSONConstants.JSON_FIELD_IFNSUL);
        fiasId = (String) jsonObject.get(JSONConstants.JSON_FIELD_FIAS_ID);
        fiasGuid = (String) jsonObject.get(JSONConstants.JSON_FIELD_FIAS_GUID);
        kladr = (String) jsonObject.get(JSONConstants.JSON_FIELD_KLADR);
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public String getFiasId() {
        return fiasId;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getIfnsfl() {
        return ifnsfl;
    }

    public String getIfnsul() {
        return ifnsul;
    }

    public String getKladr() {
        return kladr;
    }

    public String getOkato() {
        return okato;
    }

    public String getOktmo() {
        return oktmo;
    }

    public String getStructure() {
        return structure;
    }

    public String getStructureLongDesc() {
        return structureLongDesc;
    }

    public String getStructureShortDesc() {
        return structureShortDesc;
    }

    public String getTerrIfnsfl() {
        return terrIfnsfl;
    }

    public String getZipCode() {
        return zipCode;
    }

    @Override
    public JSONAware toJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONConstants.JSON_FIELD_ZIPCODE, zipCode);
        jsonObject.put(JSONConstants.JSON_FIELD_HOUSE_NUMBER, houseNumber);
        jsonObject.put(JSONConstants.JSON_FIELD_BUILDING_NUMBER, buildingNumber);
        jsonObject.put(JSONConstants.JSON_FIELD_STRUCTURE, structure);
        jsonObject.put(JSONConstants.JSON_FIELD_STRUCTURE_STATUS_SHORT_DESC, structureShortDesc);
        jsonObject.put(JSONConstants.JSON_FIELD_STRUCTURE_STATUS_LONG_DESC, structureLongDesc);
        jsonObject.put(JSONConstants.JSON_FIELD_OKATO, okato);
        jsonObject.put(JSONConstants.JSON_FIELD_OKTMO, oktmo);
        jsonObject.put(JSONConstants.JSON_FIELD_IFNSFL, ifnsfl);
        jsonObject.put(JSONConstants.JSON_FIELD_TERR_IFNSFL, terrIfnsfl);
        jsonObject.put(JSONConstants.JSON_FIELD_IFNSUL, ifnsul);
        jsonObject.put(JSONConstants.JSON_FIELD_FIAS_ID, fiasId);
        jsonObject.put(JSONConstants.JSON_FIELD_FIAS_GUID, fiasGuid);
        jsonObject.put(JSONConstants.JSON_FIELD_KLADR, kladr);
        return jsonObject;
    }
}
