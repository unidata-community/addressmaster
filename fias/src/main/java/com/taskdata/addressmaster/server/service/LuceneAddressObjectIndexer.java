/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import info.hubbitus.utils.bench.ProgressLogger;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.taskdata.addressmaster.common.LuceneHelper;
import com.taskdata.addressmaster.common.Patterns;
import com.taskdata.addressmaster.common.model.AddressElement;
import com.taskdata.addressmaster.common.model.CompositeAddressElement;
import com.taskdata.addressmaster.db.FiasIndexType;
import com.taskdata.addressmaster.server.service.datasource.RoutingDataSource;
import com.taskdata.addressmaster.server.service.exception.UpdateFiasDBServiceException;

/**
 * Index all text files under a directory.
 * <p/>
 * This is a command-line application demonstrating simple Lucene indexing.
 * Run it with no command-line arguments for usage information.
 */
@Slf4j
@Service
public class LuceneAddressObjectIndexer {

    @Autowired
    RoutingDataSource routingDataSource;

    /**
     * preparedStatement package size in LuceneAddressObjectIndexer - for create lucene index from db
     */
    @Value("${lucene.indexer.db.batch.fetch.size:1000}")
    private Integer indexerDbFetchBatchSize;

    /**
     * Buffer size for lucene index writer. In Mb. By default 512.
     */
    @Value("${lucene.index.writer.ram.buffer:512}")
    private Double indexWriterRAMBufferSizeMB;

    /**
     * Class to execute SQL query and scroll results
     */
    private static abstract class ResultSetProcessor {
        final ProgressLogger pl;

        final Connection connection;

        /**
         * Object to count, get and process SQL results
         *
         * @param connection JDBC connection to work on. Settings will be tuned. Will be closed at end.
         * @param itemName Name for logging progress
         * @param queryCount SQL query to count total amount of elements
         * @param queryData SQL query to retrieve data
         * @param batchSize Size of batch to process results from DB
         */
        ResultSetProcessor(Connection connection, String itemName, String queryCount, String queryData, Integer batchSize) throws SQLException, IOException {
            this.connection = connection;
            this.pl = new ProgressLogger(countTotals(queryCount), it -> log.debug(it.toString()), batchSize, itemName);

            processQueryScrolledResult(queryData, batchSize);
        }

        Long countTotals(String queryCount) throws SQLException {
            try (Statement stmt = connection.createStatement()) {
                ResultSet rs = stmt.executeQuery(queryCount);
                rs.next();
                return rs.getLong(1);
            }
        }

        /**
         * We have single pattern to do - SQL query which process row-by row. And there several things to remember:
         * 1. Result must be scrollable to do not get {@link OutOfMemoryError}
         * 2. Connection should be obtained and configured
         * 3. Logging progress into
         * <p>
         * For that it method for. You just provide row handler like row-mapper and that's all.
         *
         * @param query to execute
         * @throws SQLException in case of error. Provided AS IS
         */
        private void processQueryScrolledResult(String query, Integer batchSize) throws IOException, SQLException {
            val autoCommit = connection.getAutoCommit();
            try{
                connection.setAutoCommit(false); // https://jdbc.postgresql.org/documentation/83/query.html#fetchsize-example
                PreparedStatement preparedStatement = connection.prepareStatement(
                    query,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY
                );
                preparedStatement.setFetchSize(batchSize);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        pl.next();
                        processSingleResult(resultSet);
                    }
                }
            }
            finally {
                connection.setAutoCommit(autoCommit);
            }
        }

        abstract public void processSingleResult(ResultSet resultSet) throws IOException, SQLException;
    }

    //region SQL
    final private static String SQL_OBJECTS_BASE_PART = "FROM\n" +
        "    addrob_all finite\n" +
        "        LEFT JOIN socrbase T0 ON (finite.aolevel = T0.level AND finite.shortname = T0.scname)\n" +
        "    LEFT JOIN addrob_all P1 ON (finite.parentguid = P1.aoguid)\n" +
        "        LEFT JOIN socrbase T1 ON (P1.aolevel = T1.level AND P1.shortname = T1.scname)\n" +
        "    LEFT JOIN addrob_all P2 ON (P1.parentguid = P2.aoguid)\n" +
        "        LEFT JOIN socrbase T2 ON (P2.aolevel = T2.level AND P2.shortname = T2.scname)\n" +
        "    LEFT JOIN addrob_all P3 ON (P2.parentguid = P3.aoguid)\n" +
        "        LEFT JOIN socrbase T3 ON (P3.aolevel = T3.level AND P3.shortname = T3.scname)\n" +
        "    LEFT JOIN addrob_all P4 ON (P3.parentguid = P4.aoguid)\n" +
        "        LEFT JOIN socrbase T4 ON (P4.aolevel = T4.level AND P4.shortname = T4.scname)\n" +
        "    LEFT JOIN addrob_all P5 ON (P4.parentguid = P5.aoguid)\n" +
        "        LEFT JOIN socrbase T5 ON (P5.aolevel = T5.level AND P5.shortname = T5.scname)\n" +
        "WHERE finite.AOLEVEL >= 7";

    final private static String SQL_OBJECTS_COUNT = "SELECT COUNT(*) " + SQL_OBJECTS_BASE_PART;
    final private static String SQL_OBJECTS_SELECT = "SELECT\n" +
        "    -- n level\n" +
        "    finite.AOGUID AOGUID," +
        "    finite.AOID AOID," +
        "    finite.FORMALNAME FORMALNAME," +
        "    T0.SOCRNAME SOCRNAME," +
        "    finite.POSTALCODE POSTALCODE," +
        "    finite.aoLEVEL LEVEL0," +
        "    \n-- n-1 level\n" +
        "    P1.AOGUID AOGUID1," +
        "    P1.AOID AOID1," +
        "    P1.FORMALNAME FORMALNAME1," +
        "    T1.SOCRNAME SOCRNAME1," +
        "    P1.aoLEVEL LEVEL1," +
        "    \n-- n-2 level\n" +
        "    P2.AOGUID AOGUID2," +
        "    P2.AOID AOID2," +
        "    P2.FORMALNAME FORMALNAME2," +
        "    P2.aoLEVEL LEVEL2," +
        "    T2.SOCRNAME SOCRNAME2," +
        "    \n-- n-3 level\n" +
        "    P3.AOGUID AOGUID3," +
        "    P3.AOID AOID3," +
        "    P3.FORMALNAME FORMALNAME3," +
        "    P3.aoLEVEL LEVEL3," +
        "    T3.SOCRNAME SOCRNAME3," +
        "    \n-- n-4 level\n" +
        "    P4.AOGUID AOGUID4," +
        "    P4.AOID AOID4," +
        "    P4.FORMALNAME FORMALNAME4," +
        "    P4.aoLEVEL LEVEL4," +
        "    T4.SOCRNAME SOCRNAME4," +
        "    \n-- n-5 level\n" +
        "    P5.AOGUID AOGUID5," +
        "    P5.AOID AOID5," +
        "    P5.FORMALNAME FORMALNAME5," +
        "    P5.aoLEVEL LEVEL5," +
        "    T5.SOCRNAME SOCRNAME5" +
        "    -- _aolevel::text = _level::text  ::text - if fias change _level type (char -> numeric)\n" +
        SQL_OBJECTS_BASE_PART;


    final private static String SQL_HOUSE_BASE_PART = "FROM house_all\n" +
        "WHERE STARTDATE < CURRENT_DATE\n" +
        "  AND CURRENT_DATE < ENDDATE\n" +
        "-- ORDER BY houseid\n " +
        "--limit 1000\n" +
        ";";
    final private static String SQL_HOUSE_COUNT = "SELECT COUNT(*) " +SQL_HOUSE_BASE_PART;
    final private static String SQL_HOUSE_SELECT = "SELECT houseguid,\n" +
        "       houseid,\n" +
        "       aoguid,\n" +
        "       postalcode,\n" +
        "       housenum,\n" +
        "       buildnum,\n" +
        "       strucnum,\n" +
        "       strstatus\n" +
        SQL_HOUSE_BASE_PART;
    //endregion

    static private String getWithEmptyForNulls(String str) {
        if (str == null) {
            return "";
        }
        return str;
    }

    /**
     * copy/paste logic from @link com.taskdata.addressmaster.etl.writer.FiasCsvHandlerHouse.java
     */
    private static void indexFiasHouseDocs(IndexWriter writer, String houseGuid,
                                           String houseId,
                                           String aoGuid,
                                           String zipCode,
                                           String houseNumber,
                                           String buildingNumber,
                                           String structure,
                                           Integer structureTypeInteger) throws IOException {
        // make a new, empty document
        Document doc = new Document();

        doc.add(new StringField(LuceneHelper.FIELD_FHOUSE_GUID, houseGuid, Field.Store.YES));
        doc.add(new StringField(LuceneHelper.FIELD_FHOUSE_ID, houseId, Field.Store.YES));
        doc.add(new StringField(LuceneHelper.FIELD_FHOUSE_AOGUID, aoGuid, Field.Store.YES));
        doc.add(new StringField(LuceneHelper.FIELD_FHOUSE_ZIP, zipCode, Field.Store.YES));
        if (houseNumber != null) {
            doc.add(new StringField(LuceneHelper.FIELD_FHOUSE_NUMBER, houseNumber, Field.Store.YES));
            Matcher matcher = Patterns.digitsWithOptionalSlashPattern.matcher(houseNumber);
            if (matcher.find()) {
                String houseNamNumericalPart = matcher.group(1);
                doc.add(new StringField(LuceneHelper.FIELD_HOUSE_NUMERIC_PART, houseNamNumericalPart, Field.Store.NO));
            }
        }

        //todo 2017.03.01  buildingNumber != null cannot be null in current db
        if (buildingNumber != null && !buildingNumber.isEmpty()) {
            doc.add(new StringField(LuceneHelper.FIELD_FBUILDING_NUMBER, buildingNumber, Field.Store.YES));
        }
        if (structure != null && !structure.isEmpty()) {
            doc.add(new StringField(LuceneHelper.FIELD_FSTRUCTURE, structure, Field.Store.YES));
        }
        if (structureTypeInteger != null) {
            doc.add(new StoredField(LuceneHelper.FIELD_FSTRUCTURE_STATUS, structureTypeInteger));
        }
        String fullHouse = (getWithEmptyForNulls(houseNumber) + '_' + getWithEmptyForNulls(buildingNumber) + '_' + getWithEmptyForNulls(structure)).trim().toUpperCase();
        doc.add(new StringField(LuceneHelper.FIELD_FULL_HOUSE, fullHouse, Field.Store.YES));

        // OpenMode.CREATE
        writer.addDocument(doc);
    }

    private static void indexFiasObjectDocs(IndexWriter writer, CompositeAddressElement compositeAddressElement) throws IOException {
        // make a new, empty document
        Document doc = new Document();

        //todo 2017.03.09 mb write here FIELD_FOBJECT_LEAF_AOID = ~compositeAd*.getLeafGuid())
        doc.add(new StoredField(LuceneHelper.FIELD_FOBJECT_LEAF_AOID, compositeAddressElement.getLeafAoId()));

        doc.add(new StoredField(LuceneHelper.FIELD_FOBJECT_LEAF_GUID, compositeAddressElement.getLeafGuid()));

        String addressToIndex = compositeAddressElement.getAddressToIndex();
        addressToIndex = LuceneHelper.splitHyphens(addressToIndex);
        addressToIndex = LuceneHelper.addNumericStrings(addressToIndex);

        TextField metaphoneField = new TextField(LuceneHelper.FIELD_FOBJECT_METAPHONE, addressToIndex, Field.Store.NO);
        doc.add(metaphoneField);

        doc.add(new TextField(LuceneHelper.FIELD_FOBJECT_AOTYPE, compositeAddressElement.getAddressTypeToIndex(), Field.Store.NO));

        String zipCodesString = compositeAddressElement.getZipCodesString();
        if (zipCodesString != null) {
            doc.add(new TextField(LuceneHelper.FIELD_FOBJECT_ZIPCODES, zipCodesString, Field.Store.NO));
        }

        doc.add(new StoredField(LuceneHelper.FIELD_FOBJECT_CONTENT, compositeAddressElement.toJSON().toJSONString()));

        // writer.getConfig().getOpenMode() always IndexWriterConfig.OpenMode.CREATE
        //log.debug("adding " + streetGuid);

        writer.addDocument(doc);

    }

    private static CompositeAddressElement loadCompositeAddressElement(ResultSet resultSet) throws SQLException {
        List<AddressElement> addressObjects = new ArrayList<>();

        String guid0    = resultSet.getString("AOGUID");
        String aoid0    = resultSet.getString("AOID");
        String p0       = resultSet.getString("FORMALNAME");
        String at0      = resultSet.getString("SOCRNAME");
        int level0      = resultSet.getInt("LEVEL0");
        String zip0     = resultSet.getString("POSTALCODE");

        // ALL_POSTAL_CODES does not in new fias schema
        //String allZip0 = resultSet.getString("ALL_POSTAL_CODES");
        String allZip0  = resultSet.getString("POSTALCODE");

        addressObjects.add(new AddressElement(guid0, aoid0, p0, at0, level0, zip0, allZip0));

        String guid1 = resultSet.getString("AOGUID1");
        if (guid1 != null) {
            addressObjects.add(new AddressElement(resultSet, 1));
        }

        String guid2 = resultSet.getString("AOGUID2");
        if (guid2 != null) {
            addressObjects.add(new AddressElement(resultSet, 2));
        }

        String guid3 = resultSet.getString("AOGUID3");
        if (guid3 != null) {
            addressObjects.add(new AddressElement(resultSet, 3));
        }


        String guid4 = resultSet.getString("AOGUID4");
        if (guid4 != null) {
            addressObjects.add(new AddressElement(resultSet, 4));
        }

        String guid5 = resultSet.getString("AOGUID5");
        if (guid5 != null) {
            addressObjects.add(new AddressElement(resultSet, 5));
        }

        return new CompositeAddressElement(addressObjects);
    }

    private IndexWriter getIndexWriter(FiasIndexType fiasIndexType) throws IOException {
        Directory dir = FSDirectory.open(routingDataSource.getStaging().getLucene().getLuceneIndexDir().resolve(fiasIndexType.getRelativeIndexPath()).toFile());

        Analyzer analyzer = new WhitespaceAnalyzer(Version.LUCENE_42);
        IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_42, analyzer);

        // Create a new index in the directory, removing any
        // previously indexed documents:
        iwc.setOpenMode(OpenMode.CREATE);

        // Optional: for better indexing performance, if you are indexing many documents, increase the RAM buffer.
        // But if you do this, increase the max heap size to the JVM (eg add -Xmx512m or -Xmx1g):
        iwc.setRAMBufferSizeMB(indexWriterRAMBufferSizeMB);

        return new IndexWriter(dir, iwc);
    }

    /**
     * Index information about houses
     */
    private void produceFiasHouseIndex(IndexWriter writer) throws IOException, SQLException {
        new ResultSetProcessor(routingDataSource.getStaging().getConnection(), "House", SQL_HOUSE_COUNT, SQL_HOUSE_SELECT, indexerDbFetchBatchSize) {
            @Override
            public void processSingleResult(ResultSet resultSet) throws IOException, SQLException {
                String houseGuid = resultSet.getString("houseguid");
                String houseId = resultSet.getString("houseid");
                String aoGuid = resultSet.getString("aoguid");
                String zip = resultSet.getString("postalcode");
                if (resultSet.wasNull()) {
                    zip = "";
                }

                String houseNum = resultSet.getString("housenum");
                String buildingNum = resultSet.getString("buildnum");
                String structure = resultSet.getString("strucnum");
                String structureType = resultSet.getString("strstatus");
                Integer structureTypeInteger = null;
                if (structureType != null) {
                    structureTypeInteger = Integer.valueOf(structureType);
                }

                indexFiasHouseDocs(
                    writer, houseGuid, houseId, aoGuid, zip, houseNum,
                    buildingNum, structure, structureTypeInteger
                );
            }
        };
    }

    private void produceFiasIndex(FiasIndexType fiasIndexType) throws UpdateFiasDBServiceException {
        try (IndexWriter writer = getIndexWriter(fiasIndexType)){
            log.info("Indexing to directory [" + routingDataSource.getStaging().getLucene().getLuceneIndexDir() + '/' + fiasIndexType.getRelativeIndexPath() + "].");

            if (fiasIndexType == FiasIndexType.FOBJECT) {
                produceFiasObjectIndex(writer);
            } else if (fiasIndexType == FiasIndexType.FHOUSE) {
                produceFiasHouseIndex(writer);
            } else {
                throw new IllegalArgumentException("Unknown index type: " + fiasIndexType);
            }

            // NOTE: if you want to maximize search performance, you can optionally call forceMerge here.
            // This can be a terribly costly operation, so generally it's only worth it when your index is relatively
            // static (ie you're done adding documents to it):
            log.info("writer.forceMerge(1) '");
            writer.forceMerge(1);
        } catch (Exception e) {
            String error = "Error run produceFiasIndex(" + fiasIndexType + ") with exception: " + e.getMessage();
            log.error(error, e);
            throw new UpdateFiasDBServiceException(error, e);
        }
    }

    /**
     * Stage into Lucene main data of objects from table addrobj_all. That self joined 7 times to produce wide address representation
     */
    private void produceFiasObjectIndex(IndexWriter writer) throws IOException, SQLException {
        new ResultSetProcessor(routingDataSource.getStaging().getConnection(), "AddrOb", SQL_OBJECTS_COUNT, SQL_OBJECTS_SELECT, indexerDbFetchBatchSize) {
            @Override
            public void processSingleResult(ResultSet resultSet) throws IOException, SQLException {
                CompositeAddressElement compositeAddressElementToIndex = loadCompositeAddressElement(resultSet);
                indexFiasObjectDocs(writer, compositeAddressElementToIndex);
            }
        };
    }

    /**
     * Index all text files under a directory.
     */
    void readIndexFromDB() throws UpdateFiasDBServiceException {
        long startTime = System.currentTimeMillis();

        log.info("start FiasIndexType.FOBJECT");
        produceFiasIndex(FiasIndexType.FOBJECT);
        log.info("end FiasIndexType.FOBJECT");
        log.info("start FiasIndexType.FHOUSE");
        produceFiasIndex(FiasIndexType.FHOUSE);
        log.info("end FiasIndexType.FHOUSE");

        long endTime = System.currentTimeMillis();
        log.info("It took [" + (endTime - startTime) / 1000 + "] seconds to process");
    }

    void reCreateLuceneIndex(Path indexBasePath) throws IOException {
        log.info("reCreateLuceneIndex({})", indexBasePath.toAbsolutePath());
        removeDirectoryRecursive(indexBasePath);
        createLuceneIndex(indexBasePath);
    }

    /**
     * Create directory and lucene index if base directory does not exists
     * @see #createLuceneIndex(Path)
     *
     * @param indexBasePath Directory to check existence
     */
    public void createIndexIfDirectoryMissing(Path indexBasePath) throws IOException {
        if (!Files.exists(indexBasePath)){
            createLuceneIndex(indexBasePath);
        }
    }

    /**
     * Creates structure of Lucene index. It will be empty, but correct for opening and filling
     *
     * @param indexBasePath directory where it place. There will be subdirectories by index types.
     */
    public void createLuceneIndex(Path indexBasePath) throws IOException {
        log.info("createLuceneIndex({})", indexBasePath.toAbsolutePath());

        long startTime = System.currentTimeMillis();
        Files.createDirectories(indexBasePath);

        // common for all index
        Analyzer analyzer = new WhitespaceAnalyzer(Version.LUCENE_42);
        IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_42, analyzer);
        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        Directory dir;

        for (FiasIndexType type : FiasIndexType.values()) {
            Files.createDirectories(indexBasePath.resolve(type.getRelativeIndexPath()));

            dir = FSDirectory.open(new File(indexBasePath + File.separator + type.getRelativeIndexPath()));
            IndexWriter writer = new IndexWriter(dir, iwc);
            writer.close();
        }

        long endTime = System.currentTimeMillis();
        log.info("It took [" + (endTime - startTime) / 1000 + "] seconds to process");
    }

    private static void removeDirectoryRecursive(Path dir) throws IOException {
        log.info("removeDirectoryRecursive({})", dir.toAbsolutePath());
        if (Files.exists(dir)){
            Files.walk(dir) // https://stackoverflow.com/questions/779519/delete-directories-recursively-in-java/42267494#42267494
                .map(Path::toFile)
                .sorted((o1, o2) -> -o1.compareTo(o2))
                .forEach(File::delete);
        }
    }
}
