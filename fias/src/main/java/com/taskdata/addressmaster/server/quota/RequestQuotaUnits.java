/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.quota;

public enum RequestQuotaUnits {

    /**
     * Limit by requests
     */
    REQUESTS(0),
    /**
     * Limit by count of bulk records (eligible when bulk address checking only)
     */
    BULK_RECORDS(1),
    /**
     * Limit by the quantity of bytes
     */
    BYTES(2);

    public static RequestQuotaUnits find(int code) {
        for (RequestQuotaUnits requestQuotaUnits : values()) {
            if (requestQuotaUnits.code == code) {
                return requestQuotaUnits;
            }
        }
        return null;
    }

    private final int code;

    private RequestQuotaUnits(int code) {
        this.code = code;
    }

    int getCode() {
        return code;
    }
}
