/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.webapi;

import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.taskdata.addressmaster.common.JSONConstants;
import com.taskdata.addressmaster.server.event.RequestEvent;
import com.taskdata.addressmaster.server.search.model.HouseNumberObject;
import com.taskdata.addressmaster.server.service.HouseService;

/**
 * @author denis.vinnichek
 */
public class AddressHousesServlet extends AddressMasterServlet {

    private static final String ADDRESS_ID_PARAM = "addressId";

    private static final String PAGE_NUM_PARAM = "pageNum";

    private static final String PAGE_SIZE_PARAM = "pageSize";

    /*@Autowired
    private AddressService addressService;*/

    @Autowired
    private HouseService houseService;

    @Override
    protected RequestResult doGetInternal(HttpServletRequest request, HttpServletResponse response)
        throws UnsupportedEncodingException {

        String aoId = getDecodedParameter(request, ADDRESS_ID_PARAM);
        //String[] aoIds = aoIdsStr.split(" ");
        //String aoId = aoIdsStr.length() > 0 ? aoIds[aoIds.length - 1] : aoIdsStr;

        int pageNum = Integer.parseInt(getDecodedParameter(request, PAGE_NUM_PARAM));
        int pageSize = Integer.parseInt(getDecodedParameter(request, PAGE_SIZE_PARAM));

        List<HouseNumberObject> houses = houseService.getHousesByAOId(aoId, pageNum, pageSize);
        JSONArray jsonArray = new JSONArray();
        for (HouseNumberObject houseNumberObject : houses) {
            jsonArray.add(houseNumberObject.toJSON());
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONConstants.JSON_HOUSES_KEY, jsonArray);

        RequestEvent.Builder requestEventBuilder = new RequestEvent.Builder(request,
            RequestEvent.RequestEventType.GET_ADDRESS_HOUSES);
        requestEventBuilder.setUserInput("aoId: " + aoId)
            .setRows(houses.size())
            .setData(String.valueOf(houses.size()));

        return new RequestResult(jsonObject, requestEventBuilder);
    }
}
