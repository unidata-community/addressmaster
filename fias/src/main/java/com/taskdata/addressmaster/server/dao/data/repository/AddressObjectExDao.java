/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.data.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.taskdata.addressmaster.server.dao.data.entity.AddressObjectEx;

@Service
public class AddressObjectExDao extends AbstractDao<AddressObjectEx, Integer> {

    //todo 2017.03.10 moved for hibernate named query
    private static final String SQL_SELECT_AOTE_FROM_ADDRESS_OBJECT_EX_AOTE = "SELECT aote FROM AddressObjectEx aote";

    @Override
    public AddressObjectEx getById(Integer id) {
        return getEM().find(AddressObjectEx.class, id);
    }

    public Map<String, String> getSynonyms() {
        List<AddressObjectEx> synonyms = query(SQL_SELECT_AOTE_FROM_ADDRESS_OBJECT_EX_AOTE, new String[]{}, 100000);
        Map<String, String> result = new HashMap<>();
        for (AddressObjectEx addressObjectEx : synonyms) {
            result.put(addressObjectEx.getName().toLowerCase(), addressObjectEx.getAddressObject().getFormalName().toLowerCase());
        }

        return result;
    }

    @Override
    public void save(AddressObjectEx entity) {
        getEM().persist(entity);
    }
}
