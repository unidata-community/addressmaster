/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.webapi;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.taskdata.addressmaster.server.search.batch.BatchPlatform;
import com.taskdata.addressmaster.server.search.batch.source.PartStreamSource;

import static com.taskdata.addressmaster.server.search.batch.source.LocalFileSource.localFileSource;

/**
 * mb something for "Пакетная обработка данных" http://www.addressmaster.ru/batch.html
 *
 * @author denis.vinnichek
 */
@Slf4j
@MultipartConfig(location = "/tmp")
public class BatchServlet extends HttpServlet {

    private static final String SEARCH_COLUMN_NAME = "DISPLAY_ADDRESS_STRING";

    private static final int THREADS_NUM_DEFAULT = 10;

    private static final int LINES_NUM_DEFAULT = -1;

    private static final String FILE_NAME_DEFAULT = "input.csv";

    private static final String THREADS_NUN_PARAM = "threadsNum";

    private static final String LINES_NUN_PARAM = "linesNum";

    private static final String FILE_NAME_PARAM = "fileName";

    private static final String FILE_OUTPUT_PATH = "batch_files";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    private static int getIntParam(HttpServletRequest req, String paramName, int defaultValue) {
        String paramStrValue = req.getParameter(paramName);
        if (paramStrValue != null && !paramStrValue.isEmpty()) {
            try {
                return Integer.parseInt(paramStrValue);
            } catch (NumberFormatException e) {
                log.error("Can't parse int from string: '" + paramStrValue + '\'');
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    private static String getStringParam(HttpServletRequest req, String paramName, String defaultValue) {
        String paramStrValue = req.getParameter(paramName);
        return (paramStrValue != null && !paramStrValue.isEmpty()) ? paramStrValue : defaultValue;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("Started to run batch...");

        int threadsNum = getIntParam(req, THREADS_NUN_PARAM, THREADS_NUM_DEFAULT);
        int linesNum = getIntParam(req, LINES_NUN_PARAM, LINES_NUM_DEFAULT);
        String fileName = getStringParam(req, FILE_NAME_PARAM, FILE_NAME_DEFAULT);

        //todo 2017.04.05 move to application.xml or remove
        String batchInputFileDir = "/BatchFileDir";
        String inputFilePath = batchInputFileDir + File.separator + fileName;

        log.info("Going to execute Batch Servlet with [" + threadsNum + "] threads and [" + linesNum + "] line numbers");
        BatchPlatform batchPlatform = new BatchPlatform(localFileSource(inputFilePath), SEARCH_COLUMN_NAME, threadsNum, linesNum);
        Thread batchThread = new Thread(batchPlatform);
        batchThread.start();
        try {
            batchThread.join();
            log.info("Batch Servlet successfully finished.");
        } catch (InterruptedException e) {
            log.error("Can't wait till batch will finish.");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        val multipart = request.getParts();
        int threadsNum = getIntParam(request, THREADS_NUN_PARAM, THREADS_NUM_DEFAULT);
        int linesNum = getIntParam(request, LINES_NUN_PARAM, LINES_NUM_DEFAULT);
        log.info("Multipart: {}", multipart);
        if (multipart != null && !multipart.isEmpty()) {
            log.info("Incoming files: {}", multipart.size());
            for (val filePart : multipart) {
                val source = PartStreamSource.partStreamSource(FILE_OUTPUT_PATH, filePart);
                BatchPlatform batchPlatform = new BatchPlatform(source, SEARCH_COLUMN_NAME, threadsNum, linesNum);
                Thread batchThread = new Thread(batchPlatform);
                batchThread.start();
                try {
                    batchThread.join();
                    log.info("Batch Servlet successfully finished.");
                } catch (InterruptedException e) {
                    log.error("Can't wait till batch will finish.");
                }
            }
        } else {
            log.warn("Empty multipart");
        }
    }
}
