/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.batch.util;

import com.taskdata.addressmaster.common.model.CompositeAddressElement;
import com.taskdata.addressmaster.server.search.batch.model.DataLine;
import com.taskdata.addressmaster.server.search.batch.model.HeaderLine;
import com.taskdata.addressmaster.server.search.model.HouseNumberObject;
import com.taskdata.addressmaster.server.search.model.HouseRangeObject;
import com.taskdata.addressmaster.server.search.model.SearchItem;

/**
 * @author denis.vinnichek
 */
public class SearchItemUtils {

    private static final String[] FIAS_HEADER = OutputField.getFieldNames();

    private static final HeaderLine FIAS_HEADER_LINE = new HeaderLine(FIAS_HEADER);

    public static final DataLine EMPTY_FIAS_LINE;

    static {
        String[] emptyLine = new String[FIAS_HEADER.length];
        for (int i = 0; i < FIAS_HEADER.length; i++) {
            emptyLine[i] = "";
        }
        EMPTY_FIAS_LINE = new DataLine(emptyLine, FIAS_HEADER_LINE);
    }

    public static DataLine constructDataLine(SearchItem searchItem) {
        HouseNumberObject houseInfo = searchItem.getHouseInfo();
        HouseRangeObject houseRangeInfo = null;
        if (houseInfo != null) {
            houseRangeInfo = houseInfo.getHouseRangeInfo();
        }
        CompositeAddressElement addressInfo = searchItem.getAddressInfo();

        String[] fiasLine = new String[OutputField.getNumberOfFields()];

        fiasLine[OutputField.FIAS_ADDRESS.index] = addressInfo != null ? addressInfo.getClassicAddress() : "";
        fiasLine[OutputField.FIAS_CITY.index] = addressInfo != null ? addressInfo.getAddressCity() : "";
        fiasLine[OutputField.FIAS_VERDICT.index] = searchItem.getMatchStatus().toString();
        fiasLine[OutputField.FIAS_AOID.index] = String.valueOf(searchItem.getAoLeafId());
        fiasLine[OutputField.FIAS_GUID.index] = searchItem.getAoLeafGuid();
        fiasLine[OutputField.FIAS_HOUSE_ID.index] = houseInfo != null ? houseInfo.getHouseId() : "";
        fiasLine[OutputField.FIAS_HOUSE_GUID.index] = houseInfo != null ? houseInfo.getHouseGuid() : "";
        fiasLine[OutputField.FIAS_MATCH_SCORE.index] = String.valueOf(searchItem.getMatchScore());
        fiasLine[OutputField.FIAS_HOUSE_STRENGTH.index] = houseInfo != null ? houseInfo.getMatchType().toString() : "";
        fiasLine[OutputField.FIAS_LUCENE_SCORE.index] = String.valueOf(searchItem.getLuceneScore());
        fiasLine[OutputField.FIAS_AGGR_DISTANCE.index] = String.valueOf(searchItem.getAggregatedDistance());
        //fiasLine[OutputField.FIAS_MATCHED_TOKENS.index] = searchItem.getMatchedTokensString();
        fiasLine[OutputField.FIAS_MATCHED_TOKENS.index] = String.valueOf(searchItem.getMatchedTokens().size());
        fiasLine[OutputField.FIAS_ADDRESS_ZIP.index] = searchItem.getZipCode();
        fiasLine[OutputField.FIAS_HOUSE_ZIP.index] = houseInfo != null ? houseInfo.getZipCode() : "";
        fiasLine[OutputField.FIAS_HOUSE_H_NUM.index] = houseInfo != null ? houseInfo.getHouseNumber() : "";
        fiasLine[OutputField.FIAS_HOUSE_B_NUM.index] = houseInfo != null ? houseInfo.getBuildingNumber() : "";
        fiasLine[OutputField.FIAS_HOUSE_STR.index] = houseInfo != null ?
            (houseInfo.getStructure() != null ? houseInfo.getStructure().getStructureNum() : "") :
            "";
        fiasLine[OutputField.FIAS_HOUSE_RANGE_ID.index] = houseRangeInfo != null ? houseRangeInfo.getHouseRangeid() : "";
        fiasLine[OutputField.FIAS_HOUSE_RANGE_GUID.index] = houseRangeInfo != null ? houseRangeInfo.getHouseRangeGuid() : "";
        fiasLine[OutputField.FIAS_HOUSE_RANGE_ZIP.index] = houseRangeInfo != null ? houseRangeInfo.getZipCode() : "";
        fiasLine[OutputField.FIAS_HOUSE_RANGE_START.index] = houseRangeInfo != null ? String.valueOf(houseRangeInfo.getRangeStart()) : "";
        fiasLine[OutputField.FIAS_HOUSE_RANGE_END.index] = houseRangeInfo != null ? String.valueOf(houseRangeInfo.getRangeEnd()) : "";
        return new DataLine(fiasLine, FIAS_HEADER_LINE);
    }

}
