/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service;

import java.nio.file.Path;
import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.taskdata.addressmaster.server.dao.master.entity.FiasUpdaterState;
import com.taskdata.addressmaster.server.dao.master.entity.FiasUpdaterStatus;
import com.taskdata.addressmaster.server.dao.master.repository.FiasUpdaterStatusDao;
import com.taskdata.addressmaster.server.dao.master.repository.PersistentSettingsDao;
import com.taskdata.addressmaster.server.service.datasource.RoutingDataSource;
import com.taskdata.addressmaster.server.service.datasource.RoutingSourceNo;
import com.taskdata.addressmaster.server.service.exception.UpdateFiasDBServiceException;

/**
 * @author Maria Chistyakova
 * @author Pavel Alexeev
 * @since 27.03.2017.
 */
@Slf4j
@Service("dataSourceSwitchService")
@DependsOn("databaseVersionValidator")
public class DataSourceSwitchService {
    /**
     * Emulate C++ class friends concept
     * @link https://stackoverflow.com/questions/182278/is-there-a-way-to-simulate-the-c-friend-concept-in-java/18634125#18634125
     */
    public static final class SwitchKey { private SwitchKey() {} }

    private static final SwitchKey switchKey = new SwitchKey();

    private final String PROPERTY_KEY = "active.datasource";

    @Autowired
    PersistentSettingsDao persistentSettingsDao;

    @Autowired
    FiasUpdaterStatusDao statusDao;

    @Value("#{T(java.nio.file.Paths).get('${data.directory:.}')}")
    private Path luceneIndexDirBase;

    /**
     * @return current used for search datasource
     */
    public RoutingSourceNo getActive() {
        return RoutingSourceNo.valueOf(persistentSettingsDao.getById(PROPERTY_KEY).getValue());
    }

    /**
     * Switch to opposite source and return it.
     */
    @Transactional(value="transactionManagerMaster")
    public RoutingSourceNo switchToOpposite() throws UpdateFiasDBServiceException {
        if (isSwitchToStagingPossible()){
            RoutingSourceNo switchTo = getActive().opposite();
            persistentSettingsDao.save(
                persistentSettingsDao.getById(PROPERTY_KEY)
                    .setValue(switchTo.name())
            );
            routingDataSource.setCurrentNo(switchTo, switchKey);
            log.info("Switching current used Fias datasource to: " + switchTo);
            return switchTo;
        }
        else{
            throw new UpdateFiasDBServiceException("Switch datasource does not possible. Last status of update: " + statusDao.getLastOperationStatus());
        }
    }

    /**
     * Check state and errors of staging database
     *
     * @TODO logic incorrect, see http://jira.taskdata.com/browse/EG-4916. For now "switched" even did not recorded! Now implementation just moved.
     * @return true if switching treated as safe operation (update completed without errors)
     */
    private boolean isSwitchToStagingPossible() {
        FiasUpdaterStatus status = statusDao.getLastOperationStatus();

        //if last update is ok, change db
        return (!status.getWorking()
            && status.getStatus() != FiasUpdaterState.ERROR
            && !status.getSwitched()
        );
    }

    @Autowired
    RoutingDataSource routingDataSource;

    /**
     * To run application with appropriate data sources current selected persisted in master database.
     *
     * !!!Please note we can't place such code into {@link RoutingDataSource} to break dependency cycle:
     * {@link RoutingDataSource} used to create persistence unit and as parameter of entityManagerFactory bean.
     * To ensure it work properly we also set <code>hibernate.temp.use_jdbc_metadata_defaults=false</code> in fias switched
     * persistent unit (see {@link org.hibernate.engine.jdbc.env.internal.JdbcEnvironmentInitiator#initiateService(java.util.Map, org.hibernate.service.spi.ServiceRegistryImplementor)})
     * to ensure connection does not happened prior correct source will be selected on data from Main database
     *
     * @link https://stackoverflow.com/questions/1572067/org-springframework-beans-factory-beancurrentlyincreationexception-error-creati
     */
    @PostConstruct
    private void restoreCurrentSourcesState() throws UpdateFiasDBServiceException {
        RoutingSourceNo active = getActive();
        routingDataSource.setCurrentNo(active, switchKey);
        log.info("Set current DataSource to: " + active);
    }
}
