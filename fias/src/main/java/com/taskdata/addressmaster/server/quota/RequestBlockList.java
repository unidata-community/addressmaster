/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.quota;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

class RequestBlockList {

    private static class BlockTimeout extends TimerTask {

        private final Map<String, BlockTimeout> list;
        private final String key;
        private final Timer timer;

        private BlockTimeout(Map<String, BlockTimeout> list, String key, long timeoutMillis) {
            this.list = list;
            this.key = key;

            this.timer = new Timer();
            this.timer.schedule(this, timeoutMillis);
        }

        @Override
        public void run() {
            list.remove(key);
            timer.cancel();
        }
    }

    private final Map<String, BlockTimeout> addresses = Collections.synchronizedMap(new HashMap<String, BlockTimeout>());

    public RequestBlockList() {
    }

    public void blockAddress(String address, long timeoutMillis) {
        if (!addresses.containsKey(address)) {
            addresses.put(address, new BlockTimeout(addresses, address, timeoutMillis));
        }
    }

    public boolean isAddressBlocked(String remoteAddress) {
        return addresses.containsKey(remoteAddress);
    }
}
