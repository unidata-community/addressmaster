/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taskdata.addressmaster.server.dao.master.entity.RequestAudit;
import com.taskdata.addressmaster.server.dao.master.repository.RequestAuditDao;
import com.taskdata.addressmaster.server.event.RequestEvent;
import com.taskdata.addressmaster.server.event.RequestEventHandler;
import com.taskdata.addressmaster.server.event.RequestEventService;

@Service
@Slf4j
public class RequestAuditService implements RequestEventHandler, InitializingBean {

    //todo 2017.03.10 moved for hibernate named query
    private static final String SQL_SELECT_COUNT_RA_FROM_REQUEST_AUDIT_RA = "SELECT COUNT(ra) FROM RequestAudit ra";
    private static final String SQL_SELECT_COUNT_RA_FROM_REQUEST_AUDIT_RA_WHERE_RA_MAX_SCORE_LOWER_THEN = "SELECT COUNT(ra) FROM RequestAudit ra WHERE ra.maxScore <= ?1";
    private static final String SELECT_COUNT_RA_FROM_REQUEST_AUDIT_RA_WHERE_RA_MAX_SCORE_UPPER_THEN = "SELECT COUNT(ra) FROM RequestAudit ra WHERE ra.maxScore > ?1";
    private static final String SELECT_COUNT_RA_FROM_REQUEST_AUDIT_RA_WHERE_RA_ROWS_1 = "SELECT COUNT(ra) FROM RequestAudit ra WHERE ra.rows = ?1";

    @Autowired
    private RequestEventService requestEventService;

    @Autowired
    private RequestAuditDao requestAuditDao;

    @Override
    public void afterPropertiesSet() throws Exception {
        log.debug("Adding audit handler to new request events");
        requestEventService.addHandler(this);
    }

    public long countFailedRequests() {
        return (long) requestAuditDao.querySingle(SELECT_COUNT_RA_FROM_REQUEST_AUDIT_RA_WHERE_RA_MAX_SCORE_UPPER_THEN,
            new Object[]{0.5f});
    }

    public long countRowlessRequests() {
        return (long) requestAuditDao.querySingle(SELECT_COUNT_RA_FROM_REQUEST_AUDIT_RA_WHERE_RA_ROWS_1, new Object[]{0});
    }

    public long countSuccessfulRequests() {
        return (long) requestAuditDao.querySingle(SQL_SELECT_COUNT_RA_FROM_REQUEST_AUDIT_RA_WHERE_RA_MAX_SCORE_LOWER_THEN,
            new Object[]{0.5f});
    }

    public long countTotalRequests() {
        return (long) requestAuditDao.querySingle(SQL_SELECT_COUNT_RA_FROM_REQUEST_AUDIT_RA, new Object[]{});
    }

    @Override
    public void onNewRequest(RequestEvent event) {
        long startTime = System.currentTimeMillis();
        RequestAudit ra = new RequestAudit(event);
        requestAuditDao.save(ra);

        if (log.isDebugEnabled()) {
            log.debug("New request arrived:\n" + "Type: " + event.getRequestType().getDescription() + '\n' + "User input: " + '\'' + event.getUserInput() + "'\n" + "Returned rows: " + event.getRows() + '\n' + "Event metric: " + event.getEventMetric() + '\n' + "Request execution time: " + event.getRequestExecTime() + " ms" + '\n' + "Request audition time: " + (System.currentTimeMillis() - startTime) + " ms" + '\n');
        }
    }

}
