/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.impl;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.store.FSDirectory;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.taskdata.addressmaster.common.LuceneHelper;
import com.taskdata.addressmaster.common.Patterns;
import com.taskdata.addressmaster.common.model.AddressElement;
import com.taskdata.addressmaster.common.model.CompositeAddressElement;
import com.taskdata.addressmaster.server.search.model.AddressDetailsObject;
import com.taskdata.addressmaster.server.search.model.HouseMatchType;
import com.taskdata.addressmaster.server.search.model.HouseNumberObject;
import com.taskdata.addressmaster.server.search.model.HouseRangeObject;
import com.taskdata.addressmaster.server.search.model.MatchedToken;
import com.taskdata.addressmaster.server.search.model.SearchItem;
import com.taskdata.addressmaster.server.search.model.SearchItemContainer;
import com.taskdata.addressmaster.server.search.model.SearchTypeEnum;
import com.taskdata.addressmaster.server.search.model.Structure;
import com.taskdata.addressmaster.server.search.model.StructureStatus;
import com.taskdata.addressmaster.server.service.LuceneAddressObjectIndexer;
import com.taskdata.addressmaster.server.service.exception.UpdateFiasDBServiceException;

import static com.taskdata.addressmaster.db.FiasIndexType.FHOUSE;
import static com.taskdata.addressmaster.db.FiasIndexType.FOBJECT;

/**
 * Simple command-line based search demo.
 */
@Slf4j
public class LuceneSearcher {


    /**
     * number of records Lucene searcher will retrieve, these retrieves records will be further analized for similarity;
     * the bigger is the num - the better search quality is but this comes with performance penalty
     */
    @Value("${lucene.search.result.num:100}")
    private Integer luceneSearchResultNum;

    @Autowired
    LuceneAddressObjectIndexer luceneAddressObjectIndexer;

    private IndexSearcher fObjectSearcher;
    private IndexSearcher fHouseSearcher;

    private Path luceneIndexDir;

    public Path getLuceneIndexDir() {
        return luceneIndexDir;
    }

    public LuceneSearcher(Path luceneIndexDir) throws UpdateFiasDBServiceException {
        this.luceneIndexDir = luceneIndexDir;
    }

    /**
     * Open lucene indexers to perform search queries.
     * If index directory absent - it will be created empty
     */
    public void openDirectories() throws UpdateFiasDBServiceException {
        // Create if absent
        try {
            luceneAddressObjectIndexer.createIndexIfDirectoryMissing(luceneIndexDir);

            IndexReader objectReader = DirectoryReader.open(FSDirectory.open(luceneIndexDir.resolve(FOBJECT.getRelativeIndexPath()).toFile()));
            fObjectSearcher = new IndexSearcher(objectReader);

            IndexReader houseReader = DirectoryReader.open(FSDirectory.open(luceneIndexDir.resolve(FHOUSE.getRelativeIndexPath()).toFile()));
            fHouseSearcher = new IndexSearcher(houseReader);
        } catch (IOException e) {
            throw new UpdateFiasDBServiceException("Error create or open directories of Lucene searcher by path [" + luceneIndexDir.toAbsolutePath() + "]!", e);
        }
    }

    private static List<HouseNumberObject> createHouseObjectsFromRange(String houseRangeGuid, String houseRangeId, String houseRangeZip, String houseRangeType, String houseRangeStart, String houseRangeEnd) {
        List<HouseNumberObject> result = new ArrayList<>();
        int start = Integer.valueOf(houseRangeStart);
        int end = Integer.valueOf(houseRangeEnd);
        HouseRangeObject houseRangeObject = new HouseRangeObject(houseRangeGuid, houseRangeId, houseRangeZip, start, end, houseRangeType);

        if (Integer.valueOf(houseRangeType) == 1) { // flat sequence
            for (int i = start; i <= end; i++) {
                result.add(new HouseNumberObject(null, null, houseRangeZip, String.valueOf(i), "", null, houseRangeObject));
            }
        } else {
            for (int i = start; i <= end; i += 2) { // even or odd numbers
                result.add(new HouseNumberObject(null, null, houseRangeZip, String.valueOf(i), "", null, houseRangeObject));
            }
        }
        return result;
    }

    private static void findMatches(Collection<MatchedToken> matchedTokens, List<String> sourceList, Map<String, AddressElement> stringAddressElementTargetMap, boolean checkHyphens) {

        ListIterator<String> sourceListIterator = sourceList.listIterator();

        while (sourceListIterator.hasNext()) {
            int bestDistance = Integer.MAX_VALUE;
            String matchedElement = null;
            AddressElement bestCandidate = null;

            String sourceToken = sourceListIterator.next();

            Set<String> addressElementTokens = stringAddressElementTargetMap.keySet();
            for (String targetToken : addressElementTokens) {
                int levenshteinDistance = StringUtils.getLevenshteinDistance(StringUtils.remove(sourceToken, '.'), StringUtils.remove(targetToken, '.'));
                if (levenshteinDistance <= Math.min(2, sourceToken.length() / 3)) {
                    if (levenshteinDistance < bestDistance) {
                        bestDistance = levenshteinDistance;
                        bestCandidate = stringAddressElementTargetMap.get(targetToken);
                        matchedElement = targetToken;
                        if (levenshteinDistance == 0) {
                            break;
                        }
                    }
                }
            }
            if (bestCandidate != null) {
                matchedTokens.add(new MatchedToken(sourceToken, matchedElement, bestCandidate, bestDistance));
                if (checkHyphens) {
                    int sourceHyphenCount = StringUtils.countMatches(sourceToken, "-");
                    int targetHyphenCount = StringUtils.countMatches(matchedElement, "-");
                    if (sourceHyphenCount == targetHyphenCount || sourceToken.indexOf('-') != -1) {
                        stringAddressElementTargetMap.remove(matchedElement);
                        sourceListIterator.remove();
                    }
                }
            }
        }
    }

    static private List<MatchedToken> matchTokens(CompositeAddressElement compositeAddressElement, List<String> inputTokens) {
        List<MatchedToken> matchedTokens = new ArrayList<>();

        List<String> sourceWhiteSpaceList = new ArrayList<>(inputTokens);

        Map<String, AddressElement> stringAddressElementWhitespaceSeparatedMap = new HashMap<>();
        for (AddressElement addressElement : compositeAddressElement.getAddressElements()) {
            String formalName = addressElement.getFormalName().toLowerCase();
            String[] split = formalName.split(" ");
            for (String s : split) {
                stringAddressElementWhitespaceSeparatedMap.put(s, addressElement);
            }
        }
        findMatches(matchedTokens, sourceWhiteSpaceList, stringAddressElementWhitespaceSeparatedMap, true);

        List<String> sourceHyphenList = new ArrayList<>();
        for (String string : sourceWhiteSpaceList) {
            String[] split = Patterns.hyphenSeparator.split(string);
            Collections.addAll(sourceHyphenList, split);
        }

        Map<String, AddressElement> stringAddressElementHyphenSeparatedMap = new HashMap<>();
        for (Map.Entry<String, AddressElement> stringAddressElementEntry : stringAddressElementWhitespaceSeparatedMap.entrySet()) {
            AddressElement addressElement = stringAddressElementEntry.getValue();
            String[] split = Patterns.hyphenSeparator.split(stringAddressElementEntry.getKey());
            for (String str : split) {
                stringAddressElementHyphenSeparatedMap.put(str, addressElement);
            }
        }

        findMatches(matchedTokens, sourceHyphenList, stringAddressElementHyphenSeparatedMap, false);

        return matchedTokens;
    }

    private HouseNumberObject createHouseNumberObject(int id, HouseMatchType houseMatchType) throws IOException {
        Document doc = fHouseSearcher.doc(id);

        String houseNumberString = doc.get(LuceneHelper.FIELD_FHOUSE_NUMBER);
        String structure = doc.get(LuceneHelper.FIELD_FSTRUCTURE);
        Integer structureStatus = Integer.valueOf(doc.get(LuceneHelper.FIELD_FSTRUCTURE_STATUS));
        Structure structureObject = null;
        if (structure != null) {
            structureObject = new Structure(StructureStatus.findStatus(structureStatus), structure);
        }
        String buildingNumber = doc.get(LuceneHelper.FIELD_FBUILDING_NUMBER);

        String houseGuid = doc.get(LuceneHelper.FIELD_FHOUSE_GUID);
        String houseId = doc.get(LuceneHelper.FIELD_FHOUSE_ID);
        String zipCode = doc.get(LuceneHelper.FIELD_FHOUSE_ZIP);

        HouseNumberObject houseNumberObject = new HouseNumberObject(
            houseGuid,
            houseId,
            zipCode,
            houseNumberString,
            buildingNumber,
            structureObject,
            null,
            houseMatchType
        );
        return houseNumberObject;
    }


    public SearchItemContainer doSearch(SearchInput searchInput, MatchContext matchContext) throws Exception {
        Set<SearchItem> results = new HashSet<>();

        if (matchContext.getMatchSettings().isFuzzy()) {
            long startTime = System.currentTimeMillis();
            BooleanQuery fuzzyQuery = new BooleanQuery();

            BooleanQuery metaphonedQuery = new BooleanQuery();
            for (String metaphonedToken : searchInput.getMetaphonedTokens()) {
                metaphonedQuery.add(new FuzzyQuery(new Term(LuceneHelper.FIELD_FOBJECT_METAPHONE, metaphonedToken)), BooleanClause.Occur.SHOULD);
            }
            //metaphonedQuery.setMinimumNumberShouldMatch(Math.min(MIN_TOKENS_MATCHED, metaphonedQuery.getClauses().length));
            fuzzyQuery.add(metaphonedQuery, BooleanClause.Occur.MUST);

            if (!searchInput.getAddressObjectTypes().isEmpty()) {
                BooleanQuery objectTypeQuery = new BooleanQuery();
                for (String aoType : searchInput.getAddressObjectTypes()) {
                    objectTypeQuery.add(new TermQuery(new Term(LuceneHelper.FIELD_FOBJECT_AOTYPE, aoType)), BooleanClause.Occur.SHOULD);
                }
                metaphonedQuery.setBoost(0.6f);
                objectTypeQuery.setBoost(0.4f);
                fuzzyQuery.add(objectTypeQuery, BooleanClause.Occur.SHOULD);
            }

            List<SearchItem> fuzzyResults = doSearch_ConsiderLevenshteinDistance(
                fuzzyQuery,
                searchInput,
                SearchTypeEnum.FUZZY,
                matchContext
            );
            results.addAll(fuzzyResults);

            matchContext.setMeasure(MatchContext.Measure.FUZZY_A0_COMPLETE, System.currentTimeMillis() - startTime);
        }

        if (matchContext.getMatchSettings().isWildCard()) {
            long startTime = System.currentTimeMillis();

            BooleanQuery wildCardQuery = new BooleanQuery();

            BooleanQuery metaphonedQuery = new BooleanQuery();
            for (String metaphonedToken : searchInput.getMetaphonedTokens()) {
                metaphonedQuery.add(new WildcardQuery(new Term(LuceneHelper.FIELD_FOBJECT_METAPHONE, metaphonedToken + '*')), BooleanClause.Occur.MUST);
            }
            wildCardQuery.add(metaphonedQuery, BooleanClause.Occur.MUST);

            if (!searchInput.getAddressObjectTypes().isEmpty()) {
                BooleanQuery objectTypeQuery = new BooleanQuery();
                for (String aoType : searchInput.getAddressObjectTypes()) {
                    objectTypeQuery.add(new TermQuery(new Term(LuceneHelper.FIELD_FOBJECT_AOTYPE, aoType)), BooleanClause.Occur.SHOULD);
                }
                wildCardQuery.add(objectTypeQuery, BooleanClause.Occur.MUST);
            }

            List<SearchItem> wilCardResults = doSearch_ConsiderLevenshteinDistance(
                wildCardQuery,
                searchInput,
                SearchTypeEnum.WILDCARD,
                matchContext
            );
            results.addAll(wilCardResults);

            matchContext.setMeasure(MatchContext.Measure.WILD_AO_COMPETE, System.currentTimeMillis() - startTime);
        }

        return new SearchItemContainer(results);
    }

    List<SearchItem> doSearch_ConsiderLevenshteinDistance(BooleanQuery query, SearchInput searchInput, SearchTypeEnum searchTypeEnum, MatchContext matchContext) throws Exception {
        if (searchInput.getZipCode() != null) {
            Query zipQuery = new WildcardQuery(new Term(LuceneHelper.FIELD_FOBJECT_ZIPCODES, searchInput.getZipCode().substring(0, 3) + '*'));
            zipQuery.setBoost(LuceneHelper.POSTAL_CODE_SEARCH_BOOST);
            query.add(zipQuery, BooleanClause.Occur.SHOULD);
        }

        if (log.isDebugEnabled()) {
            log.debug("Searching by [" + query.toString(LuceneHelper.FIELD_FOBJECT_METAPHONE) + "] and [" + query.toString(LuceneHelper.FIELD_FOBJECT_ZIPCODES) + "] \n");
        }

        long time1 = System.currentTimeMillis();

        TopDocs hits = fObjectSearcher.search(query, luceneSearchResultNum);

        long time2 = System.currentTimeMillis();
        List<SearchItem> result = new ArrayList<>();
        for (int i = 0; i < hits.scoreDocs.length; i++) {
            Document doc = fObjectSearcher.doc(hits.scoreDocs[i].doc);//fObjectSearcher.explain(query, 1006615) fObjectSearcher.explain(query, 1009406) fObjectSearcher.explain(query, 735639)
            // todo 2017.03.09 read aoLeafId as FIELD_FOBJECT_LEAF_GUID because houses hasn't aoid, it i legacy. important for ui logic
            String aoLeafId = doc.get(LuceneHelper.FIELD_FOBJECT_LEAF_GUID);
            String aoLeafGuid = doc.get(LuceneHelper.FIELD_FOBJECT_LEAF_GUID);

            JSONArray jsonAddress = (JSONArray) JSONValue.parse(doc.get(LuceneHelper.FIELD_FOBJECT_CONTENT));
            CompositeAddressElement compositeAddressElement = new CompositeAddressElement(jsonAddress, true);

            List<MatchedToken> matchedTokens = matchTokens(compositeAddressElement, searchInput.getInitialTokens());
            if (!matchedTokens.isEmpty()) {
                HouseNumberInput houseNumberInput = searchInput.getHouseNumberInput();
                HouseNumberObject houseNumberObject = null;
                if (houseNumberInput != null) {
                    houseNumberObject = doSearchHouses(houseNumberInput, aoLeafGuid);
                }
                SearchItem item = new SearchItem(
                    hits.scoreDocs[i].score,
                    searchTypeEnum,
                    matchedTokens,
                    compositeAddressElement,
                    // todo 2017.03.09 it is copy aoLeafGuid, need fix ui
                    aoLeafId,
                    aoLeafGuid,
                    houseNumberObject);
                result.add(item);
            }
        }

        long time3 = System.currentTimeMillis();
        matchContext.addMeasure(MatchContext.Measure.INT_SEARCH_LUCENE, time2 - time1);
        matchContext.addMeasure(MatchContext.Measure.INT_SEARCH_GATHER_RESULTS, time3 - time2);
        return result;
    }

    private List<HouseNumberObject> findInIndexedHouses(String aoGuid) throws IOException {
        List<HouseNumberObject> result = new ArrayList<>();
        TermQuery aoidTermQuery = new TermQuery(new Term(LuceneHelper.FIELD_FHOUSE_AOGUID, aoGuid));
        TopDocs hits = fHouseSearcher.search(aoidTermQuery, 1000);

        for (int i = 0; i < hits.scoreDocs.length; i++) {
            Document doc = fHouseSearcher.doc(hits.scoreDocs[i].doc);

            String houseNumberString = doc.get(LuceneHelper.FIELD_FHOUSE_NUMBER);
            String structure = doc.get(LuceneHelper.FIELD_FSTRUCTURE);
            Structure structureObject = null;
            if (structure != null) {
                Integer structureStatusInt = Integer.valueOf(doc.get(LuceneHelper.FIELD_FSTRUCTURE_STATUS));
                structureObject = new Structure(StructureStatus.findStatus(structureStatusInt), structure);
            }
            String buildingNumber = doc.get(LuceneHelper.FIELD_FBUILDING_NUMBER);
            String houseid = doc.get(LuceneHelper.FIELD_FHOUSE_ID);
            String houseGuid = doc.get(LuceneHelper.FIELD_FHOUSE_GUID);
            String zipCode = doc.get(LuceneHelper.FIELD_FHOUSE_ZIP);

            result.add(new HouseNumberObject(houseGuid, houseid, zipCode, houseNumberString, buildingNumber, structureObject, null));
        }
        return result;
    }

    public List<HouseNumberObject> getHousesByAoGuid(String aoGuid) throws Exception {
        List<HouseNumberObject> result = new ArrayList<>();

        List<HouseNumberObject> inIndexedHouses = findInIndexedHouses(aoGuid);
        result.addAll(inIndexedHouses);

        result = new ArrayList<>(new TreeSet<>(result));// dedupe and sort results, sometimes the same house is present individually and as member of range
        return result;
    }

    public AddressDetailsObject getHousesById(String houseId) throws IOException, NumberFormatException {
        AddressDetailsObject addressDetails = null;

        Query query = new TermQuery(new Term(LuceneHelper.FIELD_FHOUSE_ID, houseId));
        TopDocs hits = fHouseSearcher.search(query, 1000);

        if (hits.scoreDocs.length > 0) {
            Document doc = fHouseSearcher.doc(hits.scoreDocs[0].doc);

            String houseGuid = doc.get(LuceneHelper.FIELD_FHOUSE_GUID);
            String houseNumberString = doc.get(LuceneHelper.FIELD_FHOUSE_NUMBER);
            String structure = doc.get(LuceneHelper.FIELD_FSTRUCTURE);

            String structureStatusStr = doc.get(LuceneHelper.FIELD_FSTRUCTURE_STATUS);
            String structureStatusShortDesc = null;
            String structureStatusLongDesc = null;
            if (structureStatusStr != null) {
                Integer structureStatusInt = Integer.valueOf(structureStatusStr);
                StructureStatus structureStatus = StructureStatus.findStatus(structureStatusInt);
                structureStatusLongDesc = structureStatus.getLongDesc();
                structureStatusShortDesc = structureStatus.getShortDesc();
            }
            String buildingNumber = doc.get(LuceneHelper.FIELD_FBUILDING_NUMBER);
            String zipCode = doc.get(LuceneHelper.FIELD_FHOUSE_ZIP);

            addressDetails = new AddressDetailsObject.Builder()
                .setZipCode(zipCode)
                .setHouseNumber(houseNumberString)
                .setBuildingNumber(buildingNumber)
                .setStructure(structure)
                .setStructureLongDesc(structureStatusLongDesc)
                .setStructureShortDesc(structureStatusShortDesc)
                .setFiasId(houseId)
                .setFiasGuid(houseGuid)
                .build();
        }
        return addressDetails;
    }

    private HouseNumberObject searchExactMatch(String aoGuid, String fullQuery) throws IOException {
        BooleanQuery houseQuery = new BooleanQuery();
        houseQuery.add(new TermQuery(new Term(LuceneHelper.FIELD_FHOUSE_AOGUID, aoGuid)), BooleanClause.Occur.MUST);

        houseQuery.add(new TermQuery(new Term(LuceneHelper.FIELD_FULL_HOUSE, fullQuery)), BooleanClause.Occur.MUST);

        TopDocs hits = this.fHouseSearcher.search(houseQuery, 1);

        if (hits.scoreDocs.length == 1) {
            return createHouseNumberObject(hits.scoreDocs[0].doc, HouseMatchType.EXACT);
        }
        return null;
    }


    private HouseNumberObject searchHouseOnlyMatch(String aoGuid, String houseNumQueryString) throws IOException {

        Matcher matcher = Patterns.digitsWithOptionalSlashPattern.matcher(houseNumQueryString);
        if (matcher.find()) {
            BooleanQuery exactByHouseOnlyQuery = new BooleanQuery();
            exactByHouseOnlyQuery.add(new TermQuery(new Term(LuceneHelper.FIELD_FHOUSE_AOGUID, aoGuid)), BooleanClause.Occur.MUST);

            BooleanQuery houseNumQuery = new BooleanQuery();
            String fullHouseNum = matcher.group();
            houseNumQuery.add(new TermQuery(new Term(LuceneHelper.FIELD_FULL_HOUSE, fullHouseNum + "__")), BooleanClause.Occur.SHOULD);
            if (matcher.group(2) != null) {
                houseNumQuery.add(new TermQuery(new Term(LuceneHelper.FIELD_FULL_HOUSE, matcher.group(1) + "__")), BooleanClause.Occur.SHOULD);
            }

            exactByHouseOnlyQuery.add(houseNumQuery, BooleanClause.Occur.MUST);

            TopDocs exactByHouseOnlyHits = this.fHouseSearcher.search(exactByHouseOnlyQuery, 2);
            if (exactByHouseOnlyHits.scoreDocs.length > 0) {
                return createHouseNumberObject(exactByHouseOnlyHits.scoreDocs[0].doc, HouseMatchType.HOUSE_ONLY);
            }
        }

        return null;
    }

    private HouseNumberObject searchRegexpMatch(String aoGuid, String houseNumQueryString, String fullQuery) throws IOException {
        BooleanQuery houseQuery = new BooleanQuery();
        houseQuery.add(new TermQuery(new Term(LuceneHelper.FIELD_FHOUSE_AOGUID, aoGuid)), BooleanClause.Occur.MUST);
        Matcher matcher = Patterns.digitsWithOptionalSlashPattern.matcher(houseNumQueryString);
        if (matcher.matches()) {
            String houseNumNemericalPart = matcher.group(1);
            houseQuery.add(new TermQuery(new Term(LuceneHelper.FIELD_HOUSE_NUMERIC_PART, houseNumNemericalPart)), BooleanClause.Occur.MUST);
        }
        TopDocs hits = this.fHouseSearcher.search(houseQuery, 1000);
        StringBuilder regexpQueryBuilder = new StringBuilder();
        Matcher groupMatcher = Patterns.groupingPattern.matcher(fullQuery);
        while (groupMatcher.find()) {
            String group1 = groupMatcher.group(1);
            String group2 = groupMatcher.group(2);
            if (group1.isEmpty() && group2.isEmpty()) {
                break;
            }

            if (!group2.isEmpty()) {
                group2 = Patterns.punctPattern.matcher(group2).replaceAll("");
            }
            if (!group1.isEmpty()) {
                regexpQueryBuilder.append(group1).append("(?!\\d)");
            }
            regexpQueryBuilder.append(".*");
            if (!group2.isEmpty()) {
                regexpQueryBuilder.append(group2);
                regexpQueryBuilder.append(".*");
            }
        }
        String wildcardQueryStr = regexpQueryBuilder.toString();
        Pattern wildcardQueryPattern = Pattern.compile(wildcardQueryStr);

        Collection<Integer> candidateList = new ArrayList<>();
        for (int i = 0; i < hits.scoreDocs.length; i++) {
            Document doc = this.fHouseSearcher.doc(hits.scoreDocs[i].doc);
            String fullHouseString = doc.get(LuceneHelper.FIELD_FULL_HOUSE);
            if (wildcardQueryPattern.matcher(fullHouseString).matches()) {
                candidateList.add(hits.scoreDocs[i].doc);
            }
        }

        Map<String, Integer> bestResultsMap = new HashMap<>();
        int bestResult = Integer.MAX_VALUE;
        for (Integer docId : candidateList) {
            Document doc = this.fHouseSearcher.doc(docId);
            String fullHouseString = doc.get(LuceneHelper.FIELD_FULL_HOUSE);
            int distance = StringUtils.getLevenshteinDistance(fullHouseString.replaceAll("/", ""), fullQuery);
            if (distance == bestResult) {
                bestResultsMap.put(fullHouseString, docId);
            } else if (distance < bestResult) {
                bestResult = distance;
                bestResultsMap.put(fullHouseString, docId);
            }
        }
        if (!bestResultsMap.isEmpty()) {
            Set<String> fullHouseStringKeys = bestResultsMap.keySet();
            List<String> fullHouseStringsArray = new ArrayList<>(fullHouseStringKeys);
            Collections.sort(fullHouseStringsArray);
            String key = fullHouseStringsArray.get(0);
            Integer doc = bestResultsMap.get(key);
            return createHouseNumberObject(doc, HouseMatchType.FUZZY);
        }

        return null;
    }

    /**
     * Performs a 3-step search, first looking for exact match, then, if not found, does a regexp search, and finally searches for house numbers having the only house number provided by seach
     */
    private HouseNumberObject doSearchHouses(HouseNumberInput houseNumberInput, String aoGuid) throws IOException {
        InputToken houseNumber = houseNumberInput.getHouseNumber();
        String houseNumQueryString = houseNumber != null ? houseNumber.stringValue().toUpperCase() : null;

        InputToken buildingNumber = houseNumberInput.getBuildingNumber();
        String buildingNumQueryString = buildingNumber != null ? buildingNumber.stringValue().toUpperCase() : null;

        InputToken structure = houseNumberInput.getStructure();
        String structureNumQueryString = structure != null ? structure.stringValue().toUpperCase() : null;

        String fullQuery = (houseNumQueryString + '_' + (buildingNumQueryString != null ? buildingNumQueryString : "") + '_' + (structureNumQueryString != null ? structureNumQueryString : "")).toUpperCase();

        HouseNumberObject exactSearchResult = searchExactMatch(aoGuid, fullQuery);
        if (exactSearchResult != null) {
            return exactSearchResult;
        }

        HouseNumberObject regexpSearchResult = searchRegexpMatch(aoGuid, houseNumQueryString, fullQuery);
        if (regexpSearchResult != null) {
            return regexpSearchResult;
        }

        HouseNumberObject houseOnlySearchResult = searchHouseOnlyMatch(aoGuid, houseNumQueryString);
        if (houseOnlySearchResult != null) {
            return houseOnlySearchResult;
        }
        return null;
    }
}
