/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.model;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.common.JSONConstants;

public enum StructureStatus implements JSONSerializable {
    NOT_DEFINED(0, "", ""),
    CONSTRUCTION(1, "Строение", "стр"),// ext STROENIE
    BUILDING(2, "Сооружение", "сооружение"), // ext SOORUZHENIE
    LITER(3, "Литер", "литер");

    static public StructureStatus findStatus(int code) {
        for (StructureStatus structureStatus : values()) {
            if (structureStatus.code == code) {
                return structureStatus;
            }
        }
        throw new IllegalArgumentException("Unknown StructureStatus code: " + code);
    }

    private final int code;

    private final String shortDesc;

    private final String longDesc;

    StructureStatus(int code, String longDesc, String shortDesc) {
        this.code = code;
        this.shortDesc = shortDesc;
        this.longDesc = longDesc;
    }

    public int getCode() {
        return code;
    }

    public String getLongDesc() {
        return longDesc;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    @Override
    public JSONAware toJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONConstants.JSON_FIELD_STRUCTURE_STATUS_CODE, code);
        jsonObject.put(JSONConstants.JSON_FIELD_STRUCTURE_STATUS_SHORT_DESC, shortDesc);
        jsonObject.put(JSONConstants.JSON_FIELD_STRUCTURE_STATUS_LONG_DESC, longDesc);
        return jsonObject;
    }
}
