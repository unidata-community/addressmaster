/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taskdata.addressmaster.server.search.impl.MatchContext;
import com.taskdata.addressmaster.server.search.impl.SearchInput;
import com.taskdata.addressmaster.server.search.impl.SearchInputProcessor;
import com.taskdata.addressmaster.server.search.model.MatchStatus;
import com.taskdata.addressmaster.server.search.model.SearchItem;
import com.taskdata.addressmaster.server.search.model.SearchItemContainer;
import com.taskdata.addressmaster.server.service.datasource.RoutingDataSource;

@Service
@Slf4j
public class AddressMatcherService implements InitializingBean {

    private final Map<String, String> addressObjectMetadata = new HashMap<>();

    @Autowired
    private SearchInputProcessor searchInputProcessor;

    @Autowired
    RoutingDataSource routingDataSource;

    public AddressMatcherService() {
    }

    @Override
    public void afterPropertiesSet() throws IllegalStateException {
        // @TODO outdated values! Remove - http://jira.taskdata.com/browse/EG-4909
        addressObjectMetadata.put("обл", "Область");
        addressObjectMetadata.put("р-н", "Район");
        addressObjectMetadata.put("пос", "Посёлок");
        addressObjectMetadata.put("ул", "Улица");
        addressObjectMetadata.put("дер", "Деревня");
        addressObjectMetadata.put("г", "Город");
        addressObjectMetadata.put("бул", "Бульвар");
        addressObjectMetadata.put("кр", "Край");
        addressObjectMetadata.put("рес", "Республика");
        addressObjectMetadata.put("окр", "Городской Округ");
    }

    public Map<String, String> getMetadata() {
        return addressObjectMetadata;
    }

    public SearchItemContainer match(String term, MatchContext matchContext) throws Exception {
        return match(term, matchContext, -1, null);
    }

    public SearchItemContainer match(String term, MatchContext matchContext, int limit, MatchStatus matchStatus) throws Exception {
        SearchInput searchInput = searchInputProcessor.handleInput(term, matchContext);
        log.debug("Preprocessed address string " + searchInput.getMetaphonedTokens() + ", houseInfo [" + searchInput.getNormalizedHouseNumberInput() + ']');
        SearchItemContainer searchItemContainer = routingDataSource.getCurrent().getLucene().doSearch(searchInput, matchContext);

        Collection<SearchItem> searchItems = searchItemContainer.getSearchItems();
        if (matchStatus != null) {
            Iterator<SearchItem> searchItemListIterator = searchItems.iterator();
            while (searchItemListIterator.hasNext()) {
                SearchItem next = searchItemListIterator.next();
                MatchStatus nextMatchStatus = next.getMatchStatus();
                if (nextMatchStatus.compareTo(matchStatus) < 0) {
                    searchItemListIterator.remove();
                }
            }
        }

        int size = searchItems.size();
        if (limit > 0 && size > limit) {
            searchItemContainer.limit(limit);
        }

        matchContext.finishExecition();

        return searchItemContainer;
    }
}
