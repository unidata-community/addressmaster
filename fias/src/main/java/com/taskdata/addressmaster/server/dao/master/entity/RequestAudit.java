/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.master.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

import com.taskdata.addressmaster.server.event.RequestEvent;

@Entity(name = "RequestAudit")
@Table(name = "S_REQUEST_AUDIT")
@Data
public class RequestAudit implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_request_audit_ra_id_seq")
    @SequenceGenerator(name = "s_request_audit_ra_id_seq", sequenceName = "s_request_audit_ra_id_seq", allocationSize = 1)
    @Column(name = "RA_ID", nullable = false)
    private int id;

    @Column(name = "RA_SOURCE_ADDR", length = 100, nullable = false)
    private String sourceAddress;

    @Column(name = "RA_USER_NAME", length = 100)
    private String userName;

    @Column(name = "RA_HEADERS", length = 4000)
    private String headers;

    @Column(name = "RA_INPUT")
    private String userInput;

    @Column(name = "RA_ROWS")
    private int rows;

    @Column(name = "RA_EVENT_METRIC")
    private float eventMetric;

    @Column(name = "RA_REQUEST_TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private RequestEvent.RequestEventType requestType;

    @Column(name = "RA_DATA", length = 4000, nullable = true)
    private String data;

    @Column(name = "RA_EVENT_DATE", nullable = false)
    private Date requestDate;

    @Column(name = "RA_EVENT_EXEC_TIME", nullable = false)
    private long requestExecTime;

    public RequestAudit() {
    }

    public RequestAudit(RequestEvent event) {
        copy(event);
    }

    private void copy(RequestEvent event) {
        headers = event.getHeaders();
        eventMetric = event.getEventMetric();
        rows = event.getRows();
        sourceAddress = event.getSourceAddress();
        requestType = event.getRequestType();
        userInput = event.getUserInput();
        userName = event.getUserName();
        data = event.getData();
        requestDate = event.getRequestDate();
        requestExecTime = event.getRequestExecTime();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequestAudit that = (RequestAudit) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "id=" + id + ';' + "type=" + requestType.getDescription() + ';' + "remoteAddress=" + sourceAddress + ';' + "user=" + userName + ';' + "userInput=" + userInput + ';' + "rows=" + rows + ';' + "eventMetric=" + eventMetric + ';' + "requestExecTime=" + requestExecTime + ';';
    }
}
