/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service.validator.formatters;

import java.util.Objects;

import com.taskdata.addressmaster.server.dao.data.entity.AddressObject;
import com.taskdata.addressmaster.server.dao.data.entity.HouseObject;
import com.taskdata.addressmaster.server.dao.data.entity.IFiasAddressNode;
import com.taskdata.addressmaster.server.service.validator.IFiasNodeFormatter;

/**
 * Форматировщик представления для объекта HouseObject
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/RR-514
 * @see HouseObject
 * @since 23.03.18
 */
public class HouseObjectFormatter implements IFiasNodeFormatter {

    private static final HouseObjectFormatter HOUSE_OBJECT_FORMATTER = new HouseObjectFormatter();

    public static HouseObjectFormatter houseObjectFormatter() {
        return HOUSE_OBJECT_FORMATTER;
    }

    @Override
    public String format(IFiasAddressNode object) {
        Objects.requireNonNull(object, "Object is required");
        if (object instanceof HouseObject) {
            return format((HouseObject) object);
        } else {
            throw new IllegalArgumentException(String.format("Object '%s' is not instance of '%s'", object, AddressObject.class));
        }
    }

    private String format(HouseObject addressObject) {
        return String.format("%s %s", addressObject.getHouseNum(), addressObject.getStructNum());
    }

    @Override
    public boolean accept(IFiasAddressNode object) {
        return object instanceof HouseObject;
    }
}
