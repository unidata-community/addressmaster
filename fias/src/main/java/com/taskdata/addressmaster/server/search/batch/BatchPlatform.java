/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.batch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.io.FilenameUtils;

import com.taskdata.addressmaster.server.search.batch.executor.BatchSearcher;
import com.taskdata.addressmaster.server.search.batch.model.BatchDataLine;
import com.taskdata.addressmaster.server.search.batch.model.DataLine;
import com.taskdata.addressmaster.server.search.batch.model.LastBatchDataLine;
import com.taskdata.addressmaster.server.search.batch.reader.BatchReader;
import com.taskdata.addressmaster.server.search.batch.writer.BatchWriter;

/**
 * @author denis.vinnichek
 */
@Slf4j
public class BatchPlatform implements Runnable {

    private static final String OUT_FILE_POSTFIX = "-out";

    private final IInputStreamSource inputSource;

    private final String searchColumnName;

    private final int execThreadsNum;

    private final int linesToReadNum;

    private final BlockingQueue<DataLine> dataQueue;

    private final BlockingQueue<BatchDataLine> dataToWrite;

    public BatchPlatform(IInputStreamSource inputSource, String searchColumnName, int execThreadsNum, int linesToReadNum) {
        this.inputSource = inputSource;
        this.searchColumnName = searchColumnName;
        this.execThreadsNum = execThreadsNum;
        this.linesToReadNum = linesToReadNum;
        this.dataQueue = new ArrayBlockingQueue<>(5000);
        this.dataToWrite = new ArrayBlockingQueue<>(5000);
    }

    private String constructOutFilePath() {
        val dataFilePath = inputSource.getFileName();
        String fileExtension = FilenameUtils.getExtension(dataFilePath);
        String filePathWOExt = FilenameUtils.removeExtension(dataFilePath);
        String outFilePath = filePathWOExt + OUT_FILE_POSTFIX;
        if (fileExtension != null) {
            outFilePath += FilenameUtils.EXTENSION_SEPARATOR_STR + fileExtension;
        }
        return outFilePath;
    }

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();
        try {
            BatchReader batchReader = new BatchReader(inputSource, dataQueue, linesToReadNum);
            Thread readerThread = new Thread(batchReader);
            readerThread.start();

            long searchStartTime = -1;
            List<Thread> searchThreads = new ArrayList<>();
            for (int i = 0; i < execThreadsNum; i++) {
                BatchSearcher batchSearcher = new BatchSearcher(dataQueue, dataToWrite, searchColumnName);
                batchSearcher.postInit();
                Thread searchThread = new Thread(batchSearcher);
                searchThreads.add(searchThread);
                searchThread.start();
                searchStartTime = System.currentTimeMillis();
            }

            BatchWriter batchWriter = new BatchWriter(dataToWrite, constructOutFilePath(), searchStartTime);
            Thread writeThread = new Thread(batchWriter);
            writeThread.start();

            readerThread.join();
            for (Thread execThread : searchThreads) {
                execThread.join();
            }
            dataToWrite.put(LastBatchDataLine.getInstance());
            writeThread.join();
        } catch (InterruptedException e) {
            log.error("Batch Platform Thread was interrupted.", e);
        } catch (Exception e) {
            log.error("Exception during running Batch Platform.", e);
        } finally {
            long endTime = System.currentTimeMillis();
            log.info("Successfully executed BatchPlatform.\nWorked time: " + (endTime - startTime) + " ms.");
        }
    }
}
