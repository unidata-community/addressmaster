/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.EqualsAndHashCode;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.common.JSONConstants;
import com.taskdata.addressmaster.common.Patterns;
import com.taskdata.addressmaster.common.model.AddressElement;
import com.taskdata.addressmaster.common.model.CompositeAddressElement;

@EqualsAndHashCode(callSuper = false)
public class SearchItem extends SearchResultItem implements Comparable<SearchItem> {

    private static final int STREET_LEVEL = 7;

    // weights:
    private static final int AGGREGATED_INFO_WEIGHT = 2;
    private static final int ELEMENT_LEVEL_WEIGHT = 2;
    private static final int UNMATCHED_LEVEL_BETWEEN_WEIGHT = 10;
    private static final int ZERO_DISTANCE_BOOST_WEIGHT = 5;
    private static final int RANK_ORDERED_WEIGHT = 1;
    private static final int HIGHEST_MATCH_TOKEN_LEVEL_WEIGHT = 5;
    private static final int MATCHED_ELEMENTS_WEIGHT = 5;
    private static final int NON_MATCHED_ADDRESS_ELEMENTS_WEIGHT = 2;
    private static final int LUCENE_SCORE_WEIGHT = 10;

    private static boolean checkAcceptableDistance(List<MatchedToken> matchedTokens) {
        for (MatchedToken matchedToken : matchedTokens) {
            if (matchedToken.getDistance() > 2) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkFullyMatched(String[] split, Set<String> matchedTokenStrings) {
        for (String addressElementToken : split) {
            if (!matchedTokenStrings.contains(addressElementToken)) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkZeroDistance(List<MatchedToken> matchedTokens) {
        for (MatchedToken matchedToken : matchedTokens) {
            if (matchedToken.getDistance() != 0) {
                return false;
            }
        }
        return true;
    }

    private static int computeAggregatedDistance(List<MatchedToken> matchedTokens) {
        int res = 0;
        for (MatchedToken matchedToken : matchedTokens) {
            res += matchedToken.getDistance();
        }
        return res;
    }

    private static Collection<AddressElement> getFoundAddressElements(List<MatchedToken> matchedTokens) {
        Set<AddressElement> res = new HashSet<>();
        for (MatchedToken matchedToken : matchedTokens) {
            res.add(matchedToken.getFoundElement());
        }
        return res;
    }

    private static int getLowestLevel(Collection<AddressElement> foundAddressElements) {
        int res = 0;
        for (AddressElement foundAddressElement : foundAddressElements) {
            int level = foundAddressElement.getLevel();
            if (level > res) {
                res = level;
            }
        }
        return res;
    }

    final private SearchTypeEnum matchedByQuery;

    final private float luceneScore;

    final private List<MatchedToken> matchedTokens;

    final private int aggregatedDistance;

    final private String aoLeafId;

    final private String aoLeafGuid;

    final private int highestMatchTokenLevel;

    private final int matchScore;

    private final Collection<AddressElement> matchedAddressElements;

    private final MatchStatus matchStatus;

    public SearchItem(float luceneScore,
                      SearchTypeEnum searchTypeEnum,
                      List<MatchedToken> matchedTokens,
                      CompositeAddressElement compositeAddressElement,
                      String aoLeafId,
                      String aoLeafGuid,
                      HouseNumberObject houseNumberObject) {
        super(aoLeafGuid, aoLeafId, compositeAddressElement, houseNumberObject);
        this.matchedTokens = matchedTokens;
        this.luceneScore = luceneScore;
        this.matchedByQuery = searchTypeEnum;
        this.aoLeafId = aoLeafId;
        this.aoLeafGuid = aoLeafGuid;
        this.matchedAddressElements = findMatchedAddressElements();
        this.aggregatedDistance = computeAggregatedDistance(matchedTokens);
        this.highestMatchTokenLevel = computeHighestMatchTokenLevel();

        this.matchScore = computeMatchScore();
        this.matchStatus = computeMatchStatus();
    }


    private int calculateNumUnmatchedLevelsBetween() {
        int res = 0;
        Collection<AddressElement> addressElements = getAddressInfo().getAddressElements();
        int highest = computeHighestMatchTokenLevel();
        int lowest = computeLowesMatchTokenLevel();
        List<AddressElement> unmatchedAddressElements = new ArrayList<>();
        for (AddressElement addressElement : addressElements) {
            if (!matchedAddressElements.contains(addressElement)) {
                unmatchedAddressElements.add(addressElement);
            }
        }
        for (AddressElement unmatchedAddressElement : unmatchedAddressElements) {
            int level = unmatchedAddressElement.getLevel();
            if (level > highest && level < lowest) {
                res++;
            }
        }
        return res;
    }

    private int calculateNumUnmatchedLevelsBetweenTwoLowest() {
        List<AddressElement> addressElements = new ArrayList<>(matchedAddressElements);
        Collections.sort(addressElements, Collections.reverseOrder());
        int lowestLevel = addressElements.get(0).getLevel();
        int secondLowestLevel = addressElements.get(1).getLevel();

        int res = 0;
        for (AddressElement addressElement : getAddressInfo().getAddressElements()) {
            if (!matchedAddressElements.contains(addressElement)) {
                int unmatchedLevel = addressElement.getLevel();
                if (unmatchedLevel < lowestLevel && unmatchedLevel > secondLowestLevel) {
                    res++;
                }
            }
        }
        return res;
    }

    private boolean checkAllMultiTokenElementsFullyMatched() {
        Set<AddressElement> matchedAddressElements = new HashSet<>();
        Set<String> matchedTokenStrings = new HashSet<>();
        for (MatchedToken matchedToken : matchedTokens) {
            matchedAddressElements.add(matchedToken.getFoundElement());
            matchedTokenStrings.add(matchedToken.getFoundToken());
        }
        boolean res = false;
        for (AddressElement matchedAddressElement : matchedAddressElements) {
            String formalName = matchedAddressElement.getFormalName().toLowerCase();
            String[] split = Patterns.addressElementSeparators.split(formalName);

            if (split.length > 1) {
                if (checkFullyMatched(split, matchedTokenStrings)) {
                    res = true;
                } else {
                    res = false;
                    break;
                }
            }
        }
        return res;
    }

    private boolean checkHasUnmatchedLevelsBetween() {
        int highest = computeHighestMatchTokenLevel();
        int lowest = computeLowesMatchTokenLevel();
        List<AddressElement> unmatchedAddressElements = new ArrayList<>();
        for (AddressElement addressElement : getAddressInfo().getAddressElements()) {
            if (!matchedAddressElements.contains(addressElement)) {
                unmatchedAddressElements.add(addressElement);
            }
        }
        for (AddressElement unmatchedAddressElement : unmatchedAddressElements) {
            int level = unmatchedAddressElement.getLevel();
            if (level > highest && level < lowest) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int compareTo(SearchItem o) {
        return matchScore - o.matchScore;
    }

    private int computeBiggestDistance() {
        int res = 0;
        for (MatchedToken matchedToken : matchedTokens) {
            int distance = matchedToken.getDistance();
            res = Math.max(res, distance);
        }
        return res;
    }

    private int computeHighestMatchTokenLevel() {
        int level = Integer.MAX_VALUE;
        for (MatchedToken matchedToken : matchedTokens) {
            level = Math.min(level, matchedToken.getFoundElement().getLevel());
        }
        return level;
    }

    private int computeLowesMatchTokenLevel() {
        int level = Integer.MIN_VALUE;
        for (MatchedToken matchedToken : matchedTokens) {
            level = Math.max(level, matchedToken.getFoundElement().getLevel());
        }
        return level;
    }

    private int computeMatchScore() {
        int res = 0;
        if (getHouseInfo() != null) {
            res += getHouseInfo().getMatchType().getWeight();
        }
        res += matchedTokens.size() * MATCHED_ELEMENTS_WEIGHT;

        res -= aggregatedDistance * AGGREGATED_INFO_WEIGHT;
        if (aggregatedDistance == 0 && matchedTokens.size() > 1) {
            res += ZERO_DISTANCE_BOOST_WEIGHT;
        }

        int diffMatched = getAddressInfo().getAddressElements().size() - matchedAddressElements.size();
        res -= diffMatched * NON_MATCHED_ADDRESS_ELEMENTS_WEIGHT;

        int matchedElementsLevel = 0;
        for (AddressElement matchAddressElement : matchedAddressElements) {
            matchedElementsLevel += Math.min(matchAddressElement.getLevel(), STREET_LEVEL);
        }
        res += matchedElementsLevel * ELEMENT_LEVEL_WEIGHT;

        boolean hasUnmatchedLevelsBetween = matchedAddressElements.size() == 1 || calculateNumUnmatchedLevelsBetween() > 0;
        if (matchedAddressElements.size() == 1 || hasUnmatchedLevelsBetween) {
            res -= UNMATCHED_LEVEL_BETWEEN_WEIGHT;
        }

        if (isRankOrdered() && matchedTokens.size() > 1) {
            res += RANK_ORDERED_WEIGHT;
        }
        res -= (highestMatchTokenLevel - 1) * HIGHEST_MATCH_TOKEN_LEVEL_WEIGHT;

        res += luceneScore * LUCENE_SCORE_WEIGHT;

        /*if (hasFiniteAddressElement()){
            res+= STREET_PRESENCE_WEIGHT;
        }*/

        return res;
    }

    private MatchStatus computeMatchStatus() {

        int matchAddressElementsSize = matchedAddressElements.size();
        boolean hasStreetAddressElement = hasFiniteAddressElement();
        if (matchAddressElementsSize < 2 || !hasStreetAddressElement) {
            return MatchStatus.REJECTED;
        }

        int numUnmatchedLevelsBetween = calculateNumUnmatchedLevelsBetweenTwoLowest();

        int maxDistance = 0;
        for (MatchedToken matchedToken : matchedTokens) {
            int distance = matchedToken.getDistance();
            if (distance > maxDistance) {
                maxDistance = distance;
            }
        }

        boolean isPerfect = maxDistance == 0 && numUnmatchedLevelsBetween == 0 && (getHouseInfo() != null && getHouseInfo().getMatchType() == HouseMatchType.EXACT);

        boolean isGoodStatusNoHouse = maxDistance <= 1 && numUnmatchedLevelsBetween == 0;// &&

        boolean isAcceptedStatusNoHouse = maxDistance <= 2 && numUnmatchedLevelsBetween == 0;

        boolean isGoodStatusWithHouse = isGoodStatusNoHouse && (getHouseInfo() != null && (getHouseInfo().getMatchType() == HouseMatchType.EXACT || getHouseInfo().getMatchType() == HouseMatchType.FUZZY));

        boolean isAcceptedStatusWithHouse = isAcceptedStatusNoHouse && getHouseInfo() != null;

        if (isPerfect) {
            return MatchStatus.PERFECT;
        } else if (isGoodStatusWithHouse) {
            return MatchStatus.GOOD;
        } else if (isGoodStatusNoHouse) {
            return MatchStatus.GOOD_ONLY_STREET_RECOGNIZED;
        } else if (isAcceptedStatusWithHouse) {
            return MatchStatus.ACCEPTED;
        } else if (isAcceptedStatusNoHouse) {
            return MatchStatus.ACCEPTED_ONLY_STREET_RECOGNIZED;
        } else return MatchStatus.REJECTED;
    }

    private Set<AddressElement> findMatchedAddressElements() {
        Set<AddressElement> addressElements = new HashSet<>();
        for (MatchedToken matchedToken : matchedTokens) {
            addressElements.add(matchedToken.getFoundElement());
        }
        return addressElements;
    }

    private MatchedToken findMatchToken4AddressToken(String addressToken) {
        for (MatchedToken matchedToken : matchedTokens) {
            String foundToken = matchedToken.getFoundToken();
            if (foundToken.equals(addressToken)) {
                return matchedToken;
            }
        }
        return null;
    }

    public int getAggregatedDistance() {
        return aggregatedDistance;
    }

    public String getAoLeafGuid() {
        return aoLeafGuid;
    }

    public String getAoLeafId() {
        return aoLeafId;
    }

    public float getLuceneScore() {
        return luceneScore;
    }

    public List<MatchedToken> getMatchedTokens() {
        return matchedTokens;
    }

    public String getMatchedTokensString() {
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        for (MatchedToken matchedToken : matchedTokens) {
            sb.append(matchedToken.toString()).append(',');
        }
        sb.append('}');
        return sb.toString();
    }

    public int getMatchScore() {
        return matchScore;
    }

    public MatchStatus getMatchStatus() {
        return matchStatus;
    }

    private boolean hasFiniteAddressElement() {
        for (AddressElement addressElement : matchedAddressElements) {
            if (addressElement.isFinite()) {
                return true;
            }
        }
        return false;
    }

    private boolean isRankOrdered() {
        int prevLevel = -1;
        for (MatchedToken matchedToken : matchedTokens) {
            int level = matchedToken.getFoundElement().getLevel();
            if (level < prevLevel) {
                return false;
            }
            prevLevel = level;
        }
        return true;
    }

    public JSONAware toJSON() {
        JSONObject jsonObject = (JSONObject) super.toJSON();
        jsonObject.put(JSONConstants.JSON_FIELD_MATCH_STATUS, matchStatus.toString());
        return jsonObject;
    }

    @SuppressWarnings("unchecked")
    public JSONAware toJSON(boolean debugMode) {
        JSONObject jsonObject = (JSONObject) toJSON();
        if (!debugMode) {
            return jsonObject;
        }

        DebugInfoObject debugInfoObject = DebugInfoObject.builder()
                .matchStatus(matchStatus)
                .matchScore(matchScore)
                .luceneScore(luceneScore)
                .aggregatedDistance(aggregatedDistance)
                .matchType(matchedByQuery)
                .matchedTokens(matchedTokens)
                .highestMatchTokenLevel(highestMatchTokenLevel)
                .build();

        jsonObject.put(JSONConstants.JSON_FIELD_DEBUG_INFO, debugInfoObject.toString());
        jsonObject.put(JSONConstants.JSON_FIELD_DEBUG_INFO_OBJECT, debugInfoObject.toJson());

        return jsonObject;
    }

    @Override
    public String toString() {
        return "SearchItem{" +
            "byQuery='" + matchedByQuery + '\'' +
            ", luceneScore=" + luceneScore +
            ", aggregatedDistance=" + aggregatedDistance +
            ", result='" + getAddressInfo().toJSON() + '\'' +
            ", house='" + getHouseInfo() + '\'' +
            '}';
    }
}
