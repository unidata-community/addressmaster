/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.data.repository;

import java.util.List;

import org.springframework.stereotype.Service;

import com.taskdata.addressmaster.server.dao.data.entity.Socrbase;

/**
 * DAO для объектов сокращений
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/RR-514
 * @since 23.03.18
 */
@Service
public class SocrbaseDao extends AbstractDao<Socrbase, String> {

    private static final String SELECT_ALL_VALUES = "SELECT e FROM Socrbase e WHERE name NOT IN ('Чувашия')";

    @Override
    public Socrbase getById(String id) {
        return getEM().find(Socrbase.class, id);
    }

    @Override
    public void save(Socrbase entity) {
        getEM().persist(entity);
    }

    @SuppressWarnings("unchecked")
    public List<Socrbase> selectAllValues() {
        return (List<Socrbase>) getEM().createQuery(SELECT_ALL_VALUES).getResultList();
    }

    public SocrbaseDao() {
        super();
    }
}
