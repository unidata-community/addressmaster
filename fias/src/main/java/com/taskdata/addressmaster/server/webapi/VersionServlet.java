/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.webapi;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.taskdata.addressmaster.server.dao.master.entity.FiasUpdaterState;
import com.taskdata.addressmaster.server.dao.master.entity.FiasUpdaterStatus;
import com.taskdata.addressmaster.server.event.RequestEvent;
import com.taskdata.addressmaster.server.service.AddressService;
import com.taskdata.addressmaster.server.service.DataSourceSwitchService;
import com.taskdata.addressmaster.server.service.UpdateFiasDBService;
import com.taskdata.addressmaster.server.service.exception.UpdateFiasDBServiceException;
import com.taskdata.addressmaster.server.webapi.api.HistoryResponse;
import com.taskdata.addressmaster.server.webapi.api.StatusLookupResponse;
import com.taskdata.addressmaster.server.webapi.api.UpdateFiasDataResponse;

/**
 * api {site}/VersionServlet/requestType=XXX
 * 1. get version
 * 2. get history
 * 3. run update
 * 4. get update state
 * 5. etc.
 *
 */
public class VersionServlet extends AddressMasterServlet {

    private static final String REQUEST_TYPE_KEY = "requestType";

    private static final String FIAS_VERSION_PARAM = "fiasVersion";

    private static final String UPDATE_FIAS_DATA = "updateFiasData";

    private static final String APP_BUILD_PARAM = "appBuild";

    private static final String CHANGE_DATA_SOURCE = "changeDataSource";

    private static final String CURRENT_STATUS = "currentStatus";

    private static final String UPDATE_HISTORY = "history";

    private static final String STATUS_LOOKUP = "statusLookup";

    @Autowired
    private AddressService addressService;

    @Autowired
    private UpdateFiasDBService updateFiasDBService;

    @Autowired
    DataSourceSwitchService dataSourceSwitchService;

    @Override
    protected RequestResult doGetInternal(HttpServletRequest request, HttpServletResponse response) throws IllegalArgumentException, java.io.UnsupportedEncodingException, UpdateFiasDBServiceException {
        String requestType = getDecodedParameter(request, REQUEST_TYPE_KEY);

        RequestEvent.Builder requestEventBuilder = new RequestEvent.Builder(
            request, RequestEvent.RequestEventType.VERSION);

        JSONObject json = getResponse(requestType, request);

        requestEventBuilder
            .setUserInput(requestType)
            .setData(json.toJSONString());

        return new RequestResult(json, requestEventBuilder);
    }

    private JSONObject getResponse(String requestType, HttpServletRequest request) throws UnsupportedEncodingException, UpdateFiasDBServiceException {
        UpdateFiasDataResponse res;
        FiasUpdaterStatus status;

        switch (requestType) {
            case FIAS_VERSION_PARAM:
                return updateFiasDBService.getCurrentFiasVersion();

            case APP_BUILD_PARAM:
                return addressService.getBuildNumber();

            case UPDATE_FIAS_DATA:
                status = updateFiasDBService.update(
                    getDecodedParameter(request, "url"),
                    getDecodedParameter(request, "stages"),
                    getDecodedParameter(request, "userName"),
                    getDecodedParameter(request, "userLogin")
                );

                res = new UpdateFiasDataResponse(status.getStatus(), status.getError());
                return res.toJson();

            case CHANGE_DATA_SOURCE:
                dataSourceSwitchService.switchToOpposite();

                res = new UpdateFiasDataResponse(FiasUpdaterState.OK, "switched");
                return res.toJson();

            case CURRENT_STATUS:
                status = updateFiasDBService.getLastOperationStatus();
                if (null == status){
                    status = new FiasUpdaterStatus(-1, "?", "No any update status yet", false, FiasUpdaterState.ERROR, "", "", null, null, "", false);
                }

                res = new UpdateFiasDataResponse(status.getStatus(), status.getError());
                return res.toJson();

            case UPDATE_HISTORY:
                List<FiasUpdaterStatus> list = updateFiasDBService.getHistory();
                HistoryResponse historyResponse = new HistoryResponse(list);
                return historyResponse.toJson();

            case STATUS_LOOKUP:
                List<FiasUpdaterState> states = Arrays.asList(FiasUpdaterState.values());
                StatusLookupResponse statusLookupResponse = new StatusLookupResponse(states);
                return statusLookupResponse.toJson();

            default:
                //todo 2017.03.22 return normal response. NB! ui
                throw new IllegalArgumentException("Wrong requestType argument: " + requestType);
        }
    }
}
