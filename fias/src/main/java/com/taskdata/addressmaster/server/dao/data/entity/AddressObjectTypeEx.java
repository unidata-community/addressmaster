/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.data.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * todo 2017.03.06 темна вода в облацех. мб расширение AddressObjectType для вау-эффекта
 */
@Entity(name = "AddressObjectTypeEx")
@Table(name = "FE_AO_TYPE_EX")
public class AddressObjectTypeEx implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fe_ao_type_ex_aote_id_seq")
    @SequenceGenerator(name = "fe_ao_type_ex_aote_id_seq", sequenceName = "fe_ao_type_ex_aote_id_seq", allocationSize = 1)
    @Column(name = "aote_id", nullable = false)
    private int id;

    @Column(name = "FULL_NAME", length = 50, nullable = false)
    private String fullName;

    @Column(name = "ALTERNATIVE_NAME", length = 50, nullable = false)
    private String alternativeName;

    public String getAlternativeName() {
        return alternativeName;
    }

    public String getFullName() {
        return fullName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
