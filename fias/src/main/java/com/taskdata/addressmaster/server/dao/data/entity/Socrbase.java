/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * Сокарщенные имена и абривиатуры
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/RR-514
 * @since 23.03.18
 */
@Entity(name = "Socrbase")
@Table(name = "SOCRBASE")
@Data
public class Socrbase implements Serializable {

    @Id
    @Column(name = "KOD_T_ST", length = 100, nullable = false)
    private String code;
    @Column(name = "LEVEL")
    private int level;
    @Column(name = "SOCRNAME", length = 100, nullable = false)
    private String name;
    @Column(name = "SCNAME", length = 100, nullable = false)
    private String shortName;
}
