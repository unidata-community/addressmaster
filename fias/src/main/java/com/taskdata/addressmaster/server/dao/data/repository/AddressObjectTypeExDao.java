/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.data.repository;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.taskdata.addressmaster.server.dao.data.entity.AddressObjectTypeEx;

@Service
public class AddressObjectTypeExDao extends AbstractDao<AddressObjectTypeEx, Integer> {

    //todo 2017.03.10 moved for hibernate named query
    private static final String SQL_SELECT_AOTE_FROM_ADDRESS_OBJECT_TYPE_EX_AOTE = "SELECT aote FROM AddressObjectTypeEx aote";

    public Map<String, Set<String>> getAlternativeToFormalTypeNames() {
        List<AddressObjectTypeEx> typeExtensions = query(SQL_SELECT_AOTE_FROM_ADDRESS_OBJECT_TYPE_EX_AOTE, new String[]{}, 10000);
        Map<String, Set<String>> result = new HashMap<>();

        for (AddressObjectTypeEx addressObjectTypeEx : typeExtensions) {
            String alternativeName = addressObjectTypeEx.getAlternativeName();
            if (alternativeName != null) {
                alternativeName = alternativeName.toLowerCase();
            }
            String fullName = addressObjectTypeEx.getFullName();
            if (fullName != null) {
                fullName = fullName.toLowerCase();
            }
            if (alternativeName == null || fullName == null) {
                continue;
            }

            Set<String> fullNames = result
                .computeIfAbsent(alternativeName, k -> new HashSet<>());
            fullNames.add(fullName);

        }

        return result;
    }

    @Override
    public AddressObjectTypeEx getById(Integer id) {
        return getEM().find(AddressObjectTypeEx.class, id);
    }

    @Override
    public void save(AddressObjectTypeEx entity) {
        getEM().persist(entity);
    }
}
