/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.statistics;

import java.util.EnumMap;
import java.util.Map;

import com.taskdata.addressmaster.server.event.RequestEvent;
import com.taskdata.addressmaster.server.event.RequestEventHandler;

public class RequestStatisticsHolder implements RequestEventHandler {

    private final Map<RequestEvent.RequestEventType, RequestStatistics> stats = new EnumMap<>(RequestEvent.RequestEventType.class);

    public long getFailedRequests() {
        long total = 0;
        for (RequestStatistics statistics : stats.values()) {
            total += statistics.getFailed();
        }
        return total;
    }

    public long getSuccessfulRequests() {
        return getTotalRequests() - getFailedRequests();
    }

    long getTotalRequests() {
        long total = 0;
        for (RequestStatistics statistics : stats.values()) {
            total += statistics.getTotal();
        }
        return total;
    }

    public void onNewRequest(RequestEvent event) {
        RequestStatistics statistics = stats.get(event.getRequestType());
        if (statistics == null) {
            statistics = new RequestStatistics();
            statistics.setRequestType(event.getRequestType().getCode());
            stats.put(event.getRequestType(), statistics);
        }

        statistics.incTotal();
        if (event.getRows() == 0 || event.getEventMetric() < 10) {
            statistics.incFailed();
        }
    }
}
