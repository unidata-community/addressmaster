/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.model;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.common.JSONConstants;

public class HouseRangeObject implements JSONSerializable {

    final private String houseRangeid;

    final private String houseRangeGuid;

    final private String zipCode;

    final private int rangeStart;

    final private int rangeEnd;

    final private String rangeType;

    public HouseRangeObject(JSONObject jsonObject) {
        this.houseRangeGuid = (String) jsonObject.get(JSONConstants.JSON_FIELD_HOUSE_RANGE_GUID);
        this.houseRangeid = (String) jsonObject.get(JSONConstants.JSON_FIELD_HOUSE_RANGE_ID);
        this.zipCode = (String) jsonObject.get(JSONConstants.JSON_FIELD_ZIPCODE);
        this.rangeStart = ((Long) jsonObject.get(JSONConstants.JSON_FIELD_RANGE_START)).intValue();
        this.rangeEnd = ((Long) jsonObject.get(JSONConstants.JSON_FIELD_RANGE_END)).intValue();
        this.rangeType = (String) jsonObject.get(JSONConstants.JSON_FIELD_RANGE_TYPE);
    }

    public HouseRangeObject(String houseRangeGuid, String houseRangeid, String zipCode, int rangeStart, int rangeEnd, String rangeType) {
        this.houseRangeGuid = houseRangeGuid;
        this.houseRangeid = houseRangeid;
        this.zipCode = zipCode;
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
        this.rangeType = rangeType;
    }

    public String getHouseRangeGuid() {
        return houseRangeGuid;
    }

    public String getHouseRangeid() {
        return houseRangeid;
    }

    public int getRangeEnd() {
        return rangeEnd;
    }

    public int getRangeStart() {
        return rangeStart;
    }

    public String getRangeType() {
        return rangeType;
    }

    public String getZipCode() {
        return zipCode;
    }

    @Override
    public JSONAware toJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONConstants.JSON_FIELD_HOUSE_RANGE_GUID, houseRangeGuid);
        jsonObject.put(JSONConstants.JSON_FIELD_HOUSE_RANGE_ID, houseRangeid);
        jsonObject.put(JSONConstants.JSON_FIELD_ZIPCODE, zipCode);
        jsonObject.put(JSONConstants.JSON_FIELD_RANGE_START, rangeStart);
        jsonObject.put(JSONConstants.JSON_FIELD_RANGE_END, rangeEnd);
        jsonObject.put(JSONConstants.JSON_FIELD_RANGE_TYPE, rangeType);
        return jsonObject;
    }
}
