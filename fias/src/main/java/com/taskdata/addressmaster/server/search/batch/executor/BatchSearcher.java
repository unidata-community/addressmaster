/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.batch.executor;

import java.util.Collection;
import java.util.concurrent.BlockingQueue;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.taskdata.addressmaster.server.search.batch.model.BatchDataLine;
import com.taskdata.addressmaster.server.search.batch.model.DataLine;
import com.taskdata.addressmaster.server.search.batch.model.LastDataLine;
import com.taskdata.addressmaster.server.search.batch.util.SearchItemUtils;
import com.taskdata.addressmaster.server.search.impl.MatchContext;
import com.taskdata.addressmaster.server.search.impl.MatchSettings;
import com.taskdata.addressmaster.server.search.model.SearchItem;
import com.taskdata.addressmaster.server.search.model.SearchItemContainer;
import com.taskdata.addressmaster.server.service.AddressMatcherService;

/**
 * @author denis.vinnichek
 */
@Slf4j
public class BatchSearcher implements Runnable {

    private final BlockingQueue<DataLine> dataQueue;

    private final BlockingQueue<BatchDataLine> dataToWrite;

    private final String searchColumnName;

    @Autowired
    private AddressMatcherService addressMatcherService;

    public BatchSearcher(BlockingQueue<DataLine> dataQueue, BlockingQueue<BatchDataLine> dataToWrite, String searchColumnName) {
        this.dataQueue = dataQueue;
        this.dataToWrite = dataToWrite;
        this.searchColumnName = searchColumnName;
    }

    private BatchDataLine executeSearch(DataLine dataLine) {
        String searchString = dataLine.getNamedLineItems().get(searchColumnName);
        MatchContext matchContext = new MatchContext(MatchSettings.DEFAULT_BATCH);
        DataLine foundLine = null;
        try {
            SearchItemContainer searchItemContainer = addressMatcherService.match(searchString, matchContext);
            if (searchItemContainer != null) {
                Collection<SearchItem> sortedSearchItems = searchItemContainer.getSearchItems();
                if (sortedSearchItems.size() > 0) {
                    SearchItem foundItem = sortedSearchItems.iterator().next();
                    // Below algo seems to be wrong let's take just first sorted item
//                    for (SearchItem searchItem : sortedSearchItems) {
//                        String foundZip = searchItem.getZipCode();
//                        if (foundZip != null && !foundZip.isEmpty()) {
//                            foundItem = searchItem;
//                            break;
//                        }
//                    }

                    /*if (foundItem == null) {
                        foundItem = sortedSearchItems.get(0);
                    }*/
                    foundLine = SearchItemUtils.constructDataLine(foundItem);
                }
            }
        } catch (Exception e) {
            log.error("Exception during searching by '" + searchString + "' which was extracted from '" + dataLine.getLineValue() + '\'', e);
        }

        if (foundLine == null) {
            foundLine = SearchItemUtils.EMPTY_FIAS_LINE;
            log.warn("Nothing found for: " + searchString);
        }

        return new BatchDataLine(dataLine, foundLine, matchContext);
    }

    public void postInit() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    public void run() {
        try {
            while (true) {
                DataLine dataLine = dataQueue.take();
                if (LastDataLine.getInstance().equals(dataLine)) {
                    break;
                }
                dataToWrite.put(executeSearch(dataLine));
            }
        } catch (InterruptedException e) {
            log.error("Batch executor thread: Interrupted.", e);
        } catch (Exception e) {
            log.error("Exception during running Batch Search.", e);
        } finally {
            try {
                dataQueue.put(LastDataLine.getInstance());
            } catch (InterruptedException e) {
                log.error("Can't put the last data line into queue.", e);
            }
        }
    }
}
