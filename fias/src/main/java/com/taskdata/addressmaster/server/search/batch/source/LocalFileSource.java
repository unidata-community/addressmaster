/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.batch.source;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import lombok.Value;
import lombok.val;

import com.taskdata.addressmaster.server.search.batch.IInputStreamSource;

/**
 * Источник данных на основании локально расположенного файла
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/AD-49
 * @since 04.07.18
 */
@Value
public class LocalFileSource implements IInputStreamSource {

    private final File file;

    public static LocalFileSource localFileSource(String filePath) {
        Objects.requireNonNull(filePath, "File path is required");
        val file = new File(filePath);
        if (file.isFile()) {
            return new LocalFileSource(file);
        } else {
            throw new IllegalArgumentException("Is NOT a file: " + filePath);
        }
    }

    @Override
    public InputStream openInputStream() throws IOException {
        return new FileInputStream(file);
    }

    @Override
    public String getFileName() {
        return file.getName();
    }
}
