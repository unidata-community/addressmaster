/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service.validator.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import javax.persistence.NoResultException;

import com.google.common.collect.ImmutableMap;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import com.taskdata.addressmaster.server.dao.data.entity.IFiasAddressNode;
import com.taskdata.addressmaster.server.dao.data.repository.AbstractDao;
import com.taskdata.addressmaster.server.service.validator.ISearchNodeService;

/**
 * Реализация искателя объекта по параметрам, на основании DAO объекта
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/RR_514
 * @since 23.03.18
 */
@RequiredArgsConstructor
@Slf4j
public class SearchService<T extends IFiasAddressNode> implements ISearchNodeService {

    @NonNull
    private final String entityName;
    @NonNull
    private final AbstractDao<T, ?> dao;
    @NonNull
    private final Map<String, String> fields;

    public static <T extends IFiasAddressNode> SearchService<T> searchService(String entityName, AbstractDao<T, ?> dao, Map<String, String> fields) {
        return new SearchService<>(entityName, dao, ImmutableMap.copyOf(fields));
    }

    private List<? extends IFiasAddressNode> select(Map<String, Object> params) {
        Objects.requireNonNull(params, "Params is required");
        val queryBuilder = new StringBuilder("SELECT obj FROM ")
            .append(entityName)
            .append(" obj WHERE 1=1");
        val paramList = new ArrayList<Object>();
        val counter = new AtomicInteger();
        params.forEach((key, value) -> {
            queryBuilder.append(" AND obj.").append(key);
            if (value instanceof Iterable<?>) {
                val iterator = ((Iterable<?>) value).iterator();
                queryBuilder.append(" IN (");
                if (iterator.hasNext()) {
                    paramList.add(iterator.next());
                    queryBuilder.append("?").append(counter.incrementAndGet());
                    while (iterator.hasNext()) {
                        paramList.add(iterator.next());
                        queryBuilder.append(", ?").append(counter.incrementAndGet());
                    }
                }
                queryBuilder.append(")");
            } else {
                queryBuilder.append(" = ?").append(counter.incrementAndGet());
                paramList.add(value);
            }
        });
        val query = queryBuilder.toString();
        log.debug("query: '" + query + "' -- {}", paramList);
        return dao.query(query, paramList.toArray(new Object[0]), 1000);
    }

    private Map<String, Object> prepareParameters(Map<String, Object> params) {
        val result = new HashMap<String, Object>();
        fields.forEach((key, fieldName) -> {
            val fieldValue = params.get(key);
            if (fieldValue != null) {
                result.put(fieldName, fieldValue);
            }
        });
        return result;
    }

    @Override
    public List<IFiasAddressNode> findByParameters(Map<String, Object> params) {
        val prepared = prepareParameters(params);
        List<IFiasAddressNode> result = Collections.emptyList();
        try {
            val found = select(prepared);
            if (found != null) {
                result = found.stream().map(each -> (IFiasAddressNode) each).collect(Collectors.toList());
            }
        } catch (NoResultException ignore) {
        }
        return result;
    }
}
