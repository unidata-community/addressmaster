/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.event;

import java.util.Date;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;

public class RequestEvent {

    public static class Builder {

        private final HttpServletRequest request;

        private final RequestEventType requestType;

        private String userInput;

        private int rows;

        private float eventMetric;

        private String data;

        private Date requestDate;

        private long requestExecTime;

        public Builder(HttpServletRequest request, RequestEventType requestType) {
            this.request = request;
            this.requestType = requestType;
        }

        public RequestEvent build() {
            return new RequestEvent(this);
        }

        public Builder setData(String data) {
            this.data = data;
            return this;
        }

        public Builder setEventMetric(float eventMetric) {
            this.eventMetric = eventMetric;
            return this;
        }

        public Builder setRequestDate(Date requestDate) {
            this.requestDate = requestDate;
            return this;
        }

        public Builder setRequestExecTime(long requestExecTime) {
            this.requestExecTime = requestExecTime;
            return this;
        }

        public Builder setRows(int rows) {
            this.rows = rows;
            return this;
        }

        public Builder setUserInput(String userInput) {
            this.userInput = userInput;
            return this;
        }
    }

    public enum RequestEventType {
        CORRECT(0, "Correct request"),
        AUTO_COMPLETE(1, "Auto-complete request"),
        GET_ADDRESS_HOUSES(2, "Get address houses request"),
        GET_ADDRESS_DETAILS(3, "Get address details request"),
        REST(3, "Rest Request"),
        METADATA(4, "Metadata"),
        VERSION(5, "Version"),
        BUILD_NUMBER(6, "Build Number");

        private final int code;

        private final String description;

        private RequestEventType(int code, String description) {
            this.code = code;
            this.description = description;
        }

        public int getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }
    }

    private static final String X_REAL_IP_HEADER = "X-Real-IP";

    private final String sourceAddress;

    private final String userName;

    private final String headers;

    private final String userInput;

    private final int rows;

    private final float eventMetric;

    private final RequestEventType requestType;

    private final String data;

    private final Date requestDate;

    private final long requestExecTime;

    private RequestEvent(Builder builder) {
        this.eventMetric = builder.eventMetric;
        this.rows = builder.rows;
        this.data = builder.data;

        String xRealIp = builder.request.getHeader(X_REAL_IP_HEADER);
        this.sourceAddress = xRealIp != null ? xRealIp : builder.request.getRemoteAddr();

        this.userName = builder.request.getRemoteUser();

        Enumeration headerNames = builder.request.getHeaderNames();
        StringBuilder headers = new StringBuilder();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            headers.append(headerName).append(':').append(builder.request.getHeader(headerName)).append(';');
        }
        this.headers = headers.toString();

        this.userInput = builder.userInput;
        this.requestType = builder.requestType;
        this.requestDate = builder.requestDate;
        this.requestExecTime = builder.requestExecTime;
    }

    public String getData() {
        return data;
    }

    public float getEventMetric() {
        return eventMetric;
    }

    public String getHeaders() {
        return headers;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public long getRequestExecTime() {
        return requestExecTime;
    }

    public RequestEventType getRequestType() {
        return requestType;
    }

    public int getRows() {
        return rows;
    }

    public String getSourceAddress() {
        return sourceAddress;
    }

    public String getUserInput() {
        return userInput;
    }

    public String getUserName() {
        return userName;
    }
}
