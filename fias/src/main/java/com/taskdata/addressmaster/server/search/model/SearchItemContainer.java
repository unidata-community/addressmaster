/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.common.JSONConstants;
import com.taskdata.addressmaster.server.search.impl.MatchContext;

public class SearchItemContainer {

    private List<SearchItem> searchItems;

    private String aoidsString;

    public SearchItemContainer(Collection<SearchItem> searchItems) {
        this.searchItems = new ArrayList<>(searchItems);
        if (hasPerfect()) {
            removeRejected();
        }
        Collections.sort(this.searchItems, Collections.reverseOrder()); // sort in descending order
    }

    public String getAoidsString() {
        if (aoidsString == null) {
            String[] aoidsArray = new String[searchItems.size()];
            int index = 0;
            for (SearchItem searchItem : searchItems) {
                aoidsArray[index++] = searchItem.getAoLeafId();
            }
            aoidsString = Arrays.toString(aoidsArray);
        }
        return aoidsString;
    }

    public int getBestScore() {
        if (isEmpty()) {
            return -1;
        }
        return searchItems.get(0).getMatchScore();
    }

    public int getItemsNum() {
        return searchItems.size();
    }

    @SuppressWarnings("unchecked")
    public JSONObject getJSONObject(MatchContext matchContext) {
        JSONObject result = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        for (SearchItem searchResultItem : searchItems) {
            jsonArray.add(searchResultItem.toJSON(matchContext.getMatchSettings().isDebugMode()));
        }

        result.put(JSONConstants.JSON_ADDRESSES_KEY, jsonArray);

        if (matchContext.getMatchSettings().isDebugMode()) {
            result.put(JSONConstants.JSON_FIELD_DEBUG_INFO, "OverallTime=" + matchContext.getExecutionTime() + "; Time measurement: " + matchContext.getMeasuresString());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(JSONConstants.JSON_FIELD_OVERALL_TIME, matchContext.getExecutionTime());
            jsonObject.put(JSONConstants.JSON_FIELD_TIME_MEASUREMENT, matchContext.getMeasuresAsJson());

            result.put(JSONConstants.JSON_FIELD_DEBUG_INFO_OBJECT, jsonObject);

        }
        return result;
    }

    public Collection<SearchItem> getSearchItems() {
        return searchItems;
    }

    private boolean hasPerfect() {
        for (SearchItem searchItem : searchItems) {
            if (searchItem.getMatchStatus() == MatchStatus.PERFECT) {
                return true;
            }
        }
        return false;
    }

    boolean isEmpty() {
        return searchItems.isEmpty();
    }

    public void limit(int limit) {
        searchItems = searchItems.subList(0, limit);
    }

    private void removeRejected() {
        Iterator<SearchItem> listIterator = searchItems.listIterator();
        while (listIterator.hasNext()) {
            SearchItem searchItem = listIterator.next();
            if (searchItem.getMatchStatus() == MatchStatus.REJECTED) {
                listIterator.remove();
            }
        }
    }
}
