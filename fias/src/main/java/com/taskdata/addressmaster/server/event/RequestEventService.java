/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.event;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RequestEventService implements Runnable, InitializingBean, DisposableBean {

    private final BlockingQueue<RequestEvent> queue;

    private boolean stop;
    private final List<RequestEventHandler> handlers;

    public RequestEventService() {
        this.queue = new LinkedBlockingQueue<>(Integer.MAX_VALUE);
        this.handlers = new ArrayList<>();
        this.stop = false;
    }

    public void addHandler(RequestEventHandler handler) {
        handlers.add(handler);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        stop = false;
        new Thread(this).start();
    }

    @Override
    public void destroy() throws Exception {
        stop = true;
    }

    public void fireEvent(RequestEvent event) {
        put(event);
    }

    private void processEvent(RequestEvent event) {
        if (event == null) return;
        log.trace("Running listeners for event: " + event);
        for (RequestEventHandler handler : handlers) {
            try {
                handler.onNewRequest(event);
            } catch (Throwable e) {
                log.error("Failed to handle request event " + event + " by " + handler, e);
            }
        }
    }

    private void put(RequestEvent event) {
        try {
            queue.put(event);
        } catch (InterruptedException e) {
            log.error("Failed to put new event into the queue", e);
        }
    }

    @Override
    public void run() { //TODO find out what this class was meant for
        /*while(!stop) {
            RequestEvent event = take(3000);
            LOG.info("RequestEvent = " + event);
            processEvent(event);
        }*/
    }

    private RequestEvent take(int waitMillis) {
        try {
            return queue.poll(waitMillis, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            log.error("Failed to take event from the queue", e);
        }
        return null;
    }
}
