/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.impl;

class HouseInputScanner {

    final private static String DELIMIETERS = " \t\n\r\f.,|/\\";

    final private String inputString;

    private int currentIndex = 0;

    final private int maxPosition;

    public HouseInputScanner(String inputString) {
        this.inputString = inputString;
        this.maxPosition = inputString.length();
    }

    public boolean hasNext() {
        currentIndex = skipDelimiters(currentIndex);
        return (currentIndex < maxPosition);
    }

    public InputToken nextToken() {
        currentIndex = skipDelimiters(currentIndex);
        if (currentIndex < maxPosition) {
            char c = inputString.charAt(currentIndex);

            int endIndex;
            if (Character.isDigit(c)) {
                endIndex = skipDigits(currentIndex);
            } else {
                endIndex = skipAlphas(currentIndex);
            }

            if (endIndex > currentIndex) {
                InputToken token = new InputToken(inputString, currentIndex, endIndex);
                currentIndex = endIndex;
                return token;
            } else {
                throw new IllegalStateException("Scanner failure, endIndex [" + endIndex + "] is less or equal to startIndex [" + currentIndex + ']');
            }
        } else {
            throw new IllegalStateException("Trying to invoke nextToken in invalid state, please call 'hasNext' first");
        }

    }

    private int skipAlphas(int startPosition) {
        int position = startPosition;
        while (position < maxPosition) {
            char c = inputString.charAt(position);
            if (DELIMIETERS.indexOf(c) >= 0 || Character.isDigit(c)) {
                break;
            }
            ++position;
        }
        return position;
    }

    private int skipDelimiters(int startPosition) {
        int position = startPosition;
        while (position < maxPosition) {
            char c = inputString.charAt(position);
            if (DELIMIETERS.indexOf(c) < 0) {
                break;
            }
            ++position;
        }
        return position;
    }

    private int skipDigits(int startPosition) {
        int position = startPosition;
        while (position < maxPosition) {
            char c = inputString.charAt(position);
            //if (c != '/' && (DELIMIETERS.indexOf(c) >= 0 || !Character.isDigit(c))) {
            if (c == ' ' || c == ',') { // TODO review after normalizing house indexing
                break;
            }
            ++position;
        }
        return position;
    }
}
