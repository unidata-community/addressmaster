/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.webapi;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.taskdata.addressmaster.server.service.RequestAuditService;

public class RequestAuditStatisticsServlet extends GenericServlet {

    @Autowired
    private RequestAuditService requestAuditService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        StringBuilder b = new StringBuilder();

        b.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
        b.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
        b.append("\t<head>\n");
        b.append("\t\t<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"/>\n");
        b.append("\t\t<title>Статистика пользовательских запросов</title>\n");
        b.append("\t</head>\n");
        b.append("\t<body>\n");
        b.append("\t\t<h4>Статистика пользовательских запросов</h4>\n");
        b.append("\t\t<table>\n");
        b.append("\t\t\t<tr>\n");
        b.append("\t\t\t\t<th>Метрика</th>\n");
        b.append("\t\t\t\t<th>Значение</th>\n");
        b.append("\t\t\t</tr>\n");
        b.append("\t\t\t<tr>\n");
        b.append("\t\t\t\t<td>Количество успешных запросов (score <= 0,5)</td>\n");
        b.append("\t\t\t\t<td align='center'>").append(requestAuditService.countSuccessfulRequests()).append("</td>\n");
        b.append("\t\t\t</tr>\n");
        b.append("\t\t\t<tr>\n");
        b.append("\t\t\t\t<td>Количество неуспешных запросов (score > 0,5)</td>\n");
        b.append("\t\t\t\t<td align='center'>").append(requestAuditService.countFailedRequests()).append("</td>\n");
        b.append("\t\t\t</tr>\n");
        b.append("\t\t\t<tr>\n");
        b.append("\t\t\t\t<td>Количество запросов с нулевым результатом</td>\n");
        b.append("\t\t\t\t<td align='center'>").append(requestAuditService.countRowlessRequests()).append("</td>\n");
        b.append("\t\t\t</tr>\n");
        b.append("\t\t\t<tr>\n");
        b.append("\t\t\t\t<td>Всего запросов</td>\n");
        b.append("\t\t\t\t<td align='center'>").append(requestAuditService.countTotalRequests()).append("</td>\n");
        b.append("\t\t\t</tr>\n");
        b.append("\t\t</table>\n");
        b.append("\t</body>\n");
        b.append("</html>\n");

        PrintWriter out = new PrintWriter(new OutputStreamWriter(servletResponse.getOutputStream(), "UTF8"), true);
        try {
            out.write(b.toString());
        } finally {
            out.flush();
            out.close();
        }
    }
}
