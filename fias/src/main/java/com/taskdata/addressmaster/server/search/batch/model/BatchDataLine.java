/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.batch.model;

import com.taskdata.addressmaster.server.search.impl.MatchContext;

/**
 * @author denis.vinnichek
 */
public class BatchDataLine extends CsvLine {

    private final DataLine initialLine;

    private final DataLine fiasLine;

    private HeaderLine headerLine;

    final private MatchContext matchContext;

    public BatchDataLine(DataLine initialLine, DataLine fiasLine, MatchContext matchContext) {
        super(initialLine, fiasLine);
        this.initialLine = initialLine;
        this.fiasLine = fiasLine;
        this.matchContext = matchContext;
    }

    public long getExecutionTime() {
        return matchContext.getExecutionTime();
    }

    public DataLine getFiasLine() {
        return fiasLine;
    }

    public HeaderLine getHeaderLine() {
        if (headerLine == null) {
            headerLine = new HeaderLine(initialLine.getHeaderLine(), fiasLine.getHeaderLine());
        }
        return headerLine;
    }

    public DataLine getInitialLine() {
        return initialLine;
    }
}
