/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service;

import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taskdata.addressmaster.server.search.model.AddressDetailsObject;
import com.taskdata.addressmaster.server.search.model.HouseNumberObject;
import com.taskdata.addressmaster.server.service.datasource.RoutingDataSource;

/**
 * @author Alexey Tsyryulnikov
 */
@Service
@Slf4j
public class HouseService {

    @Autowired(required = true)
    private AddressMatcherService addressMatcherService;

    @Autowired
    RoutingDataSource routingDataSource;

    /**
     * Returns address details.
     *
     * @param houseId house id for searching
     * @return details for address.
     */
    public AddressDetailsObject getAddressDetails(String houseId) {
        try {
            return routingDataSource.getCurrent().getLucene().getHousesById(houseId);
        } catch (Exception e) {
            String errorMessage = "Failed to retrieve house with ID: " + houseId;
            log.error(errorMessage, e);
            throw new RuntimeException(errorMessage, e);
        }
    }

    public List<HouseNumberObject> getHousesByAOId(String aoId, int pageNum, int pageSize) {
        try {
            //todo 2017.03.10 att! is aoId really consider aoGuid?
            return routingDataSource.getCurrent().getLucene().getHousesByAoGuid(aoId);
        } catch (Exception e) {
            String errorMessage = "Failed to retrieve houses for address object with AOID: " + aoId;
            log.error(errorMessage, e);
            throw new RuntimeException(errorMessage, e);
        }
    }
}
