/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.batch.source;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import javax.servlet.http.Part;

import lombok.Value;
import lombok.val;
import org.apache.commons.lang.StringUtils;

import com.taskdata.addressmaster.server.search.batch.IInputStreamSource;

/**
 * Источник данных на основании части запроса, переданного через upload
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/AD-49
 * @since 04.07.18
 */
@Value
public class PartStreamSource implements IInputStreamSource {


    private final String fileName;
    private final Part part;

    public static PartStreamSource partStreamSource(String path, Part part) {
        Objects.requireNonNull(part, "Part is required");
        val fileName = getSubmittedFileName(part);
        if (fileName != null && !fileName.isEmpty()) {
            return new PartStreamSource(new File(path, fileName).getAbsolutePath(), part);
        } else {
            throw new IllegalStateException("File name is not defined in part");
        }
    }


    private static String getSubmittedFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }

    @Override
    public InputStream openInputStream() throws IOException {
        return part.getInputStream();
    }

}
