/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.model;

import java.util.regex.Matcher;

import lombok.EqualsAndHashCode;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.common.JSONConstants;
import com.taskdata.addressmaster.common.Patterns;

@EqualsAndHashCode(of = {"buildingNumber", "structure", "houseNumber"})
public class HouseNumberObject implements JSONSerializable, Comparable<HouseNumberObject> {

    private static int compareHouseElements(String str1, String str2) {
        int thisHouseNum = 0;
        String thisHouseNumSuffix = "";
        Object[] thisElements = extractElements(str1);
        if (thisElements != null) {
            thisHouseNum = thisElements[0] != null ? (int) thisElements[0] : 0;
            thisHouseNumSuffix = thisElements[1] != null ? (String) thisElements[1] : "";
        }

        Object[] thatElements = extractElements(str2);
        int thatHouseNum = 0;
        String thatHouseNumSuffix = "";
        if (thatElements != null) {
            thatHouseNum = thatElements[0] != null ? (int) thatElements[0] : 0;
            thatHouseNumSuffix = thatElements[1] != null ? (String) thatElements[1] : "";
        }

        if (thisHouseNum > thatHouseNum) {
            return 1;
        }
        if (thisHouseNum < thatHouseNum) {
            return -1;
        }

        if (thisHouseNumSuffix.compareTo(thatHouseNumSuffix) > 0) {
            return 1;
        }
        if (thisHouseNumSuffix.compareTo(thatHouseNumSuffix) < 0) {
            return -1;
        }
        return 0;
    }

    private static Object[] extractElements(String str) {
        if (str != null) {
            Matcher matcher = Patterns.digitsFirstPattern.matcher(str);
            if (matcher.matches()) {
                Object[] res = new Object[2];
                String group1 = matcher.group(1);
                String group2 = matcher.group(2);
                res[0] = group1 != null ? Integer.valueOf(group1) : null;
                res[1] = group2;
                return res;
            }
        }
        return null;
    }

    final private String houseId;

    final private String houseGuid;

    final private String zipCode;

    final private String houseNumber;

    final private String buildingNumber;

    private final Structure structure;

    private final HouseRangeObject houseRangeInfo;

    private HouseMatchType matchType = HouseMatchType.NONE;

    public HouseNumberObject(JSONObject jsonObject) {
        this((String) jsonObject.get(JSONConstants.JSON_FIELD_HOUSE_GUID),
            (String) jsonObject.get(JSONConstants.JSON_FIELD_HOUSE_ID),
            (String) jsonObject.get(JSONConstants.JSON_FIELD_ZIPCODE),
            (String) jsonObject.get(JSONConstants.JSON_FIELD_HOUSE_NUMBER),
            (String) jsonObject.get(JSONConstants.JSON_FIELD_BUILDING_NUMBER),
            jsonObject.get(JSONConstants.JSON_FIELD_STRUCTURE) != null ? new Structure((JSONObject) jsonObject.get(JSONConstants.JSON_FIELD_STRUCTURE)) : null,
            jsonObject.get(JSONConstants.JSON_FIELD_HOUSE_RANGE_INFO) != null ? new HouseRangeObject((JSONObject) jsonObject.get(JSONConstants.JSON_FIELD_HOUSE_RANGE_INFO)) : null);
    }

    public HouseNumberObject(String houseGuid, String houseId, String zipCode, String houseNumber, String buildingNumber, Structure structure, HouseRangeObject houseRangeInfo) {
        this(houseGuid, houseId, zipCode, houseNumber, buildingNumber, structure, houseRangeInfo, HouseMatchType.NONE);
    }

    public HouseNumberObject(String houseGuid, String houseId, String zipCode, String houseNumber, String buildingNumber, Structure structure, HouseRangeObject houseRangeInfo, HouseMatchType matchType) {
        this.houseGuid = houseGuid;
        this.houseId = houseId;
        this.zipCode = zipCode;
        this.houseNumber = houseNumber;
        this.buildingNumber = buildingNumber;
        this.structure = structure;
        this.houseRangeInfo = houseRangeInfo;
        this.matchType = matchType;
    }

    @Override
    public int compareTo(HouseNumberObject that) {
        int houseCompare = compareHouseElements(houseNumber, that.houseNumber);
        if (houseCompare != 0) {
            return houseCompare;
        }

        int buildingCompare = compareHouseElements(buildingNumber, that.buildingNumber);
        if (buildingCompare != 0) {
            return buildingCompare;
        }

        int structureCompare = compareHouseElements(structure != null ? structure.getStructureNum() : null, that.structure != null ? that.structure.getStructureNum() : null);
        if (structureCompare != 0) {
            return structureCompare;
        }

        return 0;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public String getHouseGuid() {
        return houseGuid;
    }

    public String getHouseId() {
        return houseId;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public HouseRangeObject getHouseRangeInfo() {
        return houseRangeInfo;
    }

    public HouseMatchType getMatchType() {
        return matchType;
    }

    public Structure getStructure() {
        return structure;
    }

    public String getZipCode() {
        return zipCode;
    }

    @Override
    public JSONAware toJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONConstants.JSON_FIELD_HOUSE_GUID, houseGuid);
        jsonObject.put(JSONConstants.JSON_FIELD_HOUSE_ID, houseId);
        if (zipCode != null) {
            jsonObject.put(JSONConstants.JSON_FIELD_ZIPCODE, zipCode);
        }
        if (houseNumber != null) {
            jsonObject.put(JSONConstants.JSON_FIELD_HOUSE_NUMBER, houseNumber);
        }
        if (buildingNumber != null) {
            jsonObject.put(JSONConstants.JSON_FIELD_BUILDING_NUMBER, buildingNumber);
        }
        if (houseRangeInfo != null) {
            jsonObject.put(JSONConstants.JSON_FIELD_HOUSE_RANGE_INFO, houseRangeInfo.toJSON());
        }
        if (structure != null) {
            jsonObject.put(JSONConstants.JSON_FIELD_STRUCTURE, structure.toJSON());
        }
        return jsonObject;
    }

    @Override
    public String toString() {
        return "HouseNumberObject{" +
            "houseGuid='" + houseGuid + '\'' +
            ", houseId='" + houseId + '\'' +
            ", zipCode='" + zipCode + '\'' +
            ", houseNumber='" + houseNumber + '\'' +
            ", buildingNumber='" + buildingNumber + '\'' +
            ", structure='" + structure + '\'' +
            '}';
    }
}
