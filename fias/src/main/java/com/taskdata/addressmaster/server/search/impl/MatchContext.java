/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.impl;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

public class MatchContext {

    public enum Measure {
        PARSE_INPUT(0),
        FUZZY_A0_COMPLETE(1),
        WILD_AO_COMPETE(2),
        HOUSE_SEARCH(3),
        INT_SEARCH_LUCENE(4),
        INT_SEARCH_GATHER_RESULTS(5);

        final int index;

        Measure(int index) {
            this.index = index;
        }

        public int getIndex() {
            return index;
        }
    }

    final private MatchSettings matchSettings;

    final private long startTime;

    private long endTime = -1;

    private final long[] measures = new long[Measure.values().length];

    public MatchContext() {
        this(null);
    }

    public MatchContext(MatchSettings matchSettings) {
        this.matchSettings = matchSettings;
        this.startTime = System.currentTimeMillis();

        for (int index = 0; index < measures.length; ++index) {
            measures[index] = -1;
        }
    }

    public void addMeasure(Measure measure, long time) {
        int index = measure.getIndex();
        if (measures[index] < 0) {
            measures[index] = time;
        } else {
            measures[index] = measures[index] + time;
        }
    }

    public void finishExecition() {
        endTime = System.currentTimeMillis();
    }

    public long getEndTime() {
        return endTime;
    }

    public long getExecutionTime() {
        return endTime - startTime;
    }

    public MatchSettings getMatchSettings() {
        return matchSettings;
    }

    public String getMeasuresString() {
        StringBuilder stringBuilder = new StringBuilder("[");
        for (Measure measure : Measure.values()) {
            stringBuilder.append(measure.name()).append('=').append(measures[measure.getIndex()]).append(';');
        }
        stringBuilder.append(']');
        return stringBuilder.toString();
    }

    @SuppressWarnings("unchecked")
    public JSONAware getMeasuresAsJson() {
        JSONObject jsonObject = new JSONObject();
        for (Measure measure : Measure.values()) {
            jsonObject.put(measure.name(), measures[measure.getIndex()]);
        }
        return jsonObject;
    }


    public long getStartTime() {
        return startTime;
    }

    public void setMeasure(Measure measure, long time) {
        measures[measure.getIndex()] = time;
    }
}
