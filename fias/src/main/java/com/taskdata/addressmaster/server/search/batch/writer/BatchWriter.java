/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.batch.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import au.com.bytecode.opencsv.CSVWriter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.io.FileUtils;

import com.taskdata.addressmaster.server.search.batch.model.BatchDataLine;
import com.taskdata.addressmaster.server.search.batch.model.CsvLine;
import com.taskdata.addressmaster.server.search.batch.model.LastBatchDataLine;
import com.taskdata.addressmaster.server.search.batch.reader.BatchReader;

/**
 * @author denis.vinnichek
 */
@Slf4j
public class BatchWriter implements Runnable {

    private static final int WRITE_BLOCK_SIZE = 100;

    private final BlockingQueue<BatchDataLine> dataToWrite;

    private final String outFilePath;

    final private long startTime;

    private long lastWritingTime;

    private long counter = 0;

    private long totalExecutionTimeForAllThreads = 0;

    private double averageExecTime = 0;

    private long minExecTime = 1000000;

    private long maxExecTime = 0;

    public BatchWriter(BlockingQueue<BatchDataLine> dataToWrite, String outFilePath, long searchStartedTime) {
        this.dataToWrite = dataToWrite;
        this.outFilePath = outFilePath;
        lastWritingTime = searchStartedTime;
        startTime = searchStartedTime;
    }

    private File prepareOutputFile(String path) {
        val result = new File(path);
        val parent = result.getParentFile();
        if (parent != null && !parent.isDirectory()) {
            if (!parent.mkdirs()) {
                throw new IllegalStateException("Can't create path: " + parent);
            }
        }
        return result;
    }

    @Override
    public void run() {
        CSVWriter csvWriter = null;
        List<String[]> blockToWrite = new ArrayList<>(WRITE_BLOCK_SIZE);

        try {
            File outputFile = prepareOutputFile(outFilePath);
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(outputFile);
            OutputStreamWriter streamWriter = new OutputStreamWriter(fos, BatchReader.FILE_ENCODING);
            Writer bufferedWriter = new BufferedWriter(streamWriter);
            csvWriter = new CSVWriter(bufferedWriter, CsvLine.DEFAULT_DELIMITER);

            boolean isFirstRecord = true;
            while (true) {
                BatchDataLine batchDataLine = dataToWrite.take();
                updateStatistics(batchDataLine);

                if (LastBatchDataLine.getInstance().equals(batchDataLine)) {
                    if (blockToWrite.size() > 0) {
                        writeBlockToFile(blockToWrite, csvWriter);
                    }
                    break;
                }

                if (isFirstRecord) {
                    blockToWrite.add(batchDataLine.getHeaderLine().getLineItems());
                    isFirstRecord = false;
                }

                blockToWrite.add(batchDataLine.getLineItems());
                if (blockToWrite.size() == WRITE_BLOCK_SIZE) {
                    writeBlockToFile(blockToWrite, csvWriter);
                }
            }
        } catch (InterruptedException e) {
            log.error("Batch executor thread: Interrupted.", e);
        } catch (IOException e) {
            log.error("Can't create output file: " + outFilePath, e);
        } catch (Exception e) {
            log.error("Exception during running Batch Writer.", e);
        } finally {
            if (csvWriter != null) {
                try {
                    csvWriter.close();
                } catch (IOException e) {
                    log.error("Can't close output stream to output file: " + outFilePath, e);
                }
            }
        }
    }

    private void updateStatistics(BatchDataLine batchDataLine) {
        long lineExecTime = batchDataLine.getExecutionTime();
        if (lineExecTime >= 0) {
            ++counter;

            totalExecutionTimeForAllThreads += lineExecTime;

            if (lineExecTime < minExecTime) {
                minExecTime = lineExecTime;
            }

            if (lineExecTime > maxExecTime) {
                maxExecTime = lineExecTime;
            }
            averageExecTime = (double) totalExecutionTimeForAllThreads / (double) counter;
        }
    }

    private void writeBlockToFile(List<String[]> blockToWrite, CSVWriter csvWriter) throws IOException {
        csvWriter.writeAll(blockToWrite);
        csvWriter.flush();

        long currentTime = System.currentTimeMillis();
        long blockExecTime = currentTime - lastWritingTime;
        long totalExecTime = currentTime - startTime;

        double operationsPerSecondRecent = (double) blockToWrite.size() / ((double) blockExecTime / 1000);
        double operationsPerSecondTotal = counter / ((double) totalExecTime / 1000);
        log.info("Wrote " + counter + " lines to output file." +
            "\nOperations per second (last block measured)    : " + operationsPerSecondRecent +
            "\nOperations per second (overall)                : " + operationsPerSecondTotal +
            "\nAverage execution time of single operation (ms): " + (averageExecTime) +
            "\nMinimal execution time of single operation (ms): " + (minExecTime) +
            "\nMaximum execution time of single operation (ms): " + (maxExecTime) +
            "\nTotal execution time of all threads (sec)      : " + (totalExecutionTimeForAllThreads / 1000) +
            "\nElapsed time (sec)                             : " + (totalExecTime / 1000)
        );
        lastWritingTime = System.currentTimeMillis();

        blockToWrite.clear();
    }
}
