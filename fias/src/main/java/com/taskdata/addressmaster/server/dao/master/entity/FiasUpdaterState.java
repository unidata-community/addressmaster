/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.master.entity;

import lombok.Getter;

/**
 * @author maria.chistyakova
 * @since 28.02.2017
 */

public enum FiasUpdaterState {
    ERROR("ошибка"),
    OK("успешно"),
    DOWNLOAD("загрузка"),
    UNPACK("разпаковка"),
    DB_IMPORT("импорт в БД"),
    LUCENE_IMPORT("lucene индекс"),
    CREATE("операция обновления начата");

    FiasUpdaterState(String value) {
		this.value = value;
	}

    @Getter
    private String value;
}
