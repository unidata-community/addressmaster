/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.model;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.common.JSONConstants;

public class Structure implements JSONSerializable {

    private final StructureStatus structureStatus;

    private final String structureNum;

    public Structure(JSONObject jsonObject) {
        JSONObject structureStatusJSON = (JSONObject) jsonObject.get(JSONConstants.JSON_FIELD_STRUCTURE_STATUS);
        Long code = (Long) structureStatusJSON.get(JSONConstants.JSON_FIELD_STRUCTURE_STATUS_CODE);
        this.structureStatus = StructureStatus.findStatus(code.intValue());
        this.structureNum = (String) jsonObject.get(JSONConstants.JSON_FIELD_STRUCTURE_NUM);
    }

    public Structure(StructureStatus structureStatus, String structureNum) {
        this.structureStatus = structureStatus;
        this.structureNum = structureNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Structure structure1 = (Structure) o;

        if (structureNum != null ? !structureNum.equals(structure1.structureNum) : structure1.structureNum != null)
            return false;
        if (structureStatus != structure1.structureStatus) return false;

        return true;
    }

    public String getStructureNum() {
        return structureNum;
    }

    public StructureStatus getStructureStatus() {
        return structureStatus;
    }

    @Override
    public int hashCode() {
        int result = structureStatus != null ? structureStatus.hashCode() : 0;
        result = 31 * result + (structureNum != null ? structureNum.hashCode() : 0);
        return result;
    }

    @Override
    public JSONAware toJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONConstants.JSON_FIELD_STRUCTURE_NUM, structureNum);
        jsonObject.put(JSONConstants.JSON_FIELD_STRUCTURE_STATUS, structureStatus.toJSON());
        return jsonObject;
    }

    @Override
    public String toString() {
        return "Structure{" +
            "structureStatus=" + structureStatus +
            ", structureNum='" + structureNum + '\'' +
            '}';
    }
}
