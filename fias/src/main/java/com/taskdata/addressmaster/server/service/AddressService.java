/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taskdata.addressmaster.common.JSONConstants;
import com.taskdata.addressmaster.server.dao.data.repository.AddressObjectDao;
import com.taskdata.addressmaster.server.search.model.AddressDetailsObject;

/**
 * @author denis.vinnichek
 */
@Service
public class AddressService {
    private static final String APP_VERSION = "0.1";

    @Autowired
    private AddressObjectDao addressDao;

    private static final String SQL_GET_ADDRESS_DETAILS_QUERY = "select a.postalCode, '', '', '', a.okato, a.oktmo, a.ifnsfl, a.terrsIfnsfl, a.ifnsul, a.guid, a.code " +
        "from AddressObject a where a.id = ?";

    private static AddressDetailsObject constructAddressDetails(Object[] detailsFields) {
        String zipCode = (String) detailsFields[0];
        String houseNumber = (String) detailsFields[1];
        String buildingNumber = (String) detailsFields[2];
        String structure = (String) detailsFields[3];
        String okato = (String) detailsFields[4];
        String oktmo = (String) detailsFields[5];
        String ifnsfl = (String) detailsFields[6];
        String terrIfnsfl = (String) detailsFields[7];
        String ifnsul = (String) detailsFields[8];
        String guid = (String) detailsFields[9];
        String kladr = (String) detailsFields[10];

        return new AddressDetailsObject.Builder()
            .setZipCode(zipCode)
            .setHouseNumber(houseNumber)
            .setBuildingNumber(buildingNumber)
            .setStructure(structure)
            .setOkato(okato)
            .setOktmo(oktmo)
            .setIfnsfl(ifnsfl)
            .setTerrIfnsfl(terrIfnsfl)
            .setIfnsul(ifnsul)
            .setFiasGuid(guid)
            .setKladr(kladr)
            .build();
    }

    /**
     * Returns address details.
     *
     * @param aoId AOID for searching
     * @return details for address.
     */
    public AddressDetailsObject getAddressDetails(String aoId) {
        Object[] detailsFields = addressDao.querySingleFields(SQL_GET_ADDRESS_DETAILS_QUERY, new String[]{aoId});
        return constructAddressDetails(detailsFields);
    }

    public JSONObject getBuildNumber() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONConstants.JSON_APP_BUILD_KEY, APP_VERSION);
        return jsonObject;
    }
}
