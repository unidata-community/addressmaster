/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.data.entity;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.taskdata.addressmaster.server.dao.types.UUIDAutoSwitchType;

/**
 * main address object. use PARENTID for create full address(street, for example)
 * ADDROBJ (Object) содержит коды, наименования и типы адресообразующих элементов
 * (регионы; округа; районы (улусы, кужууны); города,
 * поселки городского типа, сельские населенные пункты;
 * элементы планировочной структуры, элементы улично-дорожной сети  .
 */
@Entity(name = "AddressObject")
@Table(name = "addrob_all")
@EqualsAndHashCode(of = {"id"})
@Data
@TypeDef(
    name = "uuid-auto",
    defaultForType = UUID.class,
    typeClass = UUIDAutoSwitchType.class
)
public class AddressObject implements IFiasAddressNode {

    @Id
    @Column(name = "AOID", nullable = false)
    @Type(type = "uuid-auto")
    private UUID id;

    @Column(name = "AOGUID", length = 36, nullable = false)
    @Type(type = "uuid-auto")
    private UUID guid;

    @OneToOne
    @JoinColumn(name = "PARENTGUID", referencedColumnName = "AOGUID")
    private AddressObject parent;

    @Column(name = "AOLEVEL", nullable = false)
    private int level;

    @Column(name = "SHORTNAME", length = 10, nullable = false)
    private String shortName;

    @Column(name = "FORMALNAME", length = 120, nullable = false)
    private String formalName;

    @Column(name = "OFFNAME", length = 120)
    private String officialName;

    @Column(name = "REGIONCODE", length = 2, nullable = false)
    private String regionCode;

    @Column(name = "AUTOCODE", length = 1, nullable = false)
    private String autonomyCode;

    @Column(name = "AREACODE", length = 3, nullable = false)
    private String areaCode;

    @Column(name = "CITYCODE", length = 3, nullable = false)
    private String cityCode;

    @Column(name = "CTARCODE", length = 3, nullable = false)
    private String cityAreaCode;

    @Column(name = "PLACECODE", length = 3, nullable = false)
    private String placeCode;

    @Column(name = "STREETCODE", length = 4)
    private String streetCode;

    @Column(name = "EXTRCODE", length = 4, nullable = false)
    private String extraCode;

    @Column(name = "SEXTCODE", length = 3, nullable = false)
    private String secondExtraCode;

    @Column(name = "IFNSFL", length = 4)
    private String ifnsfl;

    @Column(name = "TERRIFNSFL", length = 4)
    private String terrsIfnsfl;

    @Column(name = "IFNSUL", length = 4)
    private String ifnsul;

    @Column(name = "TERRIFNSUL", length = 4)
    private String terrsIfnsul;

    @Column(name = "OKATO", length = 11)
    private String okato;

    @Column(name = "OKTMO", length = 8)
    private String oktmo;

    @Column(name = "UPDATEDATE", nullable = false)
    private Date updateDate;

    @Column(name = "CODE", length = 17)
    private String code;

    @Column(name = "PLAINCODE", length = 15)
    private String plainCode;

    @Column(name = "POSTALCODE", length = 6)
    private String postalCode;

    @Column(name = "LIVESTATUS", nullable = false, columnDefinition = "char")
    private Boolean liveStatus;
}
