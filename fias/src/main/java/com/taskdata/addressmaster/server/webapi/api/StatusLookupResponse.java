/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.webapi.api;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.server.dao.master.entity.FiasUpdaterState;

import static com.taskdata.addressmaster.common.JSONConstants.JSON_FIELD_STATUS_LOOKUP;

/**
 * @author maria.chistyakova
 * @since 27.03.2017.
 */
@Data
@NoArgsConstructor
public class StatusLookupResponse {

    @Data
    @NoArgsConstructor
    public class StatusLookupElement {

        private String value;
        private String displayName;

        StatusLookupElement(FiasUpdaterState state) {
            this.value = state.toString();
            this.displayName = state.getValue();
        }

        JSONObject toJson() {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(value, displayName);
            return jsonObject;
        }
    }

    private JSONArray statuses;

    public StatusLookupResponse(List<FiasUpdaterState> list) {
        JSONArray statuses = new JSONArray();
        for (FiasUpdaterState state : list) {
            statuses.add((new StatusLookupElement(state)).toJson());
        }
        this.statuses = statuses;
    }

    public JSONObject toJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSON_FIELD_STATUS_LOOKUP, statuses);
        return jsonObject;
    }
}
