/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service.validator;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.taskdata.addressmaster.server.dao.data.entity.IFiasAddressNode;

/**
 * Интерфейс искателя объектов ФИАС по параметрам
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/КК-514
 * @since 23.03.18
 */
public interface ISearchNodeService {

    /**
     * Найти объекты по параметрам
     * @param params - параметры поиска
     * @return - список найденых объектов
     */
    List<IFiasAddressNode> findByParameters(Map<String, Object> params);

}
