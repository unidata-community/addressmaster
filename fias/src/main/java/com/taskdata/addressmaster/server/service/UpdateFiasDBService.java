/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import com.github.junrar.Junrar;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import com.taskdata.addressmaster.common.JSONConstants;
import com.taskdata.addressmaster.common.util.ZipUtil;
import com.taskdata.addressmaster.db.FlywayUpdater;
import com.taskdata.addressmaster.server.dao.master.entity.FiasUpdaterState;
import com.taskdata.addressmaster.server.dao.master.entity.FiasUpdaterStatus;
import com.taskdata.addressmaster.server.dao.master.repository.FiasUpdaterStatusDao;
import com.taskdata.addressmaster.server.service.datasource.ExtendedDataSource;
import com.taskdata.addressmaster.server.service.datasource.RoutingDataSource;
import com.taskdata.addressmaster.server.service.exception.UpdateFiasDBServiceException;

import static com.taskdata.addressmaster.common.JSONConstants.JSON_FIAS_KEY;
import static com.taskdata.addressmaster.common.Patterns.fiasVersionFromUpdateUrl;
import static com.taskdata.addressmaster.server.dao.master.entity.FiasUpdaterState.*;
import static info.hubbitus.utils.bench.ProgressLogger.each;
import static java.lang.String.format;

/**
 * service for update db and lucene from fias
 *
 * @author maria.chistyakova
 * @since 21.02.2017
 */
@Slf4j
@Service
public class UpdateFiasDBService {

    @Autowired
    FiasUpdaterStatusDao statusDao;

    @Autowired
    RoutingDataSource routingDataSource;

    @Autowired
    LuceneAddressObjectIndexer luceneAddressObjectIndexer;

    @Autowired
    AddressMatcherService addressMatcherService;

    /**
     * Classpath resource
     */
    private final String LOAD_DBF_SCRIPT = "load_dbf.bash";

    @Autowired
    ResourceLoader resourceLoader;

    @Value("${data.directory:.}/fias.archive")
    private String PATH_TO_ARCHIVE_FILE;
    @Value("${data.directory:.}/extract")
    private String PATH_TO_TMP_EXTRACT_DIRECTORY;

    /**
     * Async Update Thread
     */
    private final ExecutorService executorService = Executors.newSingleThreadExecutor(new ThreadFactory() {
        @Override
        public Thread newThread(@NotNull Runnable r) {
            Thread t = new Thread(r, "UpdateFiasDBServiceThread");
            t.setDaemon(true);
            return t;
        }
    });

    @PostConstruct
    void cleanStalledOperationsOnStart(){
        log.info("Cleanup stalled update tasks on startup");
        statusDao.failAllActive();
    }

    /**
     * Functional interface to create map of FiasUpdaterState -> "step to execute"
     * <p>
     * Does not extends {@link java.util.function.BiConsumer<T,U>} just to override signature of call method with checked exception
     */
    @FunctionalInterface
    private interface UpdateFiasDbStep<T, U> {
        default UpdateFiasDbStep<T, U> andThen(UpdateFiasDbStep<? super T, ? super U> after) throws UpdateFiasDBServiceException {
            Objects.requireNonNull(after);
            return (l, r) -> {
                this.call(l, r);
                after.call(l, r);
            };
        }

        void call(T var1, U var2) throws UpdateFiasDBServiceException;
    }

    private static final Map<FiasUpdaterState, UpdateFiasDbStep<UpdateFiasDBService, String>> UPDATE_STEPS = new LinkedHashMap<FiasUpdaterState, UpdateFiasDbStep<UpdateFiasDBService, String>>() {{
        put(DOWNLOAD, UpdateFiasDBService::downloadNewBase);
        put(UNPACK, (UpdateFiasDBService self, String url) -> self.unPack());
        put(DB_IMPORT, (UpdateFiasDBService self, String url) -> self.cleanImportDataIntoDB());
        put(LUCENE_IMPORT, (UpdateFiasDBService self, String url) -> self.reCreateLucene());
    }};


    private void downloadNewBase(String url) throws UpdateFiasDBServiceException {
        FiasUpdaterStatus status = statusDao.getActive();
        try(
            ReadableByteChannel rbc = Channels.newChannel(new URL(url).openStream());
            FileOutputStream fos = new FileOutputStream(PATH_TO_ARCHIVE_FILE)
        ) {
            log.debug("downloadNewBase(): Downloading archive file");
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

            statusDao.setStatus(DOWNLOAD, status);
            log.debug("downloadNewBase(): Done downloading archive file");
        } catch (IOException e) {
            throw new UpdateFiasDBServiceException(e.getMessage(), e);
        }
    }

    /**
     * Direct SQL execution. Targeted on low-level operations like DDL when Hibernate is not helpful
     */
    private void executeSqlDirect(String command, StringBuilder sqlLog, Connection connection) throws SQLException {
        sqlLog.append("try ").append(command).append("\n");
        PreparedStatement statement = connection.prepareStatement(command);
        boolean result = statement.execute();
        sqlLog.append("result ").append(command).append(" is ").append(result).append("\n");
    }

    public List<FiasUpdaterStatus> getHistory() {
        return statusDao.getHistory();
    }

    public FiasUpdaterStatus getLastOperationStatus() {
        return statusDao.getLastOperationStatus();
    }

    /**
     * On new update iteration as we do not provide any incremental updates just doing DROP/CREATE for public schema
     * to guarantee clean import of new data
     */
    private void resetStagingDatabase() throws SQLException {
        StringBuilder sqlLog = new StringBuilder();
        executeSqlDirect("DROP SCHEMA public CASCADE; CREATE SCHEMA public;", sqlLog, routingDataSource.getStaging().getConnection());
        log.debug("Resetting staging database result: " + sqlLog);

        FlywayUpdater.updateFias(routingDataSource.getStaging()); // Roll migrations if necessary
    }

    private void importDbfIntoDB() throws UpdateFiasDBServiceException {
        ExtendedDataSource dataSourceParams = routingDataSource.getStaging();

        // Place our loader script from resources to filesystem for regular run:
        Path script = Paths.get(PATH_TO_TMP_EXTRACT_DIRECTORY).resolve(LOAD_DBF_SCRIPT);
        log.info("Run importDbfIntoDB() script [" + script.toAbsolutePath() + "]");
        try {
            Files.copy(resourceLoader.getResource("classpath:" + LOAD_DBF_SCRIPT).getInputStream(), script, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new UpdateFiasDBServiceException("Can't copy file load_dbf.bash into temporary directory [" + script.toAbsolutePath() + "]", e);
        }

        Process process = null;
        try {
            process = Runtime.getRuntime().exec(
                new String[]{"bash", script.toAbsolutePath().toString()}
                ,new String[]{
                    "POSTGRES_DB=" + dataSourceParams.getDbName(),
                    "POSTGRES_USERNAME=" + dataSourceParams.getUsername(),
                    "POSTGRES_PASSWORD=" + dataSourceParams.getPassword(),
                    "POSTGRES_HOST=" + dataSourceParams.getHost(),
                    "POSTGRES_PORT=" + dataSourceParams.getPort()
                }
                ,script.toAbsolutePath().getParent().toFile()
            );
            log.error("Process errors: " + IOUtils.toString(process.getErrorStream(), Charset.defaultCharset()));
            log.debug("Process output: " + IOUtils.toString(process.getInputStream(), Charset.defaultCharset()));
            int exitStatus = process.waitFor();
            log.debug("Process exitStatus: " + exitStatus);
            if (exitStatus != 0) {
                throw new UpdateFiasDBServiceException("Error execute command. Please look at error output early");
            }
        } catch (IOException | InterruptedException e) {
            log.error("importDbfIntoDB(): Error happened on importing DBF into DB", e);
            throw new UpdateFiasDBServiceException("importDbfIntoDB(): Error happened on importing DBF into DB", e);
        }
        finally {
            if (null != process) process.destroy();
        }
    }

    /**
     * When we initially load new coming FIAS data from DBF or XML DB required run bunch of tasks like:
     * <ul>
     *     <li>Merge some numbered tables: address01, address02… address99 -> address</li>
     *     <li>Adjust inferred types like String -> UUID</li>
     *     <li>Create some indexes for speedup</li>
     *     <li>Cleanup</li>
     * </ul>
     * That all happened now in <code>preprocess_imported_db()</code> function.
     */
    private void processStagingDatabaseAfterInitialDataImport() throws SQLException {
        log.debug("processStagingDatabaseAfterInitialDataImport() start");
        Connection connection = routingDataSource.getStaging().getConnection();
        StringBuilder sqlLog = new StringBuilder();
        connection.setAutoCommit(false);
        executeSqlDirect("SELECT preprocess_imported_db()", sqlLog, connection);
        connection.commit();
        log.debug("processStagingDatabaseAfterInitialDataImport() log: " + sqlLog);
    }

    /**
     * Create/drop public schema to ensure cleanup, read DBF and perform some initial steps like index creation.
     *
     * do not use hibernate, only datasource, because on first migration we have
     * datasource, but hibernate do not start yet
     *
     * @see #resetStagingDatabase()
     * @see #importDbfIntoDB()
     * @see #processStagingDatabaseAfterInitialDataImport()
     */
    private void cleanImportDataIntoDB() throws UpdateFiasDBServiceException {
        try{
            resetStagingDatabase();
            importDbfIntoDB();
            processStagingDatabaseAfterInitialDataImport();
        } catch (SQLException e) {
            throw new UpdateFiasDBServiceException("cleanImportDataIntoDB(): sql error: " + e.getMessage(), e);
        }
        FiasUpdaterStatus status = statusDao.getActive();
        statusDao.setStatus(DB_IMPORT, status);
    }


    /**
     * Delete/create directory and reindex data from DB.
     * Unfortunately incremental diff read is not implemented now.
     */
    private void reCreateLucene() throws UpdateFiasDBServiceException {
        try {
            luceneAddressObjectIndexer.reCreateLuceneIndex(routingDataSource.getStaging().getLucene().getLuceneIndexDir());
        } catch (IOException e) {
            throw new UpdateFiasDBServiceException("Lucene index recreate fails: ", e);
        }
        luceneAddressObjectIndexer.readIndexFromDB();

        FiasUpdaterStatus status = statusDao.getActive();
        statusDao.setStatus(LUCENE_IMPORT, status);
    }

    @SneakyThrows
    private void unPack() throws UpdateFiasDBServiceException {
        log.debug("unPack() start on file: " + PATH_TO_ARCHIVE_FILE);
        final File archive = new File(PATH_TO_ARCHIVE_FILE);
        final File destinationFolder = new File(PATH_TO_TMP_EXTRACT_DIRECTORY);
        if (!destinationFolder.isDirectory()) {
            destinationFolder.mkdir();
        }
        File[] files = destinationFolder.listFiles();
        if (null != files && files.length != 0) {
            for (File fi : files) {
                fi.delete();
            }
        }

        String type = new Tika().detect(new File(PATH_TO_ARCHIVE_FILE));
        if (type.equals("application/rar")){
            log.info("RAR archive detected for unpacking");
            // Junrar do not have builtin filtering capabilities. But it is legacy method, so we do not wrap it now and extract all files
            Junrar.extract(archive, destinationFolder);
        }
        else if (type.equals("application/zip")){
            log.info("ZIP archive detected for unpacking");
            ZipUtil.extractToDir(archive, destinationFolder, Pattern.compile("^(SOCRBASE|ADDROB\\d+|HOUSE\\d+)\\.DBF", Pattern.CASE_INSENSITIVE), null);
        }
        else {
            throw new UpdateFiasDBServiceException("Can't detect archive type by file (we support RAR and ZIP)!");
        }

        files = destinationFolder.listFiles();
        if (null == files || 0 == files.length) {
            throw new UpdateFiasDBServiceException("unPack did not create any files");
        }
        FiasUpdaterStatus status = statusDao.getActive();
        statusDao.setStatus(UNPACK, status);
        log.debug("unPack() done");
    }

    /**
     * @param url To download archive of DBFs (ZIP or RAR supported)
     * @param stagesString optional comma-separated values of enum {@see FiasUpdaterState}
     * @param userName
     * @param userLogin @return status of full operation
     */
    public FiasUpdaterStatus update(String url, String stagesString, String userName, String userLogin) {
        List<FiasUpdaterState> steps;
        if (null == stagesString) {
            steps = Arrays.stream(FiasUpdaterState.values()).filter(UPDATE_STEPS::containsKey).collect(Collectors.toList());
        } else {
            steps = Arrays.stream(stagesString.split(","))
                .map(FiasUpdaterState::valueOf)
                .collect(Collectors.toList());
        }
        log.info("Fias database update request started. StagesString: {}, steps: {}, userName: {}, userLogin: {}", stagesString, steps, userName, userLogin);

        //if hasn't block
        FiasUpdaterStatus status = statusDao.getActive();
        if (status != null) {
            String err = format("Operation [%s] now in progress. Refusing start new", status.getStatus());
            log.error(err);
            status.setError(err);
            status.setStatus(ERROR);
            return status;
        }

        status = FiasUpdaterStatus.builder()
            .startDate(new Date())
            .status(steps.get(0))
            .working(true)
            .initialLogin(userLogin)
            .initialName(userName)
            .url(url)
            .switched(false)
            .build();

        statusDao.save(status);
        // final for other thread
        final FiasUpdaterStatus lastStatus = status;
        final UpdateFiasDBService updateFiasDBService = this;
        // task for run with other Thread
        Runnable update = () -> {
            each(
                steps
                ,(step) -> {
                    if (!UPDATE_STEPS.containsKey(step)){
                        log.warn("Step [{}] is not known as executable! Skipping", step);
                    }
                    val entry = UPDATE_STEPS.get(step);
                    log.info("Update. Start step [" + step + "]");
                    try {
                        entry.call(updateFiasDBService, url);
                    } catch (Throwable e) {
                        String error = format("Error happened on execute update step [%s]: %s", step, e.getMessage());
                        log.error(error, e);
                        statusDao.setStatus(ERROR, error, lastStatus);
                        throw new RuntimeException(error, e);
                    }
                }
                ,(Consumer<String>) (log::info)
                ,null
                ,1
            );
            statusDao.setStatus(OK, lastStatus);
        };

        // run task
        executorService.submit(update);
        // current status
        return statusDao.getLastOperationStatus();
    }

    /**
     * @return string version of current FIAS, extracted from update URL
     */
    public JSONObject getCurrentFiasVersion() {
        FiasUpdaterStatus status = statusDao.getLastOkStatus();
        JSONObject ret = new JSONObject();

        Matcher m = fiasVersionFromUpdateUrl.matcher(status.getUrl());
        if (m.matches()){
            ret.put(JSON_FIAS_KEY, m.group(1));
        }
        else{
            ret.put(JSON_FIAS_KEY, "Error get version");
        }
        return ret;
    }
}
