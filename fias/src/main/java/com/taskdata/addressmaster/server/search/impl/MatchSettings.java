/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.impl;

public class MatchSettings {

    final public static MatchSettings DEBUG = new MatchSettings(true, true, 10, false, 10, false);

    final public static MatchSettings DEFAULT = new MatchSettings(false, true, 10, false, 10, false);

    final public static MatchSettings DEFAULT_BATCH = new MatchSettings(false, true, 10, false, 10, true);

    final private boolean fuzzy;

    final private boolean wildCard;

    final private int fuzzyRecordsToReturn;

    final private int wildCardRecordsToReturn;

    final private boolean batchMode;

    final private boolean debugMode;

    private MatchSettings(boolean debugMode, boolean fuzzy, int fuzzyRecordsToReturn, boolean wildCard, int wildCardRecordsToReturn, boolean batchMode) {
        this.debugMode = debugMode;
        this.fuzzy = fuzzy;
        this.fuzzyRecordsToReturn = fuzzyRecordsToReturn;
        this.wildCard = wildCard;
        this.wildCardRecordsToReturn = wildCardRecordsToReturn;
        this.batchMode = batchMode;
    }

    public int getFuzzyRecordsToReturn() {
        return fuzzyRecordsToReturn;
    }

    public int getWildCardRecordsToReturn() {
        return wildCardRecordsToReturn;
    }

    public boolean isBatchMode() {
        return batchMode;
    }

    public boolean isDebugMode() {
        return debugMode;
    }

    public boolean isFuzzy() {
        return fuzzy;
    }

    public boolean isWildCard() {
        return wildCard;
    }
}
