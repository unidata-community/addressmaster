package com.taskdata.addressmaster.server.rest.configuration;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * @link https://coderoad.ru/26804941/Как-я-могу-заставить-JAX-RS-вернуть-свойство-Java-8-LocalDateTime-в-виде-строки
 * @author Pavel Alexeev.
 * @since 2020-11-05 01:53.
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class JacksonContextResolver implements ContextResolver<ObjectMapper> {
	private static final ObjectMapper om = init();

	@Override
	public ObjectMapper getContext(Class<?> objectType) {
		return om;
	}

	private static ObjectMapper init() {
		ObjectMapper om = new ObjectMapper();
		om.registerModule(new JavaTimeModule());
		om.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		return om;
	}
}