/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.model;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.common.JSONConstants;
import com.taskdata.addressmaster.common.model.AddressElement;

public class MatchedToken {

    final private String inputToken;

    final private String foundToken;

    final private AddressElement foundElement;

    final private int distance;

    public MatchedToken(String inputToken, String foundToken, AddressElement foundElement, int distance) {
        this.inputToken = inputToken;
        this.foundToken = foundToken;
        this.distance = distance;
        this.foundElement = foundElement;
    }

    public int getDistance() {
        return distance;
    }

    public AddressElement getFoundElement() {
        return foundElement;
    }

    public String getFoundToken() {
        return foundToken;
    }

    public String getInputToken() {
        return inputToken;
    }

    @Override
    public String toString() {
        return '[' + inputToken + ';' + foundToken + ';' + foundElement.toString() + ';' + distance + ']';
    }

    @SuppressWarnings("unchecked")
    public JSONAware toJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONConstants.JSON_FIELD_INPUT_TOKEN, inputToken);
        jsonObject.put(JSONConstants.JSON_FIELD_FOUND_TOKEN, foundToken);
        jsonObject.put(JSONConstants.JSON_FIELD_ADDRESS_ELEMENT, foundElement.toJSON());
        jsonObject.put(JSONConstants.JSON_FIELD_DISTANCE, distance);
        return jsonObject;
    }
}
