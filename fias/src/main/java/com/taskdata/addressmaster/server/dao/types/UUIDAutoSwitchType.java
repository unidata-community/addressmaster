/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.types;

import java.util.UUID;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.PostgresUUIDType;
import org.hibernate.type.descriptor.java.UUIDTypeDescriptor;
import org.hibernate.type.descriptor.sql.SqlTypeDescriptor;
import org.hibernate.type.descriptor.sql.VarcharTypeDescriptor;
import org.springframework.stereotype.Component;

/**
 * Based on
 * @link https://zorq.net/b/2012/04/21/switching-hibernates-uuid-type-mapping-per-database/
 * but implement configuration from spring automatically!
 *
 * @author David Beaumont
 * @author Pavel Alexeev
 * @since 2017-03-13 23:34.
 * @see org.hibernate.type.PostgresUUIDType
 * @see #SQL_DESCRIPTOR
 */
@Component
public class UUIDAutoSwitchType extends AbstractSingleColumnStandardBasicType<UUID> {

	private static final long serialVersionUID = 902830399800029445L;
	/**
	 * used from xml configuration
	 */
	public static String USED_HIBERNATE_DIALECT;

	/**
	 * PostgresUUIDSqlTypeDescriptor is just default value to do not get NPE in constructor!<br/>
	 * Really some magic happened there:
	 * <ol>
	 *	<li>First time that bean is constructed there no any value from {@link #SQL_DESCRIPTOR}, os it may be any
	 *	<li>Next in Spring configuration (XML in our case) we set `hibernateDialect` property
	 *	<li>Spring call {@link #setHibernateDialect} method and it set <b>static</b> {@link #SQL_DESCRIPTOR}
	 *	<li>Next time that class will be instantiated by Hibernate according to JPA annotations and correct value of
	 * 	{@link #SQL_DESCRIPTOR} will be used!
	 * </ol>
	 *
	 * 	All that technique to allow global configuration once in project, extending solution from:
	 * 	@link https://zorq.net/b/2012/04/21/switching-hibernates-uuid-type-mapping-per-database/
	 */
	private static SqlTypeDescriptor SQL_DESCRIPTOR = PostgresUUIDType.PostgresUUIDSqlTypeDescriptor.INSTANCE;

	/**
	 * first instance from spring config</br>
	 * second instance from hibernate by BeanUtils
	 */
	public UUIDAutoSwitchType() {
		super(SQL_DESCRIPTOR, UUIDTypeDescriptor.INSTANCE);
	}

	/**
	 * That property must be public despite of idea intentions! Because it is used from spring from other package
	 * @see #SQL_DESCRIPTOR
	 */
	public void setHibernateDialect(String hibernateDialect) {
		this.USED_HIBERNATE_DIALECT = hibernateDialect;
		if (USED_HIBERNATE_DIALECT.contains("PostgreSQL")) {
			// There several possible matches like PostgreSQLDialect,
			// PostgreSQL82Dialect…
			SQL_DESCRIPTOR = PostgresUUIDType.PostgresUUIDSqlTypeDescriptor.INSTANCE;
		} else {
			if (USED_HIBERNATE_DIALECT.equals("org.hibernate.dialect.H2Dialect")) {
				SQL_DESCRIPTOR = VarcharTypeDescriptor.INSTANCE;
			} else {
				throw new UnsupportedOperationException("Unsupported database!");
			}
		}
	}

	/**
	 * @see org.hibernate.type.Type#getName()
	 */
	@Override
	public String getName() {
		return "uuid-auto";
	}
}
