/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Builder;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.common.JSONConstants;
import com.taskdata.addressmaster.common.model.AddressElement;

/**
 * debug info for request /AutoCompleteServlet?term=debug:_text
 *
 * @author maria.chistyakova.
 * @link http://jira.taskdata.com/browse/EG-3628
 * @since 2017-04-06 17:43
 */
@Builder
public class DebugInfoObject {
    private MatchStatus matchStatus;
    private int matchScore;
    private float luceneScore;
    private int aggregatedDistance;
    private SearchTypeEnum matchType;
    private List<MatchedToken> matchedTokens;
    private AddressElement addressElement;
    private int highestMatchTokenLevel;

    @Override
    public String toString() {
        return "[" +
                "matchStatus=" + matchStatus + ';' +
                "matchScore=" + matchScore + ';' +
                "luceneScore=" + luceneScore + ';' +
                "aggrDist=" + aggregatedDistance + ';' +
                "matchType=" + matchType + ';' +
                "matchedTokens=" + "\n\n" +
                getMatchedTokensAsString() +
                "highestMatchTokenLevel=" + highestMatchTokenLevel +
                ";]";
    }

    @SuppressWarnings("unchecked")
    public JSONAware toJson() {
        ArrayList<JSONAware> tokens = new ArrayList<>();
        matchedTokens.forEach(matchedToken -> tokens.add(matchedToken.toJson()));

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONConstants.JSON_FIELD_MATCH_STATUS, matchStatus.toString());
        jsonObject.put(JSONConstants.JSON_FIELD_MATCH_SCORE, matchScore);
        jsonObject.put(JSONConstants.JSON_FIELD_LUCENE_SCORE, luceneScore);
        jsonObject.put(JSONConstants.JSON_FIELD_AGGREGATED_DISTANCE, aggregatedDistance);
        jsonObject.put(JSONConstants.JSON_FIELD_MATCH_TYPE, matchType.toString());
        jsonObject.put(JSONConstants.JSON_FIELD_MATCH_TOKENS, tokens);
        jsonObject.put(JSONConstants.JSON_FIELD_HIGHEST_MATCH_TOKEN_LEVEL, highestMatchTokenLevel);


        return jsonObject;
    }

    private String getMatchedTokensAsString() {
        StringBuilder sb = new StringBuilder();
        matchedTokens.forEach(matchedToken -> sb.append(matchedToken.toString()).append(";\n\n"));
        return sb.toString();
    }
}
