/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service.datasource;

import java.net.ConnectException;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbcp2.ConnectionFactory;
import org.apache.commons.dbcp2.PoolableConnectionFactory;

import com.taskdata.addressmaster.server.search.impl.LuceneSearcher;

import static java.lang.Thread.sleep;

/**
 * Besides JDBC-url connection string we need also structured parts of address like host, port and database name to pass
 * them into console script.
 * @TODO drop it after http://jira.taskdata.com/browse/EG-3970
 *
 * Additionally it holds LuceneSearcher instance, which linked to that DB datasource
 *
 * @author Pavel Alexeev
 * @since 03.08.2017
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Slf4j
public class ExtendedDataSource extends BasicDataSource {
    private String scheme = "jdbc:postgresql://";

    private String host;
    private String port;
    private String dbName;
    private String jdbcSettings;

    private RoutingSourceNo sourceNo;

    private LuceneSearcher lucene;

    public void setHost(String host) {
        this.host = host;
        updateUrl();
    }

    public void setPort(String port) {
        this.port = port;
        updateUrl();
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
        updateUrl();
    }

    public void setJdbcSettings(String jdbcSettings) {
        this.jdbcSettings = jdbcSettings;
        updateUrl();
    }

    /**
     * We extend object by parts of URl, so on set it we should remade full URL
     */
    private void updateUrl(){
        setUrl(scheme + host + ":" + port + "/" + dbName + "?" + jdbcSettings);
    }


    @Override
    @SneakyThrows
    protected PoolableConnectionFactory createPoolableConnectionFactory(ConnectionFactory driverConnectionFactory) {
        int attempt = 0;
        while (true){
            try{
                return super.createPoolableConnectionFactory(driverConnectionFactory);
            }
            catch (Exception e){
                if (e.getCause() != null && e.getCause().getCause() instanceof ConnectException){
                    log.warn("Error creating connection: [" + e.getMessage() + "] (attempt " + attempt++ + "). Retry.");
                }
                else {
                    log.warn("Error creating connection (attempt " + attempt++ + "). Retry. Exception:", e);
                }
                sleep(2000);
            }
        }
    }
}
