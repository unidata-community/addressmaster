/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taskdata.addressmaster.common.LuceneHelper;
import com.taskdata.addressmaster.common.Patterns;
import com.taskdata.addressmaster.common.codecs.Encoder;
import com.taskdata.addressmaster.common.util.Pair;

import static com.taskdata.addressmaster.server.search.impl.SearchInputProcessor.HouseNumberParseMode.REMOVE_FROM_ADDRESS_STRING;

@Service
public class SearchInputProcessorImpl implements SearchInputProcessor {

    @Autowired
    private SynonymService synonymService;

    @Autowired
    private AOTypeService aoTypeService;

    static public class AOTypePattern {

        final private String patternString;

        final private String formalTypeName;

        final private Pattern pattern;

        public AOTypePattern(String patternString, String formalTypeName) {
            this.patternString = patternString;
            this.formalTypeName = formalTypeName;
            this.pattern = Pattern.compile('(' + patternString + ')');
        }

        public String getFormalTypeName() {
            return formalTypeName;
        }

        public Pattern getPattern() {
            return pattern;
        }
    }

    static private class AOTypesHelper {

        static final private List<AOTypePattern> separatedAOTypes = new ArrayList<>();

        static {
            separatedAOTypes.add(new AOTypePattern("авт\\sокр", "автономный округ"));
            separatedAOTypes.add(new AOTypePattern("абонентский\\sящик", "абонентский ящик"));
            separatedAOTypes.add(new AOTypePattern("автономная\\sобласть", "автономная область"));
            separatedAOTypes.add(new AOTypePattern("автономный\\sокруг", "автономный округ"));
            separatedAOTypes.add(new AOTypePattern("гаражно\\sстроительный\\sкооператив", "гаражно строительный кооператив"));
            separatedAOTypes.add(new AOTypePattern("дачный\\sпоселок", "дачный поселок"));
            separatedAOTypes.add(new AOTypePattern("железнодорожная\\sплатформа", "железнодорожная платформа"));
            separatedAOTypes.add(new AOTypePattern("железнодорожная\\sстанция", "железнодорожная станция"));
            separatedAOTypes.add(new AOTypePattern("жилой\\sрайон", "жилой район"));
            separatedAOTypes.add(new AOTypePattern("курортный\\sпоселок", "курортный поселок"));
            separatedAOTypes.add(new AOTypePattern("населенный\\sпункт", "населенный пункт"));
            separatedAOTypes.add(new AOTypePattern("промышленная\\sзона", "промышленная зона"));
            separatedAOTypes.add(new AOTypePattern("поселок\\sгородского\\sтипа", "поселок городского типа"));
            separatedAOTypes.add(new AOTypePattern("рабочий\\sпоселок", "рабочий поселок"));
            separatedAOTypes.add(new AOTypePattern("сельский\\sокруг", "сельский округ"));
            separatedAOTypes.add(new AOTypePattern("сельское\\sпоселение", "сельское поселение"));
            separatedAOTypes.add(new AOTypePattern("фермерское\\sхозяйство", "фермерское хозяйство"));
        }

        final private Set<String> extractedFormalAOTypes;

        final private String inputString;

        final private String processedString;

        private AOTypesHelper(String inputString) {
            this.inputString = inputString;
            this.extractedFormalAOTypes = new HashSet<>();

            String currentString = inputString;
            for (AOTypePattern aoTypePattern : separatedAOTypes) {
                Matcher matcher = aoTypePattern.getPattern().matcher(currentString);
                if (matcher.find()) {
                    //String matchedSubString = matcher.group(1);
                    this.extractedFormalAOTypes.add(aoTypePattern.getFormalTypeName());
                    currentString = matcher.replaceFirst("");
                }
            }
            this.processedString = currentString;
        }

        public Set<String> getExtractedFormalAOTypes() {
            return extractedFormalAOTypes;
        }

        public String getProcessedString() {
            return processedString;
        }
    }

    static private class CleanseHelper {

        static final private List<Pattern> patternsToRemove = new ArrayList<>();

        static {
            patternsToRemove.add(Patterns.apartmentPattern); // комната №21
            patternsToRemove.add(Patterns.floorPattern); // 12 эт.
            patternsToRemove.add(Patterns.bracketsPattern); // (фигня в скобках)
            patternsToRemove.add(Patterns.quotationPattern); // "фигня2 в ковычках"
            patternsToRemove.add(Patterns.singleQuotationPattern); // 'фигня2 в ковычках'
            patternsToRemove.add(Patterns.poBoxPattern); // а/я 23
        }

        static public String removeGarbage(String inputString) {
            String currentString = inputString;
            for (Pattern pattern : patternsToRemove) {
                Matcher matcher = pattern.matcher(currentString);
                if (matcher.find()) {
                    currentString = matcher.replaceFirst("");
                }
            }
            return currentString;
        }
    }

    private static final Map<Pattern, String> additions = new HashMap<>();

    static {
        additions.put(Patterns.bStreetPattern, "больш");
        additions.put(Patterns.bExtendedStreetPattern, "б");
        additions.put(Patterns.mStreetPattern, "мал");
        additions.put(Patterns.mExtendedStreetPattern, "м");
    }

    private static void addTokensIfRequired(List<String> initialTokens) {
        ListIterator<String> initialTokensListIterator = initialTokens.listIterator();
        while (initialTokensListIterator.hasNext()) {
            String token = initialTokensListIterator.next();
            for (Map.Entry<Pattern, String> patternStringEntry : additions.entrySet()) {
                if (patternStringEntry.getKey().matcher(token).matches()) {
                    initialTokensListIterator.add(patternStringEntry.getValue());
                }
            }

            Matcher digitMatcher = Patterns.streetNumbers.matcher(token);
            if (digitMatcher.matches()) {
                Integer number = Integer.valueOf(digitMatcher.group(1));
                String alphabeticNumber = LuceneHelper.substitutions.get(number);
                if (alphabeticNumber != null) {
                    initialTokensListIterator.add(alphabeticNumber);
                }
            }
        }
    }

    private static void extractZip(SearchInputImpl searchInput) {
        Matcher matcher = Patterns.ZIP_PATTERN.matcher(searchInput.getInputString());
        if (matcher.find()) {
            String zip = matcher.group(1);
            searchInput.setZipCode(zip);
            searchInput.setInputStringWithoutZip(matcher.replaceFirst(""));
        } else {
            searchInput.setInputStringWithoutZip(searchInput.getInputString());
        }
    }

    private static void produceMetaphoneTokens(SearchInputImpl searchInput) {
        List<String> metaphonedTokens = new ArrayList<>();
        for (String initialToken : searchInput.getInitialTokens()) {
            String metaphonedTerm = Encoder.CUSTOM_RUSSIAN_ENCODER.encodeString(initialToken);
            StringTokenizer stringTokenizer = new StringTokenizer(metaphonedTerm);
            while (stringTokenizer.hasMoreTokens()) {
                metaphonedTokens.add(stringTokenizer.nextToken());
            }
        }

        if (searchInput.getMetaphonedTokens() == null) {
            searchInput.setMetaphonedTokens(metaphonedTokens);
        } else {
            searchInput.getMetaphonedTokens().addAll(metaphonedTokens);
        }
    }

    final private HouseNumberParseMode houseNumberParseMode = REMOVE_FROM_ADDRESS_STRING;

   /* static private List<String> tokenizeByTabSpaces(String inputString) {
        List<String> tokens = new ArrayList<>();
        StringTokenizer stringTokenizer = new StringTokenizer(
                inputString,
                LuceneSearcher.SIMPLE_TOKENIZER_SEPARATORS,
                false
        );

        while (stringTokenizer.hasMoreTokens()) {
            tokens.add(stringTokenizer.nextToken().toLowerCase());
        }

        return tokens;
    }*/

    public SearchInputProcessorImpl() {
    }

    Set<String> extractAOTypes(List<String> initialTokens) {
        Set<String> resultedAOTypeNames = new HashSet<>();

        for (String initialToken : initialTokens) {
            String tokenToSearchFor = initialToken.replaceAll("\\.", "");
            Set<String> fullAOTypeNames = aoTypeService.getNormalizedAOTypeNames(tokenToSearchFor);
            if (fullAOTypeNames != null) {
                resultedAOTypeNames.addAll(fullAOTypeNames);
            }
        }

        return resultedAOTypeNames;
    }

    private Pair<HouseNumberInput, String> extractHouseNumber(String inputString) {
        HouseInputProcessor houseInputProcessor = new HouseInputProcessor(inputString);

        HouseNumberInput houseNumberInput = houseInputProcessor.getBestGuess();
        if (houseNumberInput != null && houseNumberParseMode != HouseNumberParseMode.LEAVE_IN_ADDRESS_STRING) {
            // Let's remove house-number related tokens in case they were explicitly
            // prefixed by labels, such as '???', '??????' etc.

            List<InputToken> tokensToRemove;
            if (houseNumberParseMode == REMOVE_FROM_ADDRESS_STRING) {
                tokensToRemove = houseNumberInput.getAllTokens();
            } else if (houseNumberParseMode == HouseNumberParseMode.REMOVE_ONLY_LABELED_NUMBER) {
                tokensToRemove = new ArrayList<>();
                if (houseNumberInput.getHouseNumberLabel() != null) {
                    tokensToRemove.add(houseNumberInput.getHouseNumberLabel());
                    tokensToRemove.add(houseNumberInput.getHouseNumber());
                }
                if (houseNumberInput.getBuildingNumberLabel() != null) {
                    tokensToRemove.add(houseNumberInput.getBuildingNumberLabel());
                }
                if (houseNumberInput.getBuildingNumber() != null) {
                    tokensToRemove.add(houseNumberInput.getBuildingNumber());
                }
                if (houseNumberInput.getStructureLabel() != null) {
                    tokensToRemove.add(houseNumberInput.getStructureLabel());
                }
                if (houseNumberInput.getStructure() != null) {
                    tokensToRemove.add(houseNumberInput.getStructure());
                }
            } else {
                throw new IllegalStateException("Unknown parse mode: " + houseNumberParseMode);
            }

            if (!tokensToRemove.isEmpty()) {
                StringBuilder stringBuilder = new StringBuilder();
                Collections.sort(tokensToRemove); // Let's make sure that tokens follow in right order
                int currentIndex = 0;
                for (InputToken token : tokensToRemove) {
                    stringBuilder.append(inputString.substring(currentIndex, token.getStartIndex()));
                    currentIndex = token.getEndIndex();
                }
                stringBuilder.append(inputString.substring(currentIndex));
                inputString = stringBuilder.toString();
            }
        }
        return new Pair<>(houseNumberInput, inputString);
    }

    @Override
    public SearchInput handleInput(String inputString, MatchContext matchContext) {
        long startTime = System.currentTimeMillis();
        SearchInputImpl searchInput = new SearchInputImpl(inputString.toLowerCase());

        extractZip(searchInput);
        searchInput.setInputStringWithoutZip(CleanseHelper.removeGarbage(searchInput.getInputStringWithoutZip()));

        // TODO remove dirty chars, like "~"

        Pair<HouseNumberInput, String> houseNumberResult = extractHouseNumber(searchInput.getInputStringWithoutZip());
        searchInput.setHouseNumberInput(houseNumberResult.getFirst());
        searchInput.setInputStringWithoutZip(houseNumberResult.getSecond());

        AOTypesHelper aoTypesHelper = new AOTypesHelper(searchInput.getInputStringWithoutZip());
        searchInput.setInputStringWithoutZip(aoTypesHelper.getProcessedString());

        //List<String> initialTokens = tokenizeByTabSpaces(searchInput.getInputStringWithoutZip());
        String[] split = Patterns.inputElementSeparators.split(searchInput.getInputStringWithoutZip());

        List<String> initialTokens = new ArrayList<>();
        for (String s : split) {
            if (!s.isEmpty()) {
                initialTokens.add(s);
            }
        }
        searchInput.setInitialTokens(initialTokens);

        replaceSynonyms(searchInput);

        Set<String> resultedAOTypeNames = extractAOTypes(searchInput.getInitialTokens());
        addTokensIfRequired(searchInput.getInitialTokens());
        resultedAOTypeNames.addAll(aoTypesHelper.getExtractedFormalAOTypes());
        searchInput.setAddressObjectTypes(resultedAOTypeNames);

        produceMetaphoneTokens(searchInput);

        matchContext.setMeasure(MatchContext.Measure.PARSE_INPUT, System.currentTimeMillis() - startTime);
        return searchInput;
    }

    void replaceSynonyms(SearchInput searchInput) {
        List<String> initialTokens = searchInput.getInitialTokens();
        for (ListIterator<String> iter = initialTokens.listIterator(); iter.hasNext(); ) {
            String initialToken = iter.next();
            String standardMeaning = synonymService.getStandardMeaning(initialToken);
            if (standardMeaning != null) {
                iter.set(standardMeaning);
            }
        }
    }
}
