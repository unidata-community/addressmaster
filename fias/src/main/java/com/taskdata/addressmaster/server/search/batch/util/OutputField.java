/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.batch.util;

/**
 * Created by 502261152 on 2/11/14.
 */
public enum OutputField {
    FIAS_ADDRESS(0),
    FIAS_VERDICT(1),
    FIAS_MATCH_SCORE(2),
    FIAS_AOID(3),
    FIAS_GUID(4),
    FIAS_HOUSE_ID(5),
    FIAS_HOUSE_GUID(6),
    FIAS_HOUSE_STRENGTH(7),
    FIAS_AGGR_DISTANCE(8),
    FIAS_MATCHED_TOKENS(9),
    FIAS_ADDRESS_ZIP(10),
    FIAS_HOUSE_ZIP(11),
    FIAS_HOUSE_H_NUM(12),
    FIAS_HOUSE_B_NUM(13),
    FIAS_HOUSE_STR(14),
    FIAS_LUCENE_SCORE(15),
    FIAS_HOUSE_RANGE_ID(16),
    FIAS_HOUSE_RANGE_GUID(17),
    FIAS_HOUSE_RANGE_ZIP(18),
    FIAS_HOUSE_RANGE_START(19),
    FIAS_HOUSE_RANGE_END(20),
    FIAS_CITY(21);

    static public String[] getFieldNames() {
        String[] names = new String[getNumberOfFields()];
        int index = 0;
        for (OutputField outputField : values()) {
            names[index++] = outputField.name();
        }
        return names;
    }

    static public int getNumberOfFields() {
        return values().length;
    }

    final public int index;

    OutputField(int index) {
        this.index = index;
    }
}
