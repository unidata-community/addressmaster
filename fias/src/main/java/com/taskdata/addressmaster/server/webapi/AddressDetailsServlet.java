/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.webapi;

import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.taskdata.addressmaster.server.event.RequestEvent;
import com.taskdata.addressmaster.server.search.model.AddressDetailsObject;
import com.taskdata.addressmaster.server.service.AddressService;
import com.taskdata.addressmaster.server.service.HouseService;

/**
 * @author denis.vinnichek
 */
public class AddressDetailsServlet extends AddressMasterServlet {

    private static final String ADDRESS_ID_PARAM = "addressId";

    private static final String HOUSE_ID_PARAM = "houseId";

    private static final String ADDRESS_DETAILS_OBJ_NAME = "ADDRESS_DETAILS";

    @Autowired
    private AddressService addressService;

    @Autowired
    private HouseService houseService;

    @Override
    protected RequestResult doGetInternal(HttpServletRequest request, HttpServletResponse response)
        throws UnsupportedEncodingException {

        String aoIdsStr = getDecodedParameter(request, ADDRESS_ID_PARAM);
        String[] aoIds = aoIdsStr.split(" ");

        String aoId = aoIdsStr.length() > 0 ? aoIds[aoIds.length - 1] : aoIdsStr;
        String houseId = getDecodedParameter(request, HOUSE_ID_PARAM);

        AddressDetailsObject addressDetails;
        if (!houseId.trim().isEmpty()) {
            addressDetails = houseService.getAddressDetails(houseId);
        } else {
            addressDetails = addressService.getAddressDetails(aoId);
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ADDRESS_DETAILS_OBJ_NAME, addressDetails.toJSON());

        RequestEvent.Builder requestEventBuilder = new RequestEvent.Builder(request,
            RequestEvent.RequestEventType.GET_ADDRESS_DETAILS);
        //todo 2017.04.05 aoIds must be aoGUIDs now
        requestEventBuilder.setUserInput("aoIds: " + aoIdsStr + "; houseId: " + houseId)
            .setData(jsonObject.toJSONString());

        return new RequestResult(jsonObject, requestEventBuilder);
    }
}
