/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.data.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * todo 2017.03.06 темна вода в облацех. мб расширение AddressObject для вау-эффекта
 */
@Entity(name= "AddressObjectEx")
@Table(name = "FE_ADDRESS_OBJECT_EX")
public class AddressObjectEx implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fe_address_object_ex_aoe_id_seq")
    @SequenceGenerator(name = "fe_address_object_ex_aoe_id_seq", sequenceName = "fe_address_object_ex_aoe_id_seq", allocationSize = 1)
    @Column(name = "aoe_id", nullable = false)
    private int id;

    @Column(name = "aoe_name", length = 50, nullable = false)
    private String name;

    @OneToOne
    @JoinColumn(name = "aoid", nullable = false, referencedColumnName = "aoid")
    private AddressObject addressObject;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddressObjectEx that = (AddressObjectEx) o;

        return id == that.id;
    }

    public AddressObject getAddressObject() {
        return addressObject;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public void setAddressObject(AddressObject addressObject) {
        this.addressObject = addressObject;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
