/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.data.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * SOCRBASE (AddressObjectType) – содержит перечень полных,
 * сокращённых наименований типов адресных элементов и уровней их классификации.
 * <p>
 * shortName / full name objects lookup.
 * Entry like 0766 | ул. | улица | 7
 * KOD_T_ST may be deprecated in future(?)
 * for search SOCRNAME use SCNAME + LEVEL from AddressObject
 */
@Entity(name = "AddressObjectType")
@Table(name = "F_ADDRESS_OBJECT_TYPE"/*,
        uniqueConstraints = @UniqueConstraint(
                name = "AOT_LEVEL_SCNAME",
                columnNames = {"LEVEL", "SCNAME"} )*/)
public class AddressObjectType implements Serializable {

    @Id
    @Column(name = "KOD_T_ST", length = 4, nullable = false)
    private String id;

    @Column(name = "SCNAME", length = 10)
    private String shortName;

    @Column(name = "SOCRNAME", length = 50, nullable = false)
    private String name;

    @Column(name = "LEVEL", length = 10, nullable = false)
    private int level;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddressObjectType that = (AddressObjectType) o;

        return id.equals(that.id);
    }

    public String getId() {
        return id;
    }

    public int getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
