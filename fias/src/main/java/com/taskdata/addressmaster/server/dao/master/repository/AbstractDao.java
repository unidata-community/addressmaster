/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.master.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public abstract class AbstractDao<E, Id> {

    private static void setQueryParams(Query q, Object[] params) {
        int i = 1;
        for (Object param : params) {
            q.setParameter(i, param);
            i++;
        }
    }

    private EntityManager em;

    public abstract E getById(Id id);

    protected EntityManager getEM() {
        return em;
    }

    public List<Object[]> nativeQuery(String query, Object[] params, int firstResult, int maxResults) {
        Query q = getEM().createNativeQuery(query);
        q.setFirstResult(firstResult);
        q.setMaxResults(maxResults);
        setQueryParams(q, params);
        return q.getResultList();
    }

    public List<E> query(String query, Object[] params, int maxResults) {
        Query q = getEM().createQuery(query);
        q.setMaxResults(maxResults);
        setQueryParams(q, params);
        return q.getResultList();
    }

    public List<E> query(String query, Object[] params, int firstResult, int maxResults) {
        Query q = getEM().createQuery(query);
        q.setFirstResult(firstResult);
        q.setMaxResults(maxResults);
        setQueryParams(q, params);
        return q.getResultList();
    }

    public Object querySingle(String query, Object... params) {
        Query q = getEM().createQuery(query);
        setQueryParams(q, params);
        return q.getSingleResult();
    }

    public int update(String query, Object... params) {
        Query q = getEM().createQuery(query);
        setQueryParams(q, params);
        return q.executeUpdate();
    }

    public Object[] querySingleFields(String query, Object[] params) {
        Query q = getEM().createQuery(query);
        setQueryParams(q, params);
        return (Object[]) q.getSingleResult();
    }

    public abstract void save(E entity);

    @PersistenceContext(unitName="com.taskdata.addressmaster.server.master.jpa")
    public void setEM(EntityManager em) {
        this.em = em;
    }
}
