/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.webapi;

import java.io.IOException;
import java.util.Date;
import java.util.Objects;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;

import com.taskdata.addressmaster.server.event.RequestEvent;
import com.taskdata.addressmaster.server.service.IAddressValidateService;
import com.taskdata.addressmaster.server.service.RequestAuditService;

/**
 * Сервлет валидации адреса в ФИАС
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/RR-514
 * @since 22.03.18
 */
@Slf4j
@Getter
@Setter
public class AddressValidateServlet extends AddressMasterServlet {

    @Autowired
    private IAddressValidateService addressValidateService;
    @Autowired
    private RequestAuditService requestAuditService;

    private final JSONParser jsonParser = new JSONParser();

    /**
     * Читаем JSON объект из тела запроса
     *
     * @param request - Запрос содержащий даные
     * @return - json объект
     * @throws IOException - Ошибка чтения тела запроса
     */
    private JSONObject getRequestPayload(HttpServletRequest request) throws IOException {
        Objects.requireNonNull(request, "HttpServletRequest is required");
        try (val reader = request.getReader()) {
            val json = jsonParser.parse(reader);
            if (json instanceof JSONObject) {
                return (JSONObject) json;
            } else {
                throw new ParseException(-1, "Is not JSON object");
            }
        } catch (ParseException ex) {
            throw new IOException("Illegal request payload", ex);
        }
    }

    /**
     * Обработка POST запроса
     *
     * Тело запроса не обязательно в GET запросах, поэтому по феншую предпочтительней POST запросы
     * в REST приложениях
     *
     * @param req - hhtp request
     * @param resp - http response
     * @throws ServletException - process exception
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        long requestStartDate = System.currentTimeMillis();
        try {
            RequestResult requestResult = doGetInternal(req, resp);
            long requestEndDate = System.currentTimeMillis();
            RequestEvent requestEvent = requestResult.getRequestEventBuilder()
                .setRequestDate(new Date(requestStartDate))
                .setRequestExecTime(requestEndDate - requestStartDate)
                .build();
            requestAuditService.onNewRequest(requestEvent);
            writeJsonToResponse(requestResult.getJson(), resp);
        } catch (Throwable e) {
            log.error("Error in servlet doPost():", e);
            throw new ServletException(e);
        }
    }

    @Override
    protected RequestResult doGetInternal(HttpServletRequest request, HttpServletResponse resp) throws Exception {
        val requestPayload = getRequestPayload(request);
        log.debug("Request payload: {}", requestPayload);
        val resultPayload = addressValidateService.validate(requestPayload);
        log.debug("Result payload: {}", resultPayload);
        RequestEvent.Builder requestEventBuilder = new RequestEvent
            .Builder(request, RequestEvent.RequestEventType.REST);
        if (resultPayload == null) {
            throw new IllegalStateException("Null result value");
        }
        return new RequestResult(resultPayload, requestEventBuilder);
    }
}
