/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.data.entity;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.taskdata.addressmaster.server.dao.types.UUIDAutoSwitchType;

/**
 * Pojo для объекта дома
 */
@Entity(name = "HouseObject")
@Table(name = "house_all")
@EqualsAndHashCode(of = {"id"})
@TypeDef(
    name = "uuid-auto",
    defaultForType = UUID.class,
    typeClass = UUIDAutoSwitchType.class
)
@Data
public class HouseObject implements IFiasAddressNode {

    @Id
    @Column(name = "HOUSEID", nullable = false)
    @Type(type = "uuid-auto")
    private UUID id;
    @Column(name = "HOUSEGUID", length = 36)
    @Type(type = "uuid-auto")
    private UUID guid;
    @Column(name = "NORMDOC", length = 36)
//    @Type(type = "uuid-auto") // В БД, оно указано как VARCHAR, хотя по содержани UUID
    private String normDoc;

    @OneToOne
    @JoinColumn(name = "AOGUID", referencedColumnName = "AOGUID")
    private AddressObject parent;

    @Column(name = "STRUCNUM", length = 10)
    private String structNum;
    @Column(name = "HOUSENUM", length = 10)
    private String houseNum;
    @Column(name = "POSTALCODE", length = 6)
    private String postalCode;

    @Column(name = "IFNSFL", length = 4)
    private String ifnsfl;
    @Column(name = "IFNSUL", length = 4)
    private String ifnsul;
    @Column(name = "TERRIFNSFL", length = 4)
    private String terrIfnsfl;
    @Column(name = "TERRIFNSUL", length = 4)
    private String terrIfnsul;

    @Column(name = "CADNUM", length = 100)
    private String cadnum;
    @Column(name = "OKATO", length = 11)
    private String okato;
    @Column(name = "OKTMO", length = 11)
    private String oktmo;

    @Column(name = "STARTDATE")
    private Date startDate;
    @Column(name = "UPDATEDATE")
    private Date updateDate;

    @Column(name = "ESTSTATUS", length = 36)
    private int estStatus;
    @Column(name = "STATSTATUS", length = 5)
    private int statStatus;
    @Column(name = "STRSTATUS", length = 1)
    private int strStatus;

    @Column(name = "COUNTER", length = 4)
    private int counter;
    @Column(name = "DIVTYPE", length = 2)
    private int divType;

}
