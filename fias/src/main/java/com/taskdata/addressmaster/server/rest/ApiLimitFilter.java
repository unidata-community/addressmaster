/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.rest;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

// TODO extenalize quotas
@Provider
public class ApiLimitFilter implements ContainerRequestFilter {

    public static final long ONE_MINUTE = 60000;

    private final int numOfAllowedRequests = 60; // 60 API calls in a minute limit

    @Context
    private HttpServletRequest request;

    private final Map<String, Ticket> accessMap = new HashMap<>();// TODO Handle concurency

    @Override
    public void filter(ContainerRequestContext containerRequestContext) {
        String remoteHost = request.getRemoteHost();
        Ticket ticket = accessMap.get(remoteHost);
        if (ticket == null) {
            ticket = new Ticket();
            ticket.increment();
            accessMap.put(remoteHost, ticket);
        } else {
            Long issueTime = ticket.getIssueTime();
            long now = System.currentTimeMillis();
            if (now - issueTime > ONE_MINUTE) {
                ticket.reset();
                ticket.increment();
            } else {
                if (ticket.getApiCalls() >= numOfAllowedRequests) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ignore) {
                    }
                    throw new WebApplicationException(Response.Status.PAYMENT_REQUIRED);
                }
                ticket.increment();
            }
        }
    }
}

