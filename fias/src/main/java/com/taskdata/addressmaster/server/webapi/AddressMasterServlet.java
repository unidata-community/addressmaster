/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.webapi;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.CharEncoding;
import org.json.simple.JSONAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.taskdata.addressmaster.server.event.RequestEvent;
import com.taskdata.addressmaster.server.service.RequestAuditService;

/**
 * @author denis.vinnichek
 */
@Slf4j
public abstract class AddressMasterServlet extends HttpServlet {

    public static class RequestResult {

        @Getter
        private final JSONAware json;

        private final RequestEvent.Builder requestEventBuilder;

        RequestResult(JSONAware json, RequestEvent.Builder requestEventBuilder) {
            this.json = json;
            this.requestEventBuilder = requestEventBuilder;
        }

        public RequestEvent.Builder getRequestEventBuilder() {
            return requestEventBuilder;
        }
    }

    static String getDecodedParameter(HttpServletRequest request, String paramName) throws UnsupportedEncodingException {
        String paramStr = request.getParameter(paramName);
        if (null == paramStr) return null;
        return URLDecoder.decode(paramStr, CharEncoding.UTF_8);
    }

    static void writeJsonToResponse(JSONAware json, HttpServletResponse response) throws IOException {
        response.setContentType("application/json; charset=" + CharEncoding.UTF_8);
        response.setCharacterEncoding(CharEncoding.UTF_8);

        PrintWriter out = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), CharEncoding.UTF_8), true);
        out.println(json.toString());
        out.close();
    }

    @Autowired
    private RequestAuditService requestAuditService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try {
            long requestStartDate = System.currentTimeMillis();
            RequestResult requestResult = doGetInternal(req, resp);
            long requestEndDate = System.currentTimeMillis();

            RequestEvent requestEvent = requestResult.getRequestEventBuilder()
                .setRequestDate(new Date(requestStartDate))
                .setRequestExecTime(requestEndDate - requestStartDate)
                .build();
            requestAuditService.onNewRequest(requestEvent);

            writeJsonToResponse(requestResult.json, resp);
        } catch (Throwable e) {
            log.error("Error in servlet doGet():", e);
            throw new ServletException(e);
        }
    }

    protected abstract RequestResult doGetInternal(HttpServletRequest req, HttpServletResponse resp) throws Exception;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }
}
