/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.quota;

import java.util.HashMap;
import java.util.Map;

import com.taskdata.addressmaster.server.event.RequestEvent;
import com.taskdata.addressmaster.server.event.RequestEventHandler;

public class RequestCounter implements RequestEventHandler {

    private final Map<String, Integer> requestCount = new HashMap<>();

    public RequestCounter() {
    }

    public int getByAddress(String address) {
        return requestCount.get(address);
    }

    @Override
    public void onNewRequest(RequestEvent event) {
        Integer count = requestCount.get(event.getSourceAddress());
        if (count == null) {
            count = 0;
        }
        count++;
        requestCount.put(event.getSourceAddress(), count);
    }

    public void resetByAddress(String address) {
        if (address == null) {
            return;
        }
        requestCount.remove(address);
    }

}
