/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service.validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Stack;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taskdata.addressmaster.server.dao.data.entity.IFiasAddressNode;
import com.taskdata.addressmaster.server.dao.data.entity.Socrbase;
import com.taskdata.addressmaster.server.dao.data.repository.AddressObjectDao;
import com.taskdata.addressmaster.server.dao.data.repository.HouseObjectDao;
import com.taskdata.addressmaster.server.dao.data.repository.SocrbaseDao;
import com.taskdata.addressmaster.server.service.IAddressValidateService;
import com.taskdata.addressmaster.server.service.validator.exceptions.MoreThanOneObjectFound;
import com.taskdata.addressmaster.server.service.validator.exceptions.ObjectNotExistException;

import static com.taskdata.addressmaster.server.service.validator.formatters.AddressObjectFormatter.addressObjectFormatter;
import static com.taskdata.addressmaster.server.service.validator.formatters.HouseObjectFormatter.houseObjectFormatter;
import static com.taskdata.addressmaster.server.service.validator.search.SearchService.searchService;
import static org.apache.commons.lang.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang.StringUtils.equalsIgnoreCase;

/**
 * Сервис валидации адреса (проверки сущестования)
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/RR-514
 * @since 23.03.18
 */
@Slf4j
@Service
@Getter
@Setter
public class AddressValidateService implements IAddressValidateService {


    private static final String ID_PROPERTY = "id";

    private static final String AOUID_PROPERTY = "aouid";

    private static final String VALUE_PROPERTY = "value";

    private static final String TYPE_PROPERTY = "type";

    private static final String NUMBER_PROPERTY = "number";

    private static final String PARENT_PROPERTY = "parent";

    private static final String POSTCODE_PROPERTY = "postcode";

    private static final String VALID_PROPERTY = "valid";

    private static final String MESSAGE_PROPERTY = "message";

    private static final String DETAILS_PROPERTY = "addressDetails";

    private static final String FIRST_ADDRESS_NOT_FOUND = "Объект '%s' не найден";

    private static final String ADDRESS_NOT_FOUND = "Объект '%s' не найден в '%s' (почтовый индекс: %s, уровень: %s)";

    private static final String MORE_THAN_ONE_FOUND = "Найдено несколько объектов по адресу, неоходимо заполнить почтовый индекс в запросе";

    private static final String ILLEGAL_REQUEST = "Некорректные данные запроса: '%s'";

    private static final List<IFiasNodeFormatter> DEFAULT_FORMATTERS = ImmutableList
        .of(addressObjectFormatter(), houseObjectFormatter());

    @Autowired
    private AddressObjectDao addressObjectDao;
    @Autowired
    private HouseObjectDao houseObjectDao;
    @Autowired
    private SocrbaseDao socrbaseDao;

    private List<IFiasNodeFormatter> fiasNodeFormatters = DEFAULT_FORMATTERS;

    @Getter(lazy = true)
    private final List<ISearchNodeService> searchNodeServices = initSearchNodeServices();
    @Getter(lazy = true)
    private final List<Socrbase> socrbaseList = loadSocrbaseList();

    private final JSONParser parser = new JSONParser();
    private final Gson gson = new Gson();

    private List<Socrbase> loadSocrbaseList() {
        return socrbaseDao == null ? Collections.emptyList() : socrbaseDao.selectAllValues();
    }

    private List<ISearchNodeService> initSearchNodeServices() {
        return ImmutableList.of(
            searchService("AddressObject", addressObjectDao,
                ImmutableMap.of(ID_PROPERTY, "guid", VALUE_PROPERTY, "formalName",
                    PARENT_PROPERTY, "parent.guid", TYPE_PROPERTY, "shortName")
            ),
            searchService("HouseObject", houseObjectDao,
                ImmutableMap.of(ID_PROPERTY, "guid", VALUE_PROPERTY, "houseNum",
                    PARENT_PROPERTY, "parent.guid", NUMBER_PROPERTY, "structNum"))
        );
    }

    private JSONObject readAsJsonObject(JSONObject source, String fieldName) {
        Objects.requireNonNull(source, "Source json is required");
        Object fieldValue = source.get(fieldName);
        JSONObject result = null;
        if (fieldValue instanceof JSONObject) {
            result = (JSONObject) fieldValue;
        } else if (fieldValue != null) {
            log.warn("Unexpected value: '" + fieldName + "' = {}", fieldValue);
        }
        return result;
    }

    private JSONObject readLevelObject(JSONObject source, int index) {
        return readAsJsonObject(source, "level" + index);
    }

    private Optional<String> readStringProperty(JSONObject object, String propertyName) {
        return Optional.ofNullable(object.get(propertyName)).flatMap(id -> {
            String result = null;
            val str = String.valueOf(id);
            if (StringUtils.isNotBlank(str)) {
                result = str;
            }
            return Optional.ofNullable(result);
        });
    }

    private Optional<UUID> readUID(JSONObject object) {
        return Optional.ofNullable(object.get(AOUID_PROPERTY)).flatMap(id -> {
            UUID result = null;
            val str = String.valueOf(id);
            if (StringUtils.isNotBlank(str)) {
                try {
                    result = UUID.fromString(str);
                } catch (Throwable ex) {
                    throw new IllegalArgumentException("is not UUID: " + str);
                }
            }
            return Optional.ofNullable(result);
        });
    }

    private Optional<String> readNumber(JSONObject object) {
        return readStringProperty(object, NUMBER_PROPERTY);
    }

    private Optional<Set<String>> readObjectType(JSONObject object) {
        return readStringProperty(object, TYPE_PROPERTY).flatMap(type -> {
            Set<String> result = null;
            if (StringUtils.isNotBlank(type)) {
                val socrs = getSocrbaseList();
                result = Sets.newHashSet(type);
                for (val each : socrs) {
                    if (equalsIgnoreCase(each.getName(), type)
                        || equalsIgnoreCase(each.getShortName(), type)) {
                        result.add(each.getName());
                        result.add(each.getShortName());
                    }
                }
            }
            return Optional.ofNullable(result);
        });
    }

    private IFiasAddressNode objectByParams(String postalCode, Map<String, Object> params) {
        val search = getSearchNodeServices();
        val iter = search.iterator();
        Optional<IFiasAddressNode> selector = Optional.empty();
        while (!selector.isPresent() && iter.hasNext()) {
            val next = iter.next();
            val found = next.findByParameters(params);
            if (!found.isEmpty()) {
                int count = found.size();
                if (count > 1) {
                    if (StringUtils.isNotBlank(postalCode)) {
                        val filtered = found.stream()
                            .filter(candidate -> equalsIgnoreCase(candidate.getPostalCode(), postalCode))
                            .collect(Collectors.toList());
                        val filteredCount = filtered.size();
                        if (filteredCount > 1) {
                            throw new MoreThanOneObjectFound(postalCode, filtered);
                        } else if (filteredCount == 1) {
                            selector = Optional.of(filtered.get(0));
                        } else {
                            selector = Optional.empty();
                        }
                    } else {
                        throw new MoreThanOneObjectFound(found);
                    }
                } else {
                    selector = Optional.of(found.get(0));
                }
            }
        }
        return selector.orElseThrow(() -> new ObjectNotExistException(params));
    }

    private String concatAddressParts(Stack<IFiasAddressNode> path) {
        StringBuilder result = new StringBuilder();
        val list = new LinkedList<>(path);
        list.forEach(each -> {
            for (val frmt : fiasNodeFormatters) {
                if (frmt.accept(each)) {
                    result.append(' ').append(frmt.format(each));
                    break;
                }
            }
        });
        return StringUtils.trim(result.toString());
    }

    private String formatErrorMessage(int level, String postalCode, Stack<IFiasAddressNode> path, Map<String, Object> last) {
        StringBuilder part = new StringBuilder();
        Optional.ofNullable(last.get(TYPE_PROPERTY)).ifPresent(type -> {
            part.append(' ');
            if (type instanceof Iterable) {
                part.append(Iterables.getFirst((Iterable<?>) type, StringUtils.EMPTY));
            } else {
                part.append(type);
            }
        });
        part.append(' ').append(last.getOrDefault(VALUE_PROPERTY, StringUtils.EMPTY));
        part.append(' ').append(last.getOrDefault(NUMBER_PROPERTY, StringUtils.EMPTY));
        val where = concatAddressParts(path);
        if (StringUtils.isNotBlank(where)) {
            return String.format(ADDRESS_NOT_FOUND, part.substring(1), where, postalCode, level);
        } else {
            return String.format(FIRST_ADDRESS_NOT_FOUND, part.substring(1));
        }
    }

    private JSONObject toJsonObject(Object object) {
        Objects.requireNonNull(object, "Object is required");
        try {
            return (JSONObject) parser.parse(gson.toJson(object));
        } catch (Throwable ex) {
            log.error("Convert error", ex);
            throw new IllegalStateException("Unexpected service error", ex);
        }
    }

    private List<JsonObject> buildAddressList(Stack<IFiasAddressNode> path, List<IFiasAddressNode> nodes) {
        List<JsonObject> result;
        if (nodes != null && !nodes.isEmpty()) {
            val prefix = concatAddressParts(path);
            result = new ArrayList<>();
            nodes.forEach(it -> {
                val addressDetails = new JsonObject();
                addressDetails.add("addressDetails", gson.toJsonTree(it));
                gson.toJsonTree(it).getAsJsonObject();
                val readable = new StringBuilder(prefix);
                for (val frmt : fiasNodeFormatters) {
                    if (frmt.accept(it)) {
                        readable.append(' ').append(frmt.format(it));
                        break;
                    }
                }
                addressDetails.addProperty("readableAddress", readable.toString());
                result.add(addressDetails);
            });
        } else {
            result = Collections.emptyList();
        }
        return result;
    }

    @Override
    public JSONObject validate(JSONObject address) {
        Objects.requireNonNull(address, "Address is required");
        val result = new HashMap<String, Object>();
        result.put(VALID_PROPERTY, Boolean.FALSE);
        String postalCode = readStringProperty(address, POSTCODE_PROPERTY).orElse(StringUtils.EMPTY);
        val stack = new Stack<IFiasAddressNode>();
        val index = new AtomicInteger();
        try {
            JSONObject selector;
            Object lastFound = null;
            val params = new HashMap<String, Object>();
            while (Objects.nonNull(selector = readLevelObject(address, index.getAndIncrement()))) {
                log.debug("Lookup level object: {}", selector);
                readUID(selector).ifPresent(uid -> params.put(AOUID_PROPERTY, uid));
                readObjectType(selector).ifPresent(shortName -> params.put(TYPE_PROPERTY, shortName));
                readNumber(selector).ifPresent(number -> params.put(NUMBER_PROPERTY, number));
                params.put(VALUE_PROPERTY, defaultIfNull(selector.get(VALUE_PROPERTY), StringUtils.EMPTY));
                if (!stack.isEmpty()) {
                    val parent = stack.peek();
                    params.put(PARENT_PROPERTY, parent.getGuid());
                }
                val found = objectByParams(postalCode, params);
                lastFound = found;
                log.debug("Found: {}", found);
                stack.push(found);
                params.clear();
            }
            result.put(VALID_PROPERTY, Boolean.TRUE);
            Optional.ofNullable(lastFound).ifPresent(obj -> result.put(DETAILS_PROPERTY, toJsonObject(obj)));
        } catch (ObjectNotExistException notFound) {
            result.put(MESSAGE_PROPERTY, formatErrorMessage(index.get(), postalCode, stack, notFound.getAttributes()));
        } catch (MoreThanOneObjectFound mex) {
            result.put(MESSAGE_PROPERTY, mex.getLocalizedMessage());
            result.put("addresses", buildAddressList(stack, mex.getAddressNodes()));
        } catch (Throwable ex) {
            log.error("Unexpected validate exception", ex);
            result.put(MESSAGE_PROPERTY, String.format(ILLEGAL_REQUEST, ex.getMessage() + "; " + (null != ex.getCause() ? ex.getCause().getCause() : "")));
        }
        return new JSONObject(result);
    }

}
