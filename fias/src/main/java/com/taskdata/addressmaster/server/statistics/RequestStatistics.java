/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.statistics;

import javax.persistence.Column;
import javax.persistence.Id;

//@Entity
//@Table(name = "REQ_STATS")
public class RequestStatistics {

    @Id
    @Column(name = "STATS_ID", nullable = false)
    private int id;

    /**
     * @see com.taskdata.addressmaster.server.event.RequestEvent.RequestEventType
     */
    @Column(name = "REQ_TYPE", nullable = false)
    private int requestType;

    @Column(name = "TOTAL", nullable = false)
    private long total;

    @Column(name = "FAILED", nullable = false)
    private long failed;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RequestStatistics that = (RequestStatistics) o;

        if (id != that.id) return false;

        return true;
    }

    public long getFailed() {
        return failed;
    }

    public int getId() {
        return id;
    }

    public int getRequestType() {
        return requestType;
    }

    public long getTotal() {
        return total;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public void incFailed() {
        ++this.failed;
    }

    public void incTotal() {
        ++this.total;
    }

    public void setFailed(long failed) {
        this.failed = failed;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}
