/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.webapi.api;

import java.text.SimpleDateFormat;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.server.dao.master.entity.FiasUpdaterStatus;

import static com.taskdata.addressmaster.common.JSONConstants.*;

/**
 * @author maria.chistyakova
 * @since 27.03.2017.
 */
@Data
@NoArgsConstructor
public class HistoryResponse {

    @Data
    @NoArgsConstructor
    public class HistoryElement {

        private Integer id;
        private String version;
        private String date;
        private String state;
        private String login;
        private String name;
        private String error;

        HistoryElement(FiasUpdaterStatus status) {
            this.version = "V" + formatVersion.format(status.getStartDate());
            this.date = format.format(status.getStartDate());
            switch (status.getStatus()) {
                case ERROR:
                    this.state = "ошибка";
                    break;
                case OK:
                    this.state = "успешно";
                    break;
                default:
                    this.state = "в процессе";
                    break;
            }
            this.login = status.getInitialLogin();
            this.name = status.getInitialName();
            this.id = status.getId();
            this.error = status.getError();
        }

        JSONObject toJson() {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(JSON_FIELD_VERSION, version);
            jsonObject.put(JSON_FIELD_DATE, date);
            jsonObject.put(JSON_FIELD_STATE, state);
            jsonObject.put(JSON_FIELD_UPDATE_NUMBER, id);
            jsonObject.put(JSON_FIELD_USER_NAME, name);
            jsonObject.put(JSON_FIELD_USER_LOGIN, login);
            jsonObject.put(JSON_FIELD_ERROR, error);
            return jsonObject;
        }
    }

    SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy hh.mm");

    SimpleDateFormat formatVersion = new SimpleDateFormat("ddMMyyyy");

    private JSONArray history;

    public HistoryResponse(List<FiasUpdaterStatus> list) {
        JSONArray history = new JSONArray();
        for (FiasUpdaterStatus status : list) {
            history.add((new HistoryElement(status)).toJson());
        }
        this.history = history;
    }

    public JSONObject toJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSON_FIELD_HISTORY, history);
        return jsonObject;
    }
}
