/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service.validator.exceptions;

import java.util.List;

import lombok.Getter;
import org.apache.commons.lang.StringUtils;

import com.taskdata.addressmaster.server.dao.data.entity.IFiasAddressNode;

/**
 * Ошибка возникающая в случае если по переданным параметрам найдено несколько адресов, вместо одного
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/AD-124
 * @since 21.05.19
 **/
@Getter
public class MoreThanOneObjectFound extends RuntimeException {

    private static final String EMPTY_POSTAL_CODE_MESSAGE = "Найдено несколько объектов по адресу (%s), неоходимо заполнить почтовый индекс в запросе";

    private static final String WITH_POSTAL_CODE_MESSAGE = "Найдено несколько объектов по адресу (%s)";

    private final String postalCode;
    private final List<IFiasAddressNode> addressNodes;

    public MoreThanOneObjectFound(List<IFiasAddressNode> addressNodes) {
        super(String.format(EMPTY_POSTAL_CODE_MESSAGE, addressNodes.size()));
        this.addressNodes = addressNodes;
        this.postalCode = StringUtils.EMPTY;
    }

    public MoreThanOneObjectFound(String postalCode, List<IFiasAddressNode> addressNodes) {
        super(String.format(WITH_POSTAL_CODE_MESSAGE, addressNodes.size()));
        this.addressNodes = addressNodes;
        this.postalCode = postalCode;
    }


}
