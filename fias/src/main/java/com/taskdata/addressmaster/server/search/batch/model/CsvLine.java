/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.batch.model;

/**
 * @author denis.vinnichek
 */
public abstract class CsvLine {

    public static final char DEFAULT_DELIMITER = ';';

    private static String constructLineValue(String[] lineItems, char itemsDelimiter) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < lineItems.length; i++) {
            result.append(lineItems[i]);
            if (i < lineItems.length - 1) {
                result.append(itemsDelimiter);
            }
        }
        return result.toString();
    }

    final String[] lineItems;

    private final char itemsDelimiter;

    private final String lineValue;

    CsvLine(CsvLine firstCsvLine, CsvLine secondCsvLine) {
        String[] lineItems = new String[firstCsvLine.lineItems.length + secondCsvLine.lineItems.length];
        System.arraycopy(firstCsvLine.lineItems, 0, lineItems, 0, firstCsvLine.lineItems.length);
        System.arraycopy(secondCsvLine.lineItems, 0, lineItems, firstCsvLine.lineItems.length, secondCsvLine.lineItems.length);

        this.lineItems = lineItems;
        this.itemsDelimiter = firstCsvLine.itemsDelimiter;
        this.lineValue = constructLineValue(this.lineItems, this.itemsDelimiter);
    }

    CsvLine(String[] lineItems) {
        this(lineItems, DEFAULT_DELIMITER);
    }

    CsvLine(String[] lineItems, char itemsDelimiter) {
        this.itemsDelimiter = itemsDelimiter;
        this.lineItems = lineItems;
        this.lineValue = constructLineValue(this.lineItems, this.itemsDelimiter);
    }

    public char getItemsDelimiter() {
        return itemsDelimiter;
    }

    public String[] getLineItems() {
        return lineItems.clone();
    }

    public String getLineValue() {
        return lineValue;
    }
}
