/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.statistics;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.taskdata.addressmaster.server.event.RequestEvent;
import com.taskdata.addressmaster.server.event.RequestEventHandler;
import com.taskdata.addressmaster.server.event.RequestEventService;

@Slf4j
public class RequestStatisticsService implements RequestEventHandler, InitializingBean {

    private final RequestStatisticsHolder statistics;

    @Autowired
    private RequestEventService requestEventService;

    public RequestStatisticsService() {
        statistics = new RequestStatisticsHolder();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.debug("Initializing...");
        requestEventService.addHandler(this);
        log.debug("Initializing complete.");
    }

    public RequestStatisticsHolder getStatistics() {
        return statistics;
    }

    @Override
    public void onNewRequest(RequestEvent event) {
        log.debug("Gathering statistics from new request event: " + event);
        statistics.onNewRequest(event);
    }
}
