/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.batch.reader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.concurrent.BlockingQueue;

import au.com.bytecode.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import com.taskdata.addressmaster.server.search.batch.IInputStreamSource;
import com.taskdata.addressmaster.server.search.batch.model.CsvLine;
import com.taskdata.addressmaster.server.search.batch.model.DataLine;
import com.taskdata.addressmaster.server.search.batch.model.HeaderLine;
import com.taskdata.addressmaster.server.search.batch.model.LastDataLine;

/**
 * @author denis.vinnichek
 */
@Slf4j
public class BatchReader implements Runnable {

    public static final String FILE_ENCODING = "UTF8";

    private static String constructWrongLengthMsg(String[] rowItems, HeaderLine headerLine) {
        StringBuilder sb = new StringBuilder()
            .append("Length of parsed line not equal to header length.")
            .append("\n\trowItems.length = ")
            .append(rowItems.length)
            .append("\n\theaderItems.length = ")
            .append(headerLine.getLineItems().length)
            .append("\n\trowItems content = [");

        for (int i = 0; i < rowItems.length; i++) {
            sb.append(rowItems[i]);
            if (i < rowItems.length - 1) {
                sb.append(headerLine.getItemsDelimiter());
            }
        }

        sb.append(']')
            .append("\n\theaderItems content = [")
            .append(headerLine.getLineValue())
            .append(']');

        return sb.toString();
    }

    private final IInputStreamSource inputSource;

    private final BlockingQueue<DataLine> dataQueue;

    private final int linesToReadNum;

    public BatchReader(IInputStreamSource inputSource, BlockingQueue<DataLine> dataQueue, int linesToReadNum) {
        this.inputSource = inputSource;
        this.dataQueue = dataQueue;
        this.linesToReadNum = linesToReadNum;
    }

    @Override
    public void run() {
        int linesCounter = 0;
        CSVReader csvReader = null;
        try (Reader streamReader = new InputStreamReader(inputSource.openInputStream(), FILE_ENCODING)){
            csvReader = new CSVReader(streamReader, CsvLine.DEFAULT_DELIMITER);
            String[] rowItems;
            HeaderLine headerLine = new HeaderLine(csvReader.readNext());
            while ((rowItems = csvReader.readNext()) != null) {
                if (rowItems.length != headerLine.getLineItems().length) {
                    log.error(constructWrongLengthMsg(rowItems, headerLine));
                    continue;
                }

                DataLine dataLine = new DataLine(rowItems, headerLine);
                dataQueue.put(dataLine);
                if (linesToReadNum > 0 && ++linesCounter >= linesToReadNum) {
                    break;
                }
            }
        } catch (IOException e) {
            log.error("Exception during running CsvReader.", e);
        } catch (Exception e) {
            log.error("Exception during running Batch Reader.", e);
        } finally {
            log.info("Successfully parsed " + linesCounter + " lines.");
            try {
                dataQueue.put(LastDataLine.getInstance());
                if (csvReader != null) {
                    csvReader.close();
                }
            } catch (InterruptedException e) {
                log.error("Can't put the last data line into queue.", e);
            } catch (IOException e) {
                log.error("Can't close CsvReader.", e);
            }
        }
    }
}
