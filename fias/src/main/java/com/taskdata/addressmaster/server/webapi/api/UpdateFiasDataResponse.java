/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.webapi.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.common.JSONConstants;
import com.taskdata.addressmaster.server.dao.master.entity.FiasUpdaterState;

/**
 * @author maria.chistyakova. Created on 17.03.2017.
 */
@Data
@AllArgsConstructor
public class UpdateFiasDataResponse {
    private FiasUpdaterState status;
    private String error;

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put(JSONConstants.JSON_FIELD_STATUS, status.toString());
        // if status != ERROR -> JSON_FIELD_ERROR = ""
        json.put(JSONConstants.JSON_FIELD_ERROR, error);
        return json;
    }
}
