/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.quota;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;

//@Entity
//@Table(name = "REQ_QUOTA")
public class RequestQuota implements Serializable {

    @Id
    @Column(name = "QUOTA_ID", nullable = false)
    private int id;

    @Column(name = "QUOTA_SIZE", nullable = false)
    private int quota;

    /**
     * @see com.taskdata.addressmaster.server.quota.RequestQuotaUnits
     */
    @Column(name = "QUOTA_UNIT", nullable = false)
    private int quotaUnits;

    @Column(name = "CRITERIA", length = 100, nullable = false)
    private String criteria;

    @Column(name = "TIMEOUT", nullable = false)
    private long timeoutMillis;

    /**
     * @see com.taskdata.addressmaster.server.quota.RequestQuotaSource
     */
    @Column(name = "SOURCE", nullable = false)
    private int sourceType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RequestQuota quota = (RequestQuota) o;

        if (id != quota.id) return false;

        return true;
    }

    public String getCriteria() {
        return criteria;
    }

    int getId() {
        return id;
    }

    public int getQuota() {
        return quota;
    }

    public int getQuotaUnits() {
        return quotaUnits;
    }

    public int getSourceType() {
        return sourceType;
    }

    public long getTimeoutMillis() {
        return timeoutMillis;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setQuota(int quota) {
        this.quota = quota;
    }

    public void setQuotaUnits(int quotaUnits) {
        this.quotaUnits = quotaUnits;
    }

    public void setSourceType(int sourceType) {
        this.sourceType = sourceType;
    }

    public void setTimeoutMillis(long timeoutMillis) {
        this.timeoutMillis = timeoutMillis;
    }

    @Override
    public String toString() {
        return "RequestQuota:" + "id=" + id + ',' + "regex=" + criteria + ',' + "sourceType=" + sourceType + ',' + "quota=" + quota + ',' + "units=" + quotaUnits;
    }
}
