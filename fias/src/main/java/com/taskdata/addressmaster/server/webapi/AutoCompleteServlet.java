/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.webapi;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.taskdata.addressmaster.server.event.RequestEvent;
import com.taskdata.addressmaster.server.search.impl.MatchContext;
import com.taskdata.addressmaster.server.search.impl.MatchSettings;
import com.taskdata.addressmaster.server.search.model.SearchItemContainer;
import com.taskdata.addressmaster.server.service.AddressMatcherService;

@Slf4j
public class AutoCompleteServlet extends AddressMasterServlet {

    private static final String TERM_PARAM = "term";

    private static final String DEBUG_PREFIX = "debug:";

    private static final int RESULT_ITEM_NUM_LIMIT = 10;

    @Autowired
    private AddressMatcherService addressMatcherService;

    @Override
    protected RequestResult doGetInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (request.getRequestURI().endsWith("metadata")) {
            return handleMetadataRequest(request);
        } else {
            return handleTermQueryRequest(request);
        }
    }

    private RequestResult handleMetadataRequest(HttpServletRequest request) {
        JSONObject result = new JSONObject(addressMatcherService.getMetadata());
        RequestEvent.Builder requestEventBuilder = new RequestEvent.Builder(request, RequestEvent.RequestEventType.METADATA);
        return new RequestResult(result, requestEventBuilder);
    }

    private RequestResult handleTermQueryRequest(HttpServletRequest request) throws Exception {
        String term = getDecodedParameter(request, TERM_PARAM);

        log.info("User provided: " + term);

        MatchSettings matchSettings;
        if (term.startsWith(DEBUG_PREFIX)) {
            matchSettings = MatchSettings.DEBUG;
            term = term.substring(DEBUG_PREFIX.length());
        } else {
            matchSettings = MatchSettings.DEFAULT;
        }

        MatchContext matchContext = new MatchContext(matchSettings);
        SearchItemContainer container = addressMatcherService.match(term, matchContext, RESULT_ITEM_NUM_LIMIT, null);

        RequestEvent.Builder requestEventBuilder = new RequestEvent.Builder(request, RequestEvent.RequestEventType.AUTO_COMPLETE);
        requestEventBuilder.setUserInput(term)
            .setRows(container.getItemsNum())
            .setEventMetric(container.getBestScore())
            .setData(container.getAoidsString());
        return new RequestResult(container.getJSONObject(matchContext), requestEventBuilder);
    }
}
