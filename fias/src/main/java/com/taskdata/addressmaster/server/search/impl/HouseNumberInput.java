/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.impl;

import java.util.ArrayList;
import java.util.List;

public class HouseNumberInput {

    private InputToken houseNumberLabel;
    private InputToken houseNumber;

    private InputToken buildingNumberLabel;
    private InputToken buildingNumber;

    private InputToken structureLabel;
    private InputToken structure;

    public HouseNumberInput() {
    }

    public List<InputToken> getAllTokens() {
        List<InputToken> tokens = new ArrayList<>();

        if (houseNumberLabel != null) {
            tokens.add(houseNumberLabel);
        }

        if (houseNumber != null) {
            tokens.add(houseNumber);
        }

        if (buildingNumberLabel != null) {
            tokens.add(buildingNumberLabel);
        }

        if (buildingNumber != null) {
            tokens.add(buildingNumber);
        }

        if (structureLabel != null) {
            tokens.add(structureLabel);
        }

        if (structure != null) {
            tokens.add(structure);
        }
        return tokens;
    }

    public InputToken getBuildingNumber() {
        return buildingNumber;
    }

    public InputToken getBuildingNumberLabel() {
        return buildingNumberLabel;
    }

    public InputToken getHouseNumber() {
        return houseNumber;
    }

    public InputToken getHouseNumberLabel() {
        return houseNumberLabel;
    }

    public String getNormalizedString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (houseNumber != null) {
            stringBuilder.append("д. ").append(houseNumber).append(' ');
        }
        if (buildingNumber != null) {
            stringBuilder.append("к. ").append(buildingNumber).append(' ');
        }
        if (structure != null) {
            stringBuilder.append("лит. ").append(structure).append(' ');
        }
        return stringBuilder.toString();
    }

    public InputToken getStructure() {
        return structure;
    }

    public InputToken getStructureLabel() {
        return structureLabel;
    }

    public boolean isFilled() {
        return houseNumber != null && buildingNumber != null && structure != null;
    }

    public boolean isValid() {
        return houseNumber != null;
    }

    public void normalize() {
        if (structure == null) {
            structureLabel = null;
        }

        if (buildingNumber == null) {
            buildingNumberLabel = null;
        }
    }

    public void reset() {
        houseNumber = null;
        buildingNumber = null;
        structure = null;
        houseNumberLabel = null;
        buildingNumberLabel = null;
        structureLabel = null;
    }

    public void setBuildingNumber(InputToken buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public void setBuildingNumberLabel(InputToken buildingNumberLabel) {
        this.buildingNumberLabel = buildingNumberLabel;
    }

    public void setHouseNumber(InputToken houseNumber) {
        this.houseNumber = houseNumber;
    }

    public void setHouseNumberLabel(InputToken houseNumberLabel) {
        this.houseNumberLabel = houseNumberLabel;
    }

    public void setStructure(InputToken structure) {
        this.structure = structure;
    }

    public void setStructureLabel(InputToken structureLabel) {
        this.structureLabel = structureLabel;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (houseNumberLabel != null) {
            stringBuilder.append(houseNumberLabel).append(' ');
        }
        if (houseNumber != null) {
            stringBuilder.append(houseNumber).append(' ');
        }
        if (buildingNumberLabel != null) {
            stringBuilder.append(buildingNumberLabel).append(' ');
        }
        if (buildingNumber != null) {
            stringBuilder.append(buildingNumber).append(' ');
        }
        if (structureLabel != null) {
            stringBuilder.append(structureLabel).append(' ');
        }
        if (structure != null) {
            stringBuilder.append(structure).append(' ');
        }
        return stringBuilder.toString();
    }
}
