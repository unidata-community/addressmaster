/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.master.repository;

import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.taskdata.addressmaster.server.dao.master.entity.FiasUpdaterState;
import com.taskdata.addressmaster.server.dao.master.entity.FiasUpdaterStatus;

@Service
public class FiasUpdaterStatusDao extends AbstractDao<FiasUpdaterStatus, Integer> {

    //todo 2017.03.10 moved for hibernate named query
    private static final String SQL_SELECT_FROM_FIAS_UPDATER_STATUS_F_WHERE_F_WORKING_1 = "SELECT f FROM FiasUpdaterStatus f WHERE f.working = true";
    private static final String SQL_SELECT_LAST = "SELECT f FROM FiasUpdaterStatus f ORDER BY f.id DESC";
    private static final String SQL_SELECT_LAST_OK = "SELECT f FROM FiasUpdaterStatus f WHERE status = 'OK' ORDER BY f.id DESC";
    private static final String SQL_SELECT_HISTORY = "SELECT f FROM FiasUpdaterStatus f ORDER BY f.id DESC";

    /**
     * @return active update status with .working=1 if present. Null otherwise.
     */
    public FiasUpdaterStatus getActive() {
        try {
            return (FiasUpdaterStatus) querySingle(SQL_SELECT_FROM_FIAS_UPDATER_STATUS_F_WHERE_F_WORKING_1);
        } catch (NoResultException ignore) {
            return null;
        }
    }

    /**
     * Acc active operation will be failed. This primary to call on startup
     */
    @Transactional(value = "transactionManagerMaster")
    public void failAllActive(){
        update("UPDATE FiasUpdaterStatus s SET s.working = false, s.error = 'Fail stalled tasks on application start' WHERE s.working = true");
    }

    @Override
    public FiasUpdaterStatus getById(Integer id) {
        return getEM().find(FiasUpdaterStatus.class, id);
    }

    public List<FiasUpdaterStatus> getHistory() {
        return query(SQL_SELECT_HISTORY, new String[]{}, 100);
    }

    public FiasUpdaterStatus getLastOperationStatus() {
        List<FiasUpdaterStatus> list = query(SQL_SELECT_LAST, new String[]{}, 0, 1);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    /**
     * Return last status with "OK" = fus_status. We threat it as "Switched now". For example to get version from that
     * @TODO rework status management http://jira.taskdata.com/browse/EG-4891, http://jira.taskdata.com/browse/EG-4916
     */
    public FiasUpdaterStatus getLastOkStatus(){
        return (FiasUpdaterStatus)getEM().createQuery(SQL_SELECT_LAST_OK).setMaxResults(1).getSingleResult();
    }

    @Override
    @Transactional(value = "transactionManagerMaster")
    public void save(FiasUpdaterStatus entity) {
        getEM().persist(entity);
        getEM().flush();
    }

    @Transactional(value = "transactionManagerMaster")
    public void setStatus(FiasUpdaterState state, FiasUpdaterStatus status) {
        status.setStatus(state);
        if (state == FiasUpdaterState.OK) {
            status.setWorking(false);
            status.setEndDate(new Date());
        }
        update(status);
    }

    @Transactional(value = "transactionManagerMaster")
    public void setStatus(FiasUpdaterState i, String message, FiasUpdaterStatus status) {
        status.setStatus(i);
        status.setEndDate(new Date());
        status.setWorking(false);
        status.setError(message);
        update(status);
    }

    @Transactional(value = "transactionManagerMaster")
    public void update(FiasUpdaterStatus entity) {
        getEM().merge(entity);
        getEM().flush();
    }
}
