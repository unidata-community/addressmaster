/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.service.datasource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.taskdata.addressmaster.server.service.DataSourceSwitchService;
import com.taskdata.addressmaster.server.service.exception.UpdateFiasDBServiceException;


/**
 * Service implementing main logic of switching connections to work with different sources
 * @link http://jira.taskdata.com/browse/EG-3851
 *
 * Now we should in runtime swithch by request appropriate pair of data sources: database and lucene index.
 * That switch must be done atomicity and perform some validity checks before.
 *
 * Current implemented scheme are:
 * <dl>
 *  <dt>master database</dt>
 *      <dd>main DB which holds metadata, current settings, users and configuration</dd>
 *  <dt>staging</dt>
 *      <dd>DB and Lucene index pair which is not used now as source for search addresses and rather on stage of
 *      updating and reparing to be next version. Updating include importing new FIAS data and produce appropriate Lucene
 *      search index for it</dd>
 *  <dt>current</dt>
 *      <dd>version of DB and Lucene index which are used currently for perform search and live operation</dd>
 * </dl>
 *
 * @author Maria Chistyakova
 * @author PAvel Alexeev
 * @since 15.03.2017
 */
@Slf4j
public class RoutingDataSource extends AbstractRoutingDataSource{

    /**
     * Will be set on initialisation in {@link DataSourceSwitchService#restoreCurrentSourcesState()}
     */
    private RoutingSourceNo currentNo;

    /**
     * On change current source we also reopen directories on new active {@link com.taskdata.addressmaster.server.search.impl.LuceneSearcher}
     *
     * Java has no concept of Friend-classes like in C++, so be aware that method must be called only from
     * {@link DataSourceSwitchService} class.
     *
     * @param currentNo to change. Do nothing if it equals to current
     */
    public void setCurrentNo(RoutingSourceNo currentNo, DataSourceSwitchService.SwitchKey key) throws UpdateFiasDBServiceException {
        key.hashCode(); // "Friend-key"
        if (!currentNo.equals(this.currentNo)){
            this.currentNo = currentNo;
            getCurrent().getLucene().openDirectories();
        }
    }

    /**
     * Primary method for get key of current DB connection source
     * Implementation spring dataSource router decides problem with change hibernate connection
     * decision from http://howtodoinjava.com/spring/spring-orm/spring-3-2-5-abstractroutingdatasource-example/
     *
     * @return current db connection name
     */
    @Override
    synchronized protected Object determineCurrentLookupKey() {
        return currentNo;
    }

    /**
     * @return current active dataSource to perform operative search queries
     */
    public synchronized ExtendedDataSource getCurrent(){
        return (ExtendedDataSource)determineTargetDataSource();
    }

    /**
     * Unfortunately extended {@link AbstractRoutingDataSource} has no getters to obtain opposite source.
     * So we change current key temporary and do it only in synchronized methods!
     *
     * @return Staging dataSource for perform updates
     */
    public synchronized ExtendedDataSource getStaging(){
        RoutingSourceNo currentCurrent = currentNo;
        currentNo = currentNo.opposite();
        ExtendedDataSource ret = (ExtendedDataSource) determineTargetDataSource();
        currentNo = currentCurrent;
        return ret;
    }
}
