/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.quota;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.ServletRequest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.taskdata.addressmaster.server.event.RequestEvent;
import com.taskdata.addressmaster.server.event.RequestEventHandler;
import com.taskdata.addressmaster.server.event.RequestEventService;

@Slf4j
public class RequestQuotaService implements RequestEventHandler, InitializingBean {

    //todo 2017.03.10 moved for hibernate named query
    private static final String SQL_SELECT_T_FROM_REQUEST_QUOTA = "select t from RequestQuota";

    private final RequestQuotaDao requestQuotaDao;

    @Autowired
    private RequestEventService requestEventService;

    private final RequestCounter counter;

    private final RequestBlockList blockList;

    public RequestQuotaService(RequestQuotaDao requestQuotaDao) {
        this.requestQuotaDao = requestQuotaDao;
        this.counter = new RequestCounter();
        blockList = new RequestBlockList();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.debug("Initializing...");
        requestEventService.addHandler(this);
        log.debug("Initializing complete.");
    }

    public boolean canDoRequest(ServletRequest request) {
        log.trace("Checking quota for the request" + request.toString());
        String remoteAddress = request.getRemoteAddr();
        if (blockList.isAddressBlocked(remoteAddress)) {
            log.debug("Remote address " + remoteAddress + "is blocked");
            return false;
        }

        List<RequestQuota> quotas = requestQuotaDao.query(SQL_SELECT_T_FROM_REQUEST_QUOTA, new Object[]{}, 1000);
        Collection<RequestQuota> matchedQuotas = new ArrayList<>();
        for (RequestQuota quota : quotas) {
            Pattern pattern = Pattern.compile(quota.getCriteria());
            if (RequestQuotaSource.IP_ADDR == RequestQuotaSource.find(quota.getSourceType())
                && pattern.matcher(remoteAddress).matches()) {
                log.trace("Found quota for IP address " + remoteAddress + ": " + quota);
                matchedQuotas.add(quota);
            }
        }

        for (RequestQuota quota : matchedQuotas) {
            if (RequestQuotaUnits.REQUESTS == RequestQuotaUnits.find(quota.getQuotaUnits())
                && quota.getQuota() > counter.getByAddress(remoteAddress)) {
                log.trace("Applying quota " + quota + " for IP address " + remoteAddress);
                counter.resetByAddress(remoteAddress);
                blockList.blockAddress(remoteAddress, quota.getTimeoutMillis());
                return false;
            }
        }

        return true;
    }

    @Override
    public void onNewRequest(RequestEvent event) {
        counter.onNewRequest(event);
    }
}
