/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.search.batch;

import java.io.IOException;
import java.io.InputStream;

/**
 * Источник данных на основани для проверки данных
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/AD-49
 * @since 04.07.18
 */
public interface IInputStreamSource {

    /**
     * Открыть поток чтения данных
     * @return поток чтения
     * @throws IOException ошибка при открытии потока
     */
    InputStream openInputStream() throws IOException;

    /**
     * Возвращает имя файла
     * @return имя файла
     */
    String getFileName();

}
