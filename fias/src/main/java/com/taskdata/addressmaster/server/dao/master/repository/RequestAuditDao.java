/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.master.repository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.taskdata.addressmaster.server.dao.master.entity.RequestAudit;

@Service
public class RequestAuditDao extends AbstractDao<RequestAudit, Integer> {

    public RequestAuditDao() {
        super();
    }

    @Override
    public RequestAudit getById(Integer id) {
        return getEM().find(RequestAudit.class, id);
    }

    @Override
    @Transactional(value = "transactionManagerMaster", propagation = Propagation.REQUIRED)
    public void save(RequestAudit entity) {
        getEM().persist(entity);
        getEM().flush();
    }
}
