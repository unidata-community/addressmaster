/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.server.dao.master.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity(name = "FiasUpdaterStatus")
@Table(name = "S_FIAS_UPDATER_STATUS")
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class FiasUpdaterStatus implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_fias_updater_status_fus_id_seq")
    @SequenceGenerator(name = "s_fias_updater_status_fus_id_seq", sequenceName = "s_fias_updater_status_fus_id_seq", allocationSize = 1)
    @Column(name = "FUS_ID", nullable = false)
    private int id;

    @Column(name = "FUS_VERSION", nullable = false)
    private String version;

    @Column(name = "FUS_ERROR")
    private String error;

    @Column(name = "FUS_WORKING", nullable = false)
    private Boolean working;

    @Enumerated(EnumType.STRING)
    @Column(name = "FUS_STATUS", nullable = false)
    private FiasUpdaterState status;

    @Column(name = "FUS_INITIAL_LOGIN")
    private String initialLogin;

    @Column(name = "FUS_INITIAL_NAME")
    private String initialName;

    @Column(name = "FUS_START_DATE", nullable = false)
    private Date startDate;

    @Column(name = "FUS_END_DATE")
    private Date endDate;

    /**
     * url like  http://fias.nalog.ru/Public/..../fias_dbf.rar
     */
    @Column(name = "FUS_URL", nullable = false)
    private String url;

    @Column(name = "FUS_SWITCHED", nullable = false)
    private Boolean switched;
}
