package com.taskdata.addressmaster.server.rest.pojo;

import java.time.ZonedDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Pavel Alexeev.
 * @since 2020-11-05 00:42.
 */
@NoArgsConstructor
@AllArgsConstructor
public class Pong {
	@Getter
	ZonedDateTime pong = ZonedDateTime.now();
}
