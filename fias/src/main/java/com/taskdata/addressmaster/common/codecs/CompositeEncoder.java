/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.common.codecs;

import java.util.Arrays;
import java.util.List;

public class CompositeEncoder implements Encoder {

    final private List<Encoder> encoders;

    public CompositeEncoder(Encoder[] encoders) {
        this.encoders = Arrays.asList(encoders);
    }

    public String encodeString(String input) {
        String result = input;
        for (Encoder encoder : encoders) {
            result = encoder.encodeString(result);
        }
        return result;
    }

}
