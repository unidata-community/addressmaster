/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.common.util;

/**
 * Copyright (c) 2011-2013 TaskData LLC
 */
public class StringUtil {

    public static final String SPACE = " ";

    public static final String EMPTY = "";

    public static boolean isEmpty(String s) {
        return s == null || s.length() == 0;
    }

    public static boolean isNormalized(String s) {
        // If string does not start with capital letter
        if (!Character.isUpperCase(s.charAt(0))) {
            return false;
        }
        for (int i = 1; i < s.length(); ++i) {
            if (Character.isUpperCase(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static String normalize(String s) {
        if (isEmpty(s)) {
            return s;
        }

        // If string starts with capital letter
        if (isNormalized(s)) {
            return s;
        }
        StringBuilder builder = new StringBuilder();
        builder.append(Character.toUpperCase(s.charAt(0)));
        for (int i = 1; i < s.length(); ++i) {
            builder.append(Character.toLowerCase(s.charAt(i)));
        }

        return builder.toString();
    }
}
