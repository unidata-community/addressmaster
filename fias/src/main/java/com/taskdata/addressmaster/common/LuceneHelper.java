/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.common;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

public class LuceneHelper {

    //static public final String FIELD_MODIFIED = "modified";

    static public final String FIELD_FOBJECT_CONTENT = "content";

    static public final String FIELD_FOBJECT_METAPHONE = "metaphone";

    static public final String FIELD_FOBJECT_ZIPCODES = "zipCodes";

    static public final String FIELD_FOBJECT_LEAF_AOID = "leafAOID";

    static public final String FIELD_FOBJECT_LEAF_GUID = "leafGUID";

    static public final String FIELD_FOBJECT_AOTYPE = "aoType";

    static public final String FIELD_FHOUSE_GUID = "houseGuid";

    static public final String FIELD_FHOUSE_ID = "houseId";

    static public final String FIELD_FHOUSE_ZIP = "houseZip";

    static public final String FIELD_FHOUSE_AOGUID = "houseAOGUID";

    static public final String FIELD_FULL_HOUSE = "fullHouse";

    static public final String FIELD_HOUSE_NUMERIC_PART = "houseNumericPart";

    static public final String FIELD_FHOUSE_NUMBER_STRING = "houseNumberString";

    static public final String FIELD_FHOUSE_NUMBER = "houseNumber";

    static public final String FIELD_FBUILDING_NUMBER = "buildingNumber";

    static public final String FIELD_FSTRUCTURE = "structure";

    static public final String FIELD_FSTRUCTURE_STATUS = "structureStatus";

    static public final float POSTAL_CODE_SEARCH_BOOST = 2.0f;

    static public final int MAX_HOUSE_NUMBER = 10000;

    public static final Map<Integer, String> substitutions = new HashMap<>();

    static {
        substitutions.put(1, "перв");
        substitutions.put(2, "втор");
        substitutions.put(3, "трет");
        substitutions.put(4, "четверт");
        substitutions.put(5, "пят");
        substitutions.put(6, "шест");
        substitutions.put(7, "седьм");
        substitutions.put(8, "восьм");
        substitutions.put(9, "девят");
        substitutions.put(10, "десят");
        substitutions.put(11, "одиннадцат");
        substitutions.put(12, "двенадцат");
    }

    public static String addNumericStrings(String addressToIndex) {
        Matcher matcher = Patterns.streetNumbers4Index.matcher(addressToIndex);
        while (matcher.find()) {
            String group1 = matcher.group(1);
            String group2 = matcher.group(2);
            String number = substitutions.get(Integer.valueOf(group1));
            if (number != null) {
                addressToIndex += (' ' + number + group2 + ' ');
            }
        }
        return addressToIndex.trim();
    }

    public static String splitHyphens(String addressToIndex) {
        Matcher matcher = Patterns.hyphenInBigWords.matcher(addressToIndex);
        while (matcher.find()) {
            String group = matcher.group();
            addressToIndex += " ";
            String[] split = group.split("-");
            for (int i = 0; i < split.length; i++) {
                addressToIndex += split[i];
                if (i < split.length - 1) {
                    addressToIndex += " ";
                }
            }
        }
        return addressToIndex;
    }
}
