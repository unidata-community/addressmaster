/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.common;

public class JSONConstants {
    public static final String JSON_ADDRESSES_KEY = "addressList";
    public static final String JSON_HOUSES_KEY = "HOUSES";
    public static final String JSON_FIAS_KEY = "FIAS_VERSION";
    public static final String JSON_APP_BUILD_KEY = "APP_BUILD";


    public static final String JSON_FIELD_MATCH_STATUS = "MATCH_STATUS";
    public static final String JSON_FIELD_ADDRESS_ID = "ADDRESS_ID";
    public static final String JSON_FIELD_ADDRESS_GUID = "ADDRESS_GUID";
    public static final String JSON_FIELD_ZIPCODE = "ZIPCODE";
    public static final String JSON_FIELD_ADDRESS_INFO = "ADDRESS_INFO";
    public static final String JSON_FIELD_HOUSE_INFO = "HOUSE_INFO";
    public static final String JSON_FIELD_HOUSE_RANGE_INFO = "HOUSE_RANGE_INFO";
    public static final String JSON_FIELD_DEBUG_INFO = "DEBUG_INFO";
    public static final String JSON_FIELD_DEBUG_INFO_OBJECT = "DEBUG_INFO_OBJECT";
    public static final String JSON_FIELD_HOUSE_NUMBER = "HOUSE_NUMBER";
    public static final String JSON_FIELD_BUILDING_NUMBER = "BUILDING_NUMBER";
    public static final String JSON_FIELD_STRUCTURE = "STRUCTURE";
    public static final String JSON_FIELD_STRUCTURE_NUM = "STRUCTURE_NUM";
    public static final String JSON_FIELD_STRUCTURE_STATUS = "STRUCTURE_STATUS";
    public static final String JSON_FIELD_STRUCTURE_STATUS_CODE = "STRUCTURE_STATUS_CODE";
    public static final String JSON_FIELD_STRUCTURE_STATUS_LONG_DESC = "STRUCTURE_STATUS_LONG_DESC";
    public static final String JSON_FIELD_STRUCTURE_STATUS_SHORT_DESC = "STRUCTURE_STATUS_SHORT_DESC";
    public static final String JSON_FIELD_OKATO = "OKATO";
    public static final String JSON_FIELD_OKTMO = "OKTMO";
    public static final String JSON_FIELD_IFNSFL = "IFNSFL";
    public static final String JSON_FIELD_TERR_IFNSFL = "TERR_IFNSFL";
    public static final String JSON_FIELD_IFNSUL = "IFNSUL";
    public static final String JSON_FIELD_FIAS_ID = "FIAS_ID";
    public static final String JSON_FIELD_FIAS_GUID = "FIAS_GUID";
    public static final String JSON_FIELD_KLADR = "KLADR";
    public static final String JSON_FIELD_elementTypeName = "elementTypeName";
    public static final String JSON_FIELD_LEVEL = "level";
    public static final String JSON_FIELD_FORMAL_NAME = "formalName";
    public static final String JSON_FIELD_AO_ID = "aoId";
    public static final Object JSON_FIELD_AO_GUID = "aoGuid";
    public static final String JSON_FIELD_ALL_POSTAL_CODES = "allPostalCodes";
    public static final String JSON_FIELD_ZIP_CODE = "zipCode";
    public static final String JSON_FIELD_HOUSE_ID = "HOUSE_ID";
    public static final String JSON_FIELD_HOUSE_GUID = "HOUSE_GUID";
    public static final String JSON_FIELD_HOUSE_RANGE_GUID = "HOUSE_RANGE_GUID";
    public static final String JSON_FIELD_HOUSE_RANGE_ID = "HOUSE_RANGE_ID";
    public static final String JSON_FIELD_RANGE_START = "RANGE_START";
    public static final String JSON_FIELD_RANGE_END = "RANGE_END";
    public static final String JSON_FIELD_RANGE_TYPE = "RANGE_TYPE";


    public static final String JSON_FIELD_STATUS = "STATUS";
    public static final String JSON_FIELD_ERROR = "ERROR";

    public static final String JSON_FIELD_HISTORY = "HISTORY";
    public static final String JSON_FIELD_VERSION = "VERSION";
    public static final String JSON_FIELD_UPDATE_NUMBER = "UPDATE_NUMBER";
    public static final String JSON_FIELD_USER_NAME = "USER_NAME";
    public static final String JSON_FIELD_USER_LOGIN = "USER_LOGIN";
    public static final String JSON_FIELD_DATE = "DATE";
    public static final String JSON_FIELD_STATE = "STATE";


    public static final String JSON_FIELD_STATUS_LOOKUP = "STATE_LOOKUP";

    //for DebugInfoObject.class
    public static final String JSON_FIELD_MATCH_SCORE = "MATCH_SCORE";
    public static final String JSON_FIELD_LUCENE_SCORE = "LUCENE_SCORE ";
    public static final String JSON_FIELD_AGGREGATED_DISTANCE = "AGGREGATED_DISTANCE";
    public static final String JSON_FIELD_MATCH_TYPE = "MATCH_TYPE";
    public static final String JSON_FIELD_MATCH_TOKENS = "MATCH_TOKENS";
    public static final String JSON_FIELD_HIGHEST_MATCH_TOKEN_LEVEL = "HIGHEST_MATCH_TOKEN_LEVEL";
    public static final String JSON_FIELD_INPUT_TOKEN = "INPUT_TOKEN";
    public static final String JSON_FIELD_FOUND_TOKEN = "FOUND_TOKEN";
    public static final String JSON_FIELD_ADDRESS_ELEMENT = "ADDRESS_ELEMENT";
    public static final String JSON_FIELD_DISTANCE = "DISTANCE";

    public static final String JSON_FIELD_OVERALL_TIME = "OVERALL_TIME";
    public static final String JSON_FIELD_TIME_MEASUREMENT = "TIME_MEASUREMENT";


}
