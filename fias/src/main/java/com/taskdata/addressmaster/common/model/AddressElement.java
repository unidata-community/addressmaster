/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.common.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import lombok.Data;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.common.JSONConstants;
import com.taskdata.addressmaster.common.codecs.Encoder;
import com.taskdata.addressmaster.server.search.model.JSONSerializable;

@Data
public class AddressElement implements JSONSerializable, Comparable<AddressElement> {

    final private String guid;
    final private String aoId;
    final private int level;
    final private String formalName;
    final private String elementTypeName;
    private final String zipCode;
    private final String allPostalCodes;
    private boolean finite = false;

    public AddressElement(JSONObject jsonObject) {
        this((String) jsonObject.get(JSONConstants.JSON_FIELD_AO_GUID),
            (String) jsonObject.get(JSONConstants.JSON_FIELD_AO_ID),
            (String) jsonObject.get(JSONConstants.JSON_FIELD_FORMAL_NAME),
            (String) jsonObject.get(JSONConstants.JSON_FIELD_elementTypeName),
            ((Long) jsonObject.get(JSONConstants.JSON_FIELD_LEVEL)).intValue(),
            (String) jsonObject.get(JSONConstants.JSON_FIELD_ZIP_CODE),
            (String) jsonObject.get(JSONConstants.JSON_FIELD_ALL_POSTAL_CODES));
    }

    public AddressElement(ResultSet resultSet, int resultSetLevel) throws SQLException {
        String tempGuid = resultSet.getString("AOGUID" + resultSetLevel);
        String tempAoid = resultSet.getString("AOID" + resultSetLevel);
        String tempP = resultSet.getString("FORMALNAME" + resultSetLevel);
        String tempAt = resultSet.getString("SOCRNAME" + resultSetLevel);
        int tempLevel = resultSet.getInt("LEVEL" + resultSetLevel);

        this.guid = tempGuid;
        this.aoId = tempAoid;
        this.formalName = tempP;
        this.elementTypeName = tempAt.toLowerCase();
        this.level = tempLevel;
        this.zipCode = null;
        this.allPostalCodes = null;
        this.finite = false;
    }

    public AddressElement(String guid, String aoId, String formalName, String elementTypeName, int level) {
        this(guid, aoId, formalName, elementTypeName, level, null, null);
    }

    public AddressElement(String guid, String aoId, String formalName, String elementTypeName, int level, String zipCode, String allPostalCodes) {
        this.guid = guid;
        this.aoId = aoId;
        this.formalName = formalName;
        this.elementTypeName = (null != elementTypeName ? elementTypeName.toLowerCase() : null);
        this.level = level;
        this.zipCode = zipCode;
        this.allPostalCodes = allPostalCodes;
        this.finite = allPostalCodes != null;
    }

    @Override
    public int compareTo(AddressElement o) {
        return level - o.level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddressElement that = (AddressElement) o;

        return aoId.equals(that.aoId);
    }

    @Override
    public int hashCode() {
        return aoId.hashCode();
    }

    public String getMetaphonedObject() {
        return Encoder.CUSTOM_RUSSIAN_ENCODER.encodeString(formalName.toLowerCase());
    }

    @SuppressWarnings("unchecked")
    public JSONAware toJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSONConstants.JSON_FIELD_LEVEL, level);
        jsonObject.put(JSONConstants.JSON_FIELD_elementTypeName, elementTypeName);
        jsonObject.put(JSONConstants.JSON_FIELD_FORMAL_NAME, formalName);
        jsonObject.put(JSONConstants.JSON_FIELD_AO_ID, aoId);
        jsonObject.put(JSONConstants.JSON_FIELD_AO_GUID, guid);

        if (allPostalCodes != null) {
            jsonObject.put(JSONConstants.JSON_FIELD_ALL_POSTAL_CODES, allPostalCodes);
        }
        if (zipCode != null) {
            jsonObject.put(JSONConstants.JSON_FIELD_ZIP_CODE, zipCode);
        }
        return jsonObject;
    }

    @Override
    public String toString() {
        return "AddressElement{" +
            "aoId='" + aoId + '\'' +
            ", level=" + level +
            ", formalName='" + formalName + '\'' +
            ", elementTypeName='" + elementTypeName + '\'' +
            '}';
    }
}
