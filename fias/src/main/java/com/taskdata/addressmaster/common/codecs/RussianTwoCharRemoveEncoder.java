/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.common.codecs;

public class RussianTwoCharRemoveEncoder implements Encoder {

    final private static int RUSSIAN_MIN_CODE = '\u0430'; // 'a'

    final private static int RUSSIAN_MAX_CODE = '\u0451'; // 'ё'

    final private static char STRANGE_RUSSIAN_CHAR = '\u0450'; // 'ѐ'

    final private static char STRANGE_RUSSIAN_CHAR_REPLACEMENT = '\u0451'; // 'ё'

    final private static char SPACE_CHAR = '\u0020';

    final private static int MAX_NUMBER_OF_CHARS = RUSSIAN_MAX_CODE - RUSSIAN_MIN_CODE + 2;

    final private static char[] TWO_CHAR_MAPPINGS = new char[MAX_NUMBER_OF_CHARS * MAX_NUMBER_OF_CHARS];

    final private static char[] ONE_CHAR_MAPPINGS = new char[MAX_NUMBER_OF_CHARS];

    final private static char[] IGNORE_CHARS = new char[]{'\u044a', '\u044c', '.'};

    static {
        initialize();
    }

    static private int getIndex(char c1) {
        return c1 > 0 ? ((int) c1) - RUSSIAN_MIN_CODE + 1 : 0;
    }

    static private int getIndex(char c1, char c2) {
        if (Character.isDigit(c1) || Character.isDigit(c2) || c1 == '-' || c2 == '-') {
            return 0;
        } else {
            int i1 = c1 > 0 ? ((int) c1) - RUSSIAN_MIN_CODE + 1 : 0;
            int i2 = c2 > 0 ? ((int) c2) - RUSSIAN_MIN_CODE + 1 : 0;
            return i1 * MAX_NUMBER_OF_CHARS + i2;
        }
    }

    static private char getOneCharMapping(char c) {
        return Character.isDigit(c) ? c :
            c == '-' ? '-' : ONE_CHAR_MAPPINGS[getIndex(c)];
    }

    static private char getTwoCharMapping(char c1, char c2) {
        return TWO_CHAR_MAPPINGS[getIndex(c1, c2)];
    }

    static private void initialize() {
        for (int i = 0; i < TWO_CHAR_MAPPINGS.length; ++i) {
            TWO_CHAR_MAPPINGS[i] = 0;
        }

        for (int i = 0; i < ONE_CHAR_MAPPINGS.length; ++i) {
            ONE_CHAR_MAPPINGS[i] = 0;
        }

        // и,о  -> и
        TWO_CHAR_MAPPINGS[getIndex('\u0438', '\u043e')] = '\u0438';
        // й,о  -> и
        TWO_CHAR_MAPPINGS[getIndex('\u0439', '\u043e')] = '\u0438';
        // и,е -> и
        TWO_CHAR_MAPPINGS[getIndex('\u0438', '\u0435')] = '\u0438';
        // й,е -> и
        TWO_CHAR_MAPPINGS[getIndex('\u0439', '\u0435')] = '\u0438';

        // п,б -> п
        TWO_CHAR_MAPPINGS[getIndex('\u043f', '\u0431')] = '\u043f';
        // с,з -> с
        TWO_CHAR_MAPPINGS[getIndex('\u0441', '\u0437')] = '\u0441';
        // т,д -> т
        TWO_CHAR_MAPPINGS[getIndex('\u0442', '\u0434')] = '\u0442';
        // ф,в -> ф
        TWO_CHAR_MAPPINGS[getIndex('\u0444', '\u0432')] = '\u0444';
        // к,г -> к
        TWO_CHAR_MAPPINGS[getIndex('\u043a', '\u0433')] = '\u043a';
        // т,с -> ц
        TWO_CHAR_MAPPINGS[getIndex('\u0442', '\u0441')] = '\u0446';
        // д,с -> ц
        TWO_CHAR_MAPPINGS[getIndex('\u0434', '\u0441')] = '\u0446';

        // с,ч -> щ
        TWO_CHAR_MAPPINGS[getIndex('\u0441', '\u0447')] = '\u0449';
        // ц,ч -> щ
        TWO_CHAR_MAPPINGS[getIndex('\u0446', '\u0447')] = '\u0449';
        // д,щ -> щ
        TWO_CHAR_MAPPINGS[getIndex('\u0434', '\u0449')] = '\u0449';

        // ю -> у
        ONE_CHAR_MAPPINGS[getIndex('\u044e')] = '\u0443';
        // о -> а
        ONE_CHAR_MAPPINGS[getIndex('\u043e')] = '\u0430';
        // ы -> а
        ONE_CHAR_MAPPINGS[getIndex('\u044b')] = '\u0430';
        // я -> а
        ONE_CHAR_MAPPINGS[getIndex('\u044f')] = '\u0430';
        // е -> и
        ONE_CHAR_MAPPINGS[getIndex('\u0435')] = '\u0438';
        // ё -> и
        ONE_CHAR_MAPPINGS[getIndex('\u0451')] = '\u0438';
        // э -> и
        ONE_CHAR_MAPPINGS[getIndex('\u044d')] = '\u0438';
    }

    static private char preprocess(char c) {
        for (char ignore_char : IGNORE_CHARS) {
            if (c == ignore_char) {
                // Let's just remove ignore char
                return 0;
            }
        }

        if (c >= RUSSIAN_MIN_CODE && c <= RUSSIAN_MAX_CODE) {
            if (c == STRANGE_RUSSIAN_CHAR) {
                return STRANGE_RUSSIAN_CHAR_REPLACEMENT;
            } else {
                return c;
            }
        } else if (Character.isDigit(c) || c == '-') {
            return c;
        } else {
            // Treat everything else as a separator
            return SPACE_CHAR;
        }
    }

    public String encodeString(String input) {
        input = input.toLowerCase();
        StringBuilder stringBuilder = new StringBuilder(input.length());
        char previous_char = 0;
        char current_char = 0;
        int inputLength = input.length();
        for (int i = 0; i < inputLength; ++i) {
            current_char = preprocess(input.charAt(i));

            if (current_char == 0) {
                continue;
            }

            if (current_char == SPACE_CHAR) {
                // Let's start from the very beginning
                // Let's take care about the last char from the previous word
                if (previous_char > 0) {
                    stringBuilder.append(previous_char);
                }
                stringBuilder.append(SPACE_CHAR);
                previous_char = 0;
            } else {
                char singleMappedChar = getOneCharMapping(current_char);
                if (singleMappedChar > 0) {
                    current_char = singleMappedChar;
                }

                char mappedChar = getTwoCharMapping(previous_char, current_char);
                if (mappedChar > 0) {
                    previous_char = mappedChar;
                    current_char = mappedChar;
                } else {
                    if (previous_char > 0) {
                        stringBuilder.append(previous_char);
                    }
                    previous_char = current_char;
                }
            }
        }

        // Let's make sure to append the very last symbol in the string
        if (current_char > 0 && current_char != SPACE_CHAR) {
            stringBuilder.append(current_char);
        } else if (previous_char > 0 && current_char != SPACE_CHAR) { // Special case when ignored char is the last in string
            stringBuilder.append(previous_char);
        }

        return stringBuilder.toString().trim();
    }
}
