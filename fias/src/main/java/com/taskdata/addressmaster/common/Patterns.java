/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.common;

import java.util.regex.Pattern;

public interface Patterns {
    Pattern streetNumbers = Pattern.compile("(\\d+).*");
    Pattern streetNumbers4Index = Pattern.compile("(\\d+)-(\\p{IsCyrillic}{1,2})($|\\s)");
    Pattern inputElementSeparators = Pattern.compile("\\p{Space}|(?<!\\.\\S?)[.,](?!\\S?\\.)"); //пробелы или точка запятая, за которыми не следует точка через 1 символ - для случая "П.С.", "В.О." и т.п.
    Pattern groupingPattern = Pattern.compile("(\\d*)(\\D*)");
    Pattern punctPattern = Pattern.compile("\\p{Punct}");
    Pattern digitsWithOptionalSlashPattern = Pattern.compile("([1-9]\\d*)([/|-]?\\d+)?"); // только цифры, опционально зазделённые '/' или '-'
    Pattern addressElementSeparators = Pattern.compile(" |-(?=\\S{3,})"); // пробел или черта("-") за которой следуют 3 и больше символов
    Pattern hyphenSeparator = Pattern.compile("-(?=\\S{3,})"); // черта("-") за которой следуют 3 и больше символов
    Pattern digitsFirstPattern = Pattern.compile("(\\d+)?(\\D.*)?");
    Pattern structNumPattern = Pattern.compile("[\\pL]");
    Pattern BUILDING_NUM_PATTERN = Pattern.compile("[1][0-9]|[1-9]"); // корпус - только цифры, номер < 20
    Pattern ZIP_PATTERN = Pattern.compile("(\\d\\d\\d\\d\\d\\d?+)");

    Pattern bStreetPattern = Pattern.compile("б", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
    Pattern bExtendedStreetPattern = Pattern.compile("больш\\p{IsCyrillic}{2}", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
    Pattern mStreetPattern = Pattern.compile("м", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
    Pattern mExtendedStreetPattern = Pattern.compile("мал\\p{IsCyrillic}{2}", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

    Pattern apartmentPattern = Pattern.compile("(комната|комн\\.|офис|оф\\.|кабинет|каб\\.|квартира|кв\\.)\\s*\\S?\\d*"); // комната №21
    Pattern bracketsPattern = Pattern.compile("\\(.*\\)"); // (фигня в скобках)
    Pattern floorPattern = Pattern.compile("\\d+\\s*?(этаж|эт\\.)"); // 12 эт.
    Pattern quotationPattern = Pattern.compile("\".*\""); // "фигня2 в ковычках"
    Pattern singleQuotationPattern = Pattern.compile("'.*'"); // "фигня2 в ковычках"
    Pattern poBoxPattern = Pattern.compile("\\b([А|а][\\\\|/]{1}[Я|я]\\s*\\d+)"); // а/я 23

    Pattern hyphenInBigWords = Pattern.compile("\\S{3,}-\\S{3,}");

    Pattern fiasVersionFromUpdateUrl = Pattern.compile(".+/([\\d.]+)/.+");
}
