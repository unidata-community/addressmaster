/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.taskdata.addressmaster.common.model;

import java.util.Collection;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.taskdata.addressmaster.server.search.model.JSONSerializable;

public class CompositeAddressElement implements JSONSerializable {

    private final SortedSet<AddressElement> addressElements;

    public CompositeAddressElement(Collection<AddressElement> elements) {
        addressElements = new TreeSet<>(elements);
    }

    public CompositeAddressElement(JSONArray jsonArray, boolean useJSONMarker) {
        addressElements = new TreeSet<>();
        for (Object aJsonArray : jsonArray) {
            JSONObject jsonObject = (JSONObject) aJsonArray;
            addressElements.add(new AddressElement(jsonObject));
        }
    }

    public String getAddressCity() {
        StringBuilder sb = new StringBuilder();
        for (AddressElement addressElement : addressElements) {
            if ((addressElement.getLevel() == 1 && "город".equalsIgnoreCase(addressElement.getElementTypeName()))
                || addressElement.getLevel() == 4
                || addressElement.getLevel() == 6) {
                sb.append(addressElement.getElementTypeName())
                    .append(' ')
                    .append(addressElement.getFormalName());
                break;
            }
        }
        return sb.toString();
    }

    public Collection<AddressElement> getAddressElements() {
        return Collections.unmodifiableCollection(addressElements);
    }

    public String getAddressToIndex() {
        StringBuilder addressToIndexBuilder = new StringBuilder();

        for (AddressElement addressElement : addressElements) {
            addressToIndexBuilder.append(addressElement.getMetaphonedObject()).append(' ');
        }

        return addressToIndexBuilder.toString().trim();
    }

    public String getAddressTypeToIndex() {
        StringBuilder addressTypeToIndexBuilder = new StringBuilder();
        for (AddressElement addressElement : addressElements) {
            addressTypeToIndexBuilder.append(addressElement.getElementTypeName()).append(' ');
        }

        return addressTypeToIndexBuilder.toString();
    }

    public String getClassicAddress() {
        StringBuilder sb = new StringBuilder();
        for (AddressElement addressElement : addressElements) {
            sb.append(addressElement.getElementTypeName()).append(' ').append(addressElement.getFormalName()).append(", ");
        }
        return sb.toString();
    }

    /**
     * @since 2017.03.06 It seems not used
     */
    @Deprecated
    public String getFullAddress() {
        StringBuilder fullAddressBuilder = new StringBuilder();
        for (AddressElement addressElement : addressElements) {
            fullAddressBuilder.append(addressElement.getFormalName()).append(' ').append(addressElement.getElementTypeName().toLowerCase()).append(' ');
        }
        return fullAddressBuilder.toString().trim();
    }

    public String getLeafAoId() {
        AddressElement finiteAddressElement = addressElements.last();
        return finiteAddressElement.getAoId();
    }

    public String getLeafGuid() {
        AddressElement finiteAddressElement = addressElements.last();
        return finiteAddressElement.getGuid();
    }

    public String getZipCodesString() {
        AddressElement finiteAddressElement = addressElements.last();
        String zipCode = finiteAddressElement.getZipCode();
        if (zipCode != null) {
            return zipCode;
        }
        return finiteAddressElement.getAllPostalCodes();
    }

    public JSONAware toJSON() {
        JSONArray jsonArray = new JSONArray();
        for (AddressElement addressElement : addressElements) {
            jsonArray.add(addressElement.toJSON());
        }
        return jsonArray;
    }
}
