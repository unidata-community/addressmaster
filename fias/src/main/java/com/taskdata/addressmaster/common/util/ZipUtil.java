package com.taskdata.addressmaster.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import lombok.extern.slf4j.Slf4j;

/**
 * @link https://www.baeldung.com/java-compress-and-uncompress
 * @author Pavel Alexeev.
 * @since 2020-11-04 03:21.
 */
@Slf4j
public class ZipUtil {

	/**
	 * Shorthand for {@see extractToDir} without regexp patterns to exclude all files.
	 * @inheritDoc
	 */
	public static void extractToDir(File fileZip, File destDir) throws IOException {
		extractToDir(fileZip, destDir, null, null);
	}

	/**
	 * Extract Zip archive into provided directory path
	 *
	 * @param fileZip File archive to extract
	 * @param destDir Destination directory extract to
	 * @param includePattern RegExp pattern to match filenames for extract. If null - all.
	 * @param excludePattern RegExp pattern to exclude filenames form extracting. If null - no excludes
	 * @throws IOException in case of extraction error
	 */
	public static void extractToDir(File fileZip, File destDir, Pattern includePattern, Pattern excludePattern) throws IOException {
		byte[] buffer = new byte[1024];
		ZipInputStream zis = new ZipInputStream(new FileInputStream(fileZip));
		ZipEntry zipEntry = zis.getNextEntry();
		while (zipEntry != null) {
			if (null != includePattern && !includePattern.matcher(zipEntry.getName()).find()){
				log.debug("ZipEntry [" + zipEntry.getName() + "] does not match includePattern [" + includePattern + "]. Skipping");
				zipEntry = zis.getNextEntry();
				continue;
			}
			if (null != excludePattern && excludePattern.matcher(zipEntry.getName()).find()){
				log.debug("ZipEntry [" + zipEntry.getName() + "] matches excludePattern [" + excludePattern + "]. Skipping");
				zipEntry = zis.getNextEntry();
				continue;
			}
			log.debug("Extracting ZipEntry [" + zipEntry.getName() + "]...");
			File newFile = newFile(destDir, zipEntry);
			FileOutputStream fos = new FileOutputStream(newFile);
			int len;
			while ((len = zis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}
			fos.close();
			zipEntry = zis.getNextEntry();
		}
		zis.closeEntry();
		zis.close();
	}

	private static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
		File destFile = new File(destinationDir, zipEntry.getName());

		String destDirPath = destinationDir.getCanonicalPath();
		String destFilePath = destFile.getCanonicalPath();

		if (!destFilePath.startsWith(destDirPath + File.separator)) {
			throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
		}

		return destFile;
	}
}
