//configuration

/**
 * Конфигурация
 */

var config = config || {};

//адрес приложения
//config.API_URL = '/fias/';
config.API_URL = extractServletNameIfAny();

//путь к запросу адресов по введенной строке
config.REQUEST_ADDRESS_ENDPOINT = config.API_URL + 'AutoCompleteServlet';

//путь к запросу метаданных
config.REQUEST_METADATA_ENDPOINT = config.API_URL + 'AutoCompleteServlet/metadata';

//путь к запросу подробной информации о выбранном адресе
config.REQUEST_ADDRESS_DETAILS_ENDPOINT = config.API_URL + 'AddressDetailsServlet';

//путь к запросу номеров домов для выбранного адреса
config.REQUEST_ADDRESS_HOUSES_ENDPOINT = config.API_URL + 'AddressHousesServlet';

//путь к запросу версии ФИАС и номера сборки
config.REQUEST_FIAS_VERSION = config.API_URL + 'VersionServlet';

// задержка на запрос после ввода с клавиатуры (мс)
config.REQUEST_TIMEOUT = 1000;

function extractServletNameIfAny() {
    var pathArray = window.location.pathname.split( '/' );
    if (pathArray) {
        return pathArray[0];
    }
    return '/';
}
