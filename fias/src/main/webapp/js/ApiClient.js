/*
 * ApiClient - осуществляет запросы к сервису адресов
 *
 * */
var ApiClient = (function (self) {

    var REQUEST_ADDRESS_PATH = '/AutoCompleteServlet?term={{term}}'; // не используется, берется из конфига

    /**
     * Метод инициализации, необходимо вызвать перед использыванием клиента.
     * @param config - конфигурация
     * @return ApiClient - возвращает ссылку на себя
     */
    self.init = function (config) {
        self.config = config;
        self.metadata = null;
        self.addressListContainer = null; //хранит последний запрошенный список адресов
        return self;
    };

    /**
     * Метод осуществляет запрос адресов по введенной фразе в строке поиска
     *
     * @param search_str - введенная строка
     * @param resultCb - функция обратного вызова, в качестве параметра принимает список адресов
     */
    self.requestAddressList = function (search_str, resultCb) {
        var url = self.config.REQUEST_ADDRESS_ENDPOINT;
        var requestParams = {'term':encodeURIComponent(search_str)};
        self._get(url, null, requestParams, function (result) {
            self.addressListContainer = result;
            if (resultCb) {
                resultCb(result);
            }
        }, function (error) {
            self.addressListContainer = null;
            self.log.error("Error request address, search_str=\"" + search_str + "\"");
        });
    };

    self.requestMetadata = function (resultCb) {
        if (self.metadata != null) {
            if (resultCb) {
                resultCb(self.metadata);
            }
            return;
        }

        var url = self.config.REQUEST_METADATA_ENDPOINT;
        self._get(url, null, null, function (result) {
            self.metadata = result;
            if (resultCb) {
                resultCb(result);
            }
        }, function (error) {
            self.metadata = null;
            self.log.error("Error request address, search_str=\"" + search_str + "\"");
        });
    };

    /**
     * Метод осуществляет запрос номеров домов для выбранного адреса.
     */
    self.requestAddressHouses = function (addressId, paginationData, resultCb) {
        var url = self.config.REQUEST_ADDRESS_HOUSES_ENDPOINT;
        var requestParams = {'addressId': encodeURIComponent(addressId)
                            ,'pageNum': encodeURIComponent(paginationData.pageNum)
                            ,'pageSize': encodeURIComponent(paginationData.pageSize)};
        self._get(url, null, requestParams, function (result) {
            $.each(result['HOUSES'], function(index, value) {
                var houseStr = "";
                houseStr += value.HOUSE_NUMBER ? value.HOUSE_NUMBER : "";
                houseStr += value.BUILDING_NUMBER ? (" корп. " + value.BUILDING_NUMBER) : "";
                houseStr += value.STRUCTURE ? (" "+ value.STRUCTURE.STRUCTURE_STATUS.STRUCTURE_STATUS_SHORT_DESC + " " + value.STRUCTURE.STRUCTURE_NUM) : "",
                value.HOUSE_STR = houseStr;
            });
            if (resultCb) {
                resultCb(result);
            }
        }, function (error) {
            self.addressListContainer = null;
            self.log.error("Error request address houses \"" + url + "\"");
        });
    }

    /**
         * Метод запрашивает номер версии ФИАС
         */
        self.requestFiasVersion = function (resultCb) {
            var url = self.config.REQUEST_FIAS_VERSION;
            var requestParams = {'requestType': 'fiasVersion'};
            self._get(url, null, requestParams, function (result) {

            }, function (error) {
                self.log.error("Error requesting FIAS version \"" + url + "\"");
            });
        }

    /**
         * Метод запрашивает номер сборки приложения
         */
        self.requestFiasVersion = function (resultCb) {
            var url = self.config.REQUEST_FIAS_VERSION;
            var requestParams = {'requestType': 'appBuild'};
            self._get(url, null, requestParams, function (result) {

            }, function (error) {
                self.log.error("Error requesting FIAS version \"" + url + "\"");
            });
        }

    /**
     * Метод осуществляет запрос подробной информации о выбранном адресе
     *
     * @param addressId - id адреса
     * @param houseId - id дома
     * @param resultCb - функция обратного вызова
     */
    self.requestAddressDetails = function (addressId, houseId, rangeId, resultCb) {
        var url = self.config.REQUEST_ADDRESS_DETAILS_ENDPOINT;
        var requestParams = {
                            'addressId': encodeURIComponent(addressId),
                            'houseId': encodeURIComponent(houseId),
                            'rangeId': encodeURIComponent(rangeId)
                            };
        self._get(url, null, requestParams, function (result) {
            if (resultCb) {
                resultCb(result);
            }
        }, function (error) {
            self.addressListContainer = null;
            self.log.error("Error request address details \"" + url + "\"");
        });
    };

    self.requestAddressInfo = function (address_id, resultCb) {
        resultCb(self._findAddress(address_id, self.addressListContainer.addressList));
    };

    self._findAddress = function (address_id, address_list) {
        if (address_id == null && address_list == null && address_list.length == 0) return null;
        for (var key in address_list){
            var address = address_list[key];
            if (address.ADDRESS_ID == address_id) {
                return address;
            }
        }
        return null;
    };

    /**
     * Внутренний метод, осуществляет GET запрос
     * @param url
     * @param pathVariables данные, подстваляются в шаблон url {{name}}
     * @param requestParams
     * @param successCb
     * @param errorCb
     * @private
     */
    self._get = function (url, pathVariables, requestParams, successCb, errorCb) {
        if (pathVariables != null) {
            for (var key in pathVariables) {
                url = url.replace('{{' + key + '}}', encodeURIComponent(pathVariables[key]));
            }
        }

        if (requestParams != null) {
            for (var key in requestParams) {
                url = self._addUrlParameter(url, key, requestParams[key]);
            }
        }

        $.ajax({
            type:'GET',
            url:url,
            dataType:'json',
            contentType:'application/json',
            success:function (result) {
                if (successCb) {
                    successCb(result);
                }
            },
            error:function (error) {
                if (errorCb) {
                    errorCb(error);
                }
            },
            statusCode:{
                404:function () {
                    self.log.error('404 not found: ' + url);
                },
                500:function () {
                    self.log.error('500 internal server error: ' + url);
                }
            }
        });
    };

    self._addUrlParameter = function (url, key, val) {
        var parts = url.match(/([^?#]+)(\?[^#]*)?(\#.*)?/);
        var url = parts[1];
        var qs = parts[2] || '';
        var hash = parts[3] || '';

        if (!qs) {
            return url + '?' + key + '=' + encodeURIComponent(val) + hash;
        } else {
            var qs_parts = qs.substr(1).split("&");
            var i;
            for (i = 0; i < qs_parts.length; i++) {
                var qs_pair = qs_parts[i].split("=");
                if (qs_pair[0] == key) {
                    qs_parts[ i ] = key + '=' + encodeURIComponent(val);
                    break;
                }
            }
            if (i == qs_parts.length) {
                qs_parts.push(key + '=' + encodeURIComponent(val));
            }
            return url + '?' + qs_parts.join('&') + hash;
        }
    };

    // логгер
    self.log = {
        error:function (message) {
            if (console && console.error) {
                console.error(message);
            }
        },
        info:function (message) {
            if (console && console.info) {
                console.info(message);
            }
        }
    };
    return self;
})(ApiClient || {});
