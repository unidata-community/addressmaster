$(document).ready(function () {
    UIController.init(config);

    var url = self.config.REQUEST_FIAS_VERSION;
    $.getJSON(url,  {requestType: "fiasVersion"},
                     function(data) {
                $('#version').html("Версия ФИАС: " + data.FIAS_VERSION);
            });
    $.getJSON(url,  {requestType: "appBuild"},
                         function(data) {
                    $('#build').html("Build no: " + data.APP_BUILD);
                });
});

/**
 * UIController - основной класс, осуществляет обрабоку всех пользовательских действий
 */
var UIController = (function (self) {

    var ADDRESS_LI_SELECTOR = '.address';
    var SEARCH_INPUT_SELECTOR = '.search-input input';

    var SEARCH_RESULT_SELECTOR = '.search-result';

    var OPEN_ON_MAP_SELECTOR = '.open-on-map';

    var SHOW_AO_DETAILS_SELECTOR = '.show-details';
    var ADDRESS_DETAILS_POPUP_SELECTOR = '.address-details';

    var SELECT_HOUSE_SELECTOR = '.house-number';

    /**
     * Инициализация UIController-а
     * @param config принимает на вход словарь с конфигурацией.
     */
    self.init = function (config) {
        if (config == null) {
            self.log.error("config is null");
            return;
        }
        self.config = config;
        self.client = ApiClient.init(config);

        self.bind();
    };

    /**
     * Связывание пользовательских событий с обработчиками
     */
    self.bind = function () {
        var timeout_id = 0;

        $(ADDRESS_LI_SELECTOR).live('click', function (e) {
            self.addressClickAction($(e.currentTarget));
        });

        $(OPEN_ON_MAP_SELECTOR).live('click', function (e) {
            self.openOnMapAction($(e.currentTarget));
        });

        $(SHOW_AO_DETAILS_SELECTOR).live('click', function (e) {
            self.showAODetailsAction($(e.currentTarget));
            return false;
        });

        $(SELECT_HOUSE_SELECTOR).live('click', function (e) {
            self.selectHouseAction($(e.currentTarget));
            return false;
        });

        $(SEARCH_INPUT_SELECTOR).on('keyup', function (e) {
            if (e.keyCode != 13 && e.keyCode != 27 && e.keyCode != 38 && e.keyCode != 40) {
                clearTimeout(timeout_id);
                timeout_id = setTimeout(function () {
                    self.searchAction($(e.currentTarget));
                }, self.config.REQUEST_TIMEOUT);
            }
        });

        $(SEARCH_INPUT_SELECTOR).on('keypress', function (e) {
            if (e.keyCode == 13) {
                clearTimeout(timeout_id);
                self.searchAction($(e.currentTarget));
            }
        });

        $(document).on('keydown', function (e) {
            if ($('body').hasClass('loading')) {
                //Cancel event because of loading data
                e.stopPropagation();
                return;
            } else if (e.keyCode == 13) {
                if ($(e.target).prop("tagName").toLowerCase() === 'input') {
                    //Nothing to do, because input has own event handler
                    return;
                }
                self.enterAction();
            } else if (e.keyCode == 27) {
                self.escapeAction();
            } else if (e.keyCode == 38) {
                self.arrowUpAction();
            } else if (e.keyCode == 40) {
                self.arrowDownAction();
            }
        });

        $('body').on({
            ajaxStart: function() {
                $(this).addClass('loading');
            },
            ajaxStop: function() {
                $(this).removeClass('loading');
            }
        });

    };

    /**
     * Обработчик события ввода поисковой фразы
     *
     * @param targetInput - input c поисковой фразой
     */
    self.searchAction = function (targetInput, resultCb) {
        var search_str = targetInput.attr('value');
        if (search_str == null || search_str.trim() == "") {
            self.log.info("search string is empty");
            self.displayAddressList(null);
            return;
        }
        self.client.requestAddressList(search_str, function (addressListContainer) {
            self.client.requestMetadata(function (metadata) {
                self._updateAddressListWithMetadata(addressListContainer.addressList, metadata);
                self.displayAddressList(addressListContainer);
                if (resultCb) {
                    resultCb();
                }
            });
        })
    };

    /**
     * Обработчик событий клика на строку с адресом
     * @param target
     */
    self.addressClickAction = function (target) {
        if (target == null || target.length == 0) {
            return;
        }
        if (target.parent().hasClass("selected")) {
            self.hideSelectedAddressInfo();
            return;
        }
        var address_id = target.attr("p_address_id");

        self.hideSelectedAddressInfo();
        self.client.requestAddressInfo(address_id, function (addressInfo) {
            self.client.requestMetadata(function (metadata) {
                self._updateAddressWithMetadata(addressInfo, metadata);
                if (!(addressInfo.HOUSE_INFO && addressInfo.HOUSE_INFO.HOUSE_NUMBER) && !addressInfo.HOUSES) {
                    var paginationData = {pageNum: 0, pageSize: 300};
                    self.client.requestAddressHouses(address_id, paginationData, function (addressHouses) {
                        addressInfo.HOUSES = addressHouses['HOUSES'];
                        self.displayAddressInfo(addressInfo, target)
                    });
                } else {
                    self.displayAddressInfo(addressInfo, target)
                }
            });
        });
    };

    self.openOnMapAction = function (target) {
        //open map with a href
    };

    /**
     * Обработчик событий клика на линк подробнее для выбранного адреса
     * @param target
     */
    self.showAODetailsAction = function (target) {
        var addressId = target.attr("p_address_id");
        var houseId = target.attr("p_house_id");
        var rangeId = target.attr("p_range_id");

        self.client.requestAddressInfo(addressId, function(address) {
                if (!address.ADDRESS_DETAILS) {
                    self.client.requestAddressDetails(addressId, houseId, rangeId, function (addressDetails) {
                        address.ADDRESS_DETAILS = addressDetails.ADDRESS_DETAILS;
                        self._openAddressDetailsPopup(address);
                    });
                } else {
                    self._openAddressDetailsPopup(address);
                }
        });
    };

    /**
     * Показывает попап с подробной информацией о выбранном адресе
     */
    self._openAddressDetailsPopup = function (address) {
        var popupHtml = tmpl('address_details_tpl', {data: address});
        $.colorbox({html: popupHtml, transition: 'none', opacity: 0.5, close: 'Закрыть'});
    }

    /**
     * Обработчик событий клика на линк с номером дома для выбранного адреса
     * @param target
     */
    self.selectHouseAction = function (target) {
        var addressStr = target.attr("p_address_str");
        var zipCode = target.attr("p_zip_code");
        var houseStr = target.attr("p_house_str");

        var searchStr = "";
        searchStr += zipCode ? (zipCode + ", ") : "";
        searchStr += addressStr ? (addressStr + ", ") : "";
        searchStr += houseStr ? ("дом " + houseStr) : "";

        $(SEARCH_INPUT_SELECTOR).val(searchStr);
        self.searchAction($(SEARCH_INPUT_SELECTOR), function() {
            var foundAddresses = $(SEARCH_RESULT_SELECTOR).find(ADDRESS_LI_SELECTOR);
            if (foundAddresses && foundAddresses.length > 0) {
                self.addressClickAction($(foundAddresses[0]));
            }
        });

        var detailsPopup = $(ADDRESS_DETAILS_POPUP_SELECTOR);
        if (detailsPopup.length > 0) {
            $.colorbox.close();
        }
    };

    /**
     * Обработчик события нажатия на клавишу Enter (необходимо инициировать нажатие линка Подробнее для раскрытого адреса)
     */
    self.enterAction = function () {
        if ($(ADDRESS_DETAILS_POPUP_SELECTOR).size() > 0) {
            //Nothing to do because of opened popup with address details
            return;
        }
        $(SEARCH_RESULT_SELECTOR).find('.selected').find(SHOW_AO_DETAILS_SELECTOR).trigger("click");
    };

    /**
     * Обработчик события нажатия на клавишу Esc (необходимо свернуть выбранные адресса)
     */
    self.escapeAction = function () {
        if ($(ADDRESS_DETAILS_POPUP_SELECTOR).size() > 0) {
            //Nothing to do because of opened popup with address details
            return;
        }
        self.hideSelectedAddressInfo();
    };


    /**
     * Обработчик события нажатия клавиши "вверх"
     */
    self.arrowUpAction = function () {
        if ($(ADDRESS_DETAILS_POPUP_SELECTOR).size() > 0) {
            //Nothing to do because of opened popup with address details
            return;
        }
        var selectedAddress = $(SEARCH_RESULT_SELECTOR).find('.selected ' + ADDRESS_LI_SELECTOR);
        var targetAddress;
        if (selectedAddress != null && selectedAddress.length && selectedAddress.parent().prev().length) {
            targetAddress = selectedAddress.parent().prev().find(ADDRESS_LI_SELECTOR);
        } else {
            targetAddress = $(SEARCH_RESULT_SELECTOR).find('li ' + ADDRESS_LI_SELECTOR).last();
        }
        self.addressClickAction(targetAddress);
    };

    /**
     * Обработчик события нажатия клавиши "вниз"
     */
    self.arrowDownAction = function () {
        if ($(ADDRESS_DETAILS_POPUP_SELECTOR).size() > 0) {
            //Nothing to do because of opened popup with address details
            return;
        }
        var selectedAddress = $(SEARCH_RESULT_SELECTOR).find('.selected ' + ADDRESS_LI_SELECTOR);
        var targetAddress;
        if (selectedAddress != null && selectedAddress.length && selectedAddress.parent().next().length) {
            targetAddress = selectedAddress.parent().next().find(ADDRESS_LI_SELECTOR);
        } else {
            targetAddress = $(SEARCH_RESULT_SELECTOR).find('li ' + ADDRESS_LI_SELECTOR).first();
        }
        self.addressClickAction(targetAddress);
    };

    /**
     * Метод выводит список адресов, по запрошенной фразе в виде таблицы
     * @param addressList - список адресов
     */
    self.displayAddressList = function (addressListContainer) {
        var html = "";
        if (addressListContainer != null && addressListContainer.addressList != null && addressListContainer.addressList.length > 0) {
            html = tmpl("address_tpl", {'addressListContainer':addressListContainer})
        } else {
            html = "<p>Адрес не найден</p>";
        }
        $(SEARCH_RESULT_SELECTOR).html(html)
    };


    /**
     * Метод обновляет список адресов данными из метадаты (добавляет полные названия к сокращениям),
     * фомирует и добавляет адресную строку
     * @param addressList - список адресов
     * @param metadata - мета данные (словарь сокращенных названий)
     */
    self._updateAddressListWithMetadata = function (addressList, metadata) {
        if (addressList != null && addressList.length > 0) {
            for (var key in addressList) {
                addressList[key] = self._updateAddressWithMetadata(addressList[key], metadata);
            }
        }
        return addressList;
    };

    /**
     *
     * Метод обновляет адрес данными из метадаты, фомирует и добавляет адресную строку
     * @param address - адрес
     * @param metadata - мета данные (словарь сокращенных названий)
     */
    self._updateAddressWithMetadata = function (address, metadata) {
        if (address.ADDRESS_INFO) {
            var addressStr = "";
            for (var key in address.ADDRESS_INFO) {
                var item = address.ADDRESS_INFO[key];
                item.elementName = metadata[item.elementTypeName] ? metadata[item.elementTypeName] : item.elementTypeName;
                var addressNameStr = item.elementTypeName + ". " + item.formalName;
                addressStr += (addressStr.length == 0) ? (addressNameStr) : (", " + addressNameStr);
            }
            var houseInfo = address.HOUSE_INFO

            if (houseInfo) {
                addressStr += houseInfo.HOUSE_NUMBER ? ", дом " + houseInfo.HOUSE_NUMBER : "";
                addressStr += houseInfo.BUILDING_NUMBER ? ", корпус " + houseInfo.BUILDING_NUMBER : "";
                addressStr += houseInfo.STRUCTURE ? ", " + houseInfo.STRUCTURE.STRUCTURE_STATUS.STRUCTURE_STATUS_SHORT_DESC + " " + houseInfo.STRUCTURE.STRUCTURE_NUM : "";
            }
            address.ADDRESS_STR = addressStr;
        }
        return address;
    };

    /**
     * Скрывает выбранный адресс в строку
     */
    self.hideSelectedAddressInfo = function () {
        $(SEARCH_RESULT_SELECTOR).find('.selected').each(function () {
            var item = $(this);
            item.removeClass('selected');
            item.find(".address").show();
            item.find(".address-info").hide();
        })
    };

    /**
     * Отображает подробную информацию об адресе
     * @param addressInfo информация об адресе
     * @param target - элемент списка, куда вставить html с информацией
     */
    self.displayAddressInfo = function (addressInfo, target) {
        var parentLi = target.parent();
        parentLi.addClass("selected");
        var addressInfoItem = parentLi.find(".address-info");
        if (addressInfoItem != null && addressInfoItem.length > 0) {
            addressInfoItem.show();
            return;
        }
        var html = tmpl("address_info_tpl", {'data':addressInfo});
        parentLi.append(html);
    };

    //логгер
    self.log = {
        error:function (message) {
            if (console && console.error) {
                console.error(message);
            }
        },
        info:function (message) {
            if (console && console.info) {
                console.info(message);
            }
        }
    };


    self._copyToClipboard = function (text) {

    };

    return self;
})(UIController || {});