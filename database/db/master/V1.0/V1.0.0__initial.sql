--
-- PostgreSQL database create
--

-- CREATE DATABASE fias ENCODING = 'utf8'  LC_COLLATE ='utf8_general_ci';

-- connections store, for start
CREATE TABLE S_PROPERTY_DATA_STORE (
    PDS_ID              SERIAL  PRIMARY KEY, -- it will be s_property_data_store_psd_id_seq. set it in entity
    PDS_DB_SOURCE       text,
    PDS_ACTIVE          boolean
);
INSERT INTO S_PROPERTY_DATA_STORE(
    PDS_DB_SOURCE,PDS_ACTIVE
)
VALUES ('DATA_SOURCE_1', true),
       ('DATA_SOURCE_2', false);

-- request log
CREATE TABLE S_REQUEST_AUDIT (
	RA_ID               SERIAL  PRIMARY KEY, -- it will be s_request_audit_ra_id_seq. set it in entity
	RA_DATA             text,
	RA_HEADERS          text,
	RA_EVENT_METRIC     float,
	RA_ROWS             integer,
	RA_SOURCE_ADDR      varchar(100) not null,
	RA_REQUEST_TYPE     varchar(32) not null,
	RA_INPUT            varchar(255),
	RA_USER_NAME        varchar(100),
	RA_EVENT_DATE       timestamp not null,
	RA_EVENT_EXEC_TIME  bigint not null
);

-- update db history
CREATE TABLE S_FIAS_UPDATER_STATUS (
	FUS_ID          SERIAL  PRIMARY KEY,  -- it will be s_fias_updater_status_fus_id_seq. set it in entity
	FUS_VERSION     text,
	FUS_ERROR       text,
	FUS_WORKING     boolean,
	FUS_STATUS      text,
	FUS_INITIAL_LOGIN      text,
	FUS_INITIAL_NAME      text,
	FUS_START_DATE  timestamp not null,
	FUS_END_DATE    timestamp,
	FUS_URL         text,
	FUS_SWITCHED    boolean
);
INSERT INTO S_FIAS_UPDATER_STATUS(
    FUS_VERSION,FUS_ERROR,FUS_WORKING,FUS_STATUS, FUS_INITIAL_LOGIN, FUS_INITIAL_NAME, FUS_START_DATE, FUS_END_DATE, FUS_URL, FUS_SWITCHED
)
VALUES ('empty', '', false, 'OK', 'server', 'server', CURRENT_DATE, CURRENT_DATE, 'url', true);


-- old table. used in AddressService, show on ui last db version.
-- write a first value on first migration
CREATE TABLE FIAS_DB_VERSION(
	ID          SERIAL PRIMARY KEY,
	DATE_STR    text
);
INSERT INTO FIAS_DB_VERSION (DATE_STR) VALUES('empty');