-- Store for different settings
CREATE TABLE s_settings (
    key         text PRIMARY KEY,
    value       text
)
;
INSERT INTO s_settings
SELECT 'active.datasource', pds_db_source FROM s_property_data_store WHERE pds_active = true
;

DROP TABLE S_PROPERTY_DATA_STORE
;