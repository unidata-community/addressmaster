-- After loading source DBF incoming from fias.nalog.ru we want do some transformations like type changes, table merges and indexes creation
CREATE OR REPLACE FUNCTION preprocess_imported_db() RETURNS void AS
$$
BEGIN
	PERFORM replace_numbered_tables_by_full('house', 'STARTDATE < CURRENT_DATE AND CURRENT_DATE < ENDDATE');

	PERFORM replace_numbered_tables_by_full('addrob', 'actstatus = 1');

	PERFORM convert_refs_to_uuid_and_index();

	ALTER TABLE socrbase ALTER COLUMN level TYPE NUMERIC USING level::numeric;
	CREATE INDEX idx_socrbase_cover ON socrbase(level, scname, socrname);

	PERFORM fill_synonyms_and_common_mistakes();

	ANALYZE;
END
$$
LANGUAGE 'plpgsql';