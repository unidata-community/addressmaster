--
-- PostgreSQL database create
--

-- CREATE DATABASE fias ENCODING = 'utf8'  LC_COLLATE ='utf8_general_ci';


-- fun megatable
CREATE TABLE FE_ADDRESS_OBJECT_EX (
  AOE_ID    SERIAL PRIMARY KEY, ---- it will be fe_address_object_ex_aoe_id_seq. set it in entity
  AOE_NAME  text,
  AOID      text
);

-- fun megatable
CREATE TABLE FE_AO_TYPE_EX (
  aote_id               SERIAL PRIMARY KEY, ---- it will be fe_ao_type_ex_aote_id_seq. set it in entity
  FULL_NAME             text,
  ALTERNATIVE_NAME      text
);


-- Table to add manual match synonyms like 'Петербург' == 'Санкт-Петербург'
CREATE TABLE FE_SYNONYMS (
	NAME text,
	FORMAL_NAME text,
	PRIMARY KEY(NAME, FORMAL_NAME)
);

INSERT INTO FE_SYNONYMS VALUES
	('спб', 'Санкт-Петербург')
	,('питер', 'Санкт-Петербург')
	,('петербург', 'Санкт-Петербург')
	,('питербург', 'Санкт-Петербург')
	,('ленинград', 'Санкт-Петербург')
	,('петроград', 'Санкт-Петербург')
	,('мск', 'Москва');

-- That function must be called on data import to obtain UIDs of records to match mistakes and abbreviations
CREATE OR REPLACE FUNCTION fill_synonyms_and_common_mistakes() RETURNS void AS
$func$
BEGIN
	INSERT INTO FE_ADDRESS_OBJECT_EX (aoe_name, aoid)
	SELECT s.name, ao.aoid
	FROM
		FE_SYNONYMS s
		JOIN addrob_all ao ON (ao.formalname = s.formal_name AND aolevel = 1 AND nextid IS NULL);
END
$func$
LANGUAGE 'plpgsql'

--INSERT INTO FE_AO_TYPE_EX (FULL_NAME, ALTERNATIVE_NAME)
--SELECT distinct FULL_NAME, ALTERNATIVE_NAME FROM
--(SELECT lower(SOCRNAME) as FULL_NAME, lower(SCNAME) as ALTERNATIVE_NAME FROM socrbase t WHERE T.KOD_T_ST::text != 0::text) S1
--union
--(SELECT lower(SOCRNAME) as FULL_NAME, lower(SOCRNAME) as ALTERNATIVE_NAME FROM socrbase t WHERE T.KOD_T_ST::text != 0::text);

INSERT INTO FE_AO_TYPE_EX (ALTERNATIVE_NAME, FULL_NAME) VALUES
-- Автономный округ TODO problem with spaces
--  ('ао', '101'), ('авт окр', '101')
	('ок', 'округ'), ('окр', 'округ')
-- Автономная область TODO problem with spaces
--  ,('аоб', '102')
--  ,('авоб', '102')
--  ,('ав об', '102')
--  ,('а обл', '102')
--  ,('авобл', '102')
--  ,('ав обл', '102')
-- Город
	,('грд', 'город'),('гор', 'город'),('г-д', 'город')
-- Край
	,('кр', 'край'),('кр-й', 'край')
-- Область
	,('о', 'область'),('об', 'область'),('об-сть', 'область')
-- Республика
	,('р', 'республика'),('рес', 'республика'),('р-ка', 'республика')
-- Район
	,('р', 'район'),('рай', 'район'),('р-он', 'район')
-- Аллея
	,('а', 'аллея'),('ал', 'аллея'),('алл', 'аллея'),('а-я', 'аллея')
-- Бульвар
	,('б', 'бульвар'),('бул', 'бульвар'),('бул-р', 'бульвар')
-- Дорога
--  ,('дор', 'дорога')
	,('д', 'дорога'),('др', 'дорога'),('д-а', 'дорога'),('д-га', 'дорога')
-- Квартал
	,('к', 'квартал'),('кв', 'квартал'),('квртл', 'квартал'),('к-л', 'квартал'),('квар', 'квартал'),('кварт', 'квартал')
-- Линия
	,('л', 'линия'),('лин', 'линия'),('лн', 'линия'),('л-я', 'линия')
-- Набережная
	,('н', 'набережная'),('н-ая', 'набережная'),('н-я', 'набережная')
-- Остров
	,('ост', 'остров'),('остр', 'остров'),('о-в', 'остров')
-- Парк
	,('пар', 'парк'),('п-рк', 'парк')
-- Переулок
--  ,('пер', 'переулок')
	,('п', 'переулок'),('пер-к', 'переулок'),('п-к', 'переулок')
-- Площадь
--  ,('пл', 'площадь')
	,('пл-дь', 'площадь'),('п-дь', 'площадь')
-- Проезд
	,('пр-д', 'проезд')
-- Проспект
	,('п', 'проспект'),('пр', 'проспект'),('пр-т', 'проспект'),('п-т', 'проспект'),('прос', 'проспект'),('просп', 'проспект')
-- Сквер
	,('ск', 'сквер'),('скв', 'сквер'),('с-р', 'сквер'),('ск-р', 'сквер')
-- Улица
	,('у', 'улица'),('ули', 'улица'),('у-ца', 'улица'),('у-а', 'улица'),('ул-а', 'улица')
-- Шоссе
--  ,('ш', 'шоссе')
	,('шос', 'шоссе'),('ш-се', 'шоссе'),('ш-е', 'шоссе')
;


----------------------------------------------------------------
-- NB! use all function successively; they depend on one other.
-- it's for logging on UpdateFiasDBService
-- or you can create common function and call it.
----------------------------------------------------------------


-- Function to automatically construct and execute create statement for numbered tables which have no explicit hierarchy
-- For example FIAS (KLADR) FROM DBF exported as set of numbered tables like house01, house02... house99 next call:
-- SELECT create_full_view_FROM_numbered_tables('house')
-- will create view house_all which contain all rows, FROM that tables
-- Returns generated DDL query string just for info.
-- usage: SELECT replace_numbered_tables_by_full('house', 'STARTDATE < CURRENT_DATE AND CURRENT_DATE < ENDDATE')
CREATE OR REPLACE FUNCTION replace_numbered_tables_by_full(_tableprefix text, _whereclause text = null)
	RETURNS text LANGUAGE plpgsql VOLATILE AS
$func$
DECLARE
	_sql text;
BEGIN
	SELECT INTO _whereclause
		CASE WHEN _whereclause IS NOT NULL
			THEN ' WHERE ' || _whereclause
			ELSE ''
		END;


	SELECT INTO _sql
		'CREATE TABLE ' || _tableprefix || '_all AS
		SELECT * FROM ' || array_to_string(
			ARRAY_AGG(TABLE_NAME::text)
			, _whereclause || E'\nUNION ALL\nSELECT * FROM '
		) || _whereclause || E';\n\n' ||
		'DROP TABLE ' || array_to_string(
			ARRAY_AGG(TABLE_NAME::text), '; DROP TABLE '
		) || ';'
	FROM INFORMATION_SCHEMA.TABLES
	WHERE
		table_name SIMILAR TO _tableprefix || '\d+';
	EXECUTE _sql;
	RETURN _sql;
END
$func$;

CREATE OR REPLACE FUNCTION convert_refs_to_uuid_and_index() RETURNS text AS
$func$
DECLARE
	_sql text;
BEGIN
	SELECT INTO _sql
		STRING_AGG(
			format($$ALTER TABLE %1$I.%I ALTER COLUMN %I TYPE UUID USING NULLIF(%3$I, '')::UUID;$$, table_schema, table_name, column_name) || E'\n' ||
				format(E'CREATE INDEX %I__%I ON %I.%1$I(%2$I)\n', table_name, column_name, table_schema)
			,';'
		)
	--	,'CREATE INDEX ' || table_name || '__' || column_name
	FROM INFORMATION_SCHEMA.columns
	WHERE
		table_schema = 'public'
		AND column_name ~* '\wid'
		AND table_name !~* '^(s_|fe_)'
		AND data_type = 'character varying';

	EXECUTE _sql;
	RETURN _sql;
END
$func$
LANGUAGE 'plpgsql';

-- After loading source DBF incoming from fias.nalog.ru we want do some transformations like type changes, table merges and indexes creation
CREATE OR REPLACE FUNCTION preprocess_imported_db() RETURNS void AS
$$
BEGIN
	PERFORM replace_numbered_tables_by_full('house', 'STARTDATE < CURRENT_DATE AND CURRENT_DATE < ENDDATE');

	PERFORM replace_numbered_tables_by_full('addrob', 'actstatus = 1');

	PERFORM convert_refs_to_uuid_and_index();

	ALTER TABLE socrbase ALTER COLUMN level TYPE NUMERIC USING level::numeric;
	CREATE INDEX idx_socrbase_cover ON socrbase(level, scname, socrname);

	CREATE INDEX houseint_startdate ON houseint (startdate);
	CREATE INDEX houseint_enddate ON houseint (enddate);

	PERFORM fill_synonyms_and_common_mistakes();

	ANALYZE;
END
$$
LANGUAGE 'plpgsql';