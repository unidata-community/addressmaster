-- After loading source DBF incoming from fias.nalog.ru we want do some transformations like type changes, table merges and indexes creation
CREATE OR REPLACE FUNCTION preprocess_imported_db() RETURNS void AS
$$
BEGIN
	PERFORM replace_numbered_tables_by_full('house', 'STARTDATE < CURRENT_DATE AND CURRENT_DATE < ENDDATE');

	PERFORM replace_numbered_tables_by_full('addrob', 'actstatus = 1');

	PERFORM convert_refs_to_uuid_and_index();

	ALTER TABLE socrbase ALTER COLUMN level TYPE NUMERIC USING level::numeric;
	CREATE INDEX idx_socrbase_cover ON socrbase(level, scname, socrname);

	PERFORM fill_synonyms_and_common_mistakes();

	-- There type `Чувашия`!!!
	-- "https://fias.nalog.ru/Docs/Сведения о составе информации ФИАС до 09062016.doc" (официальная дока по ФИАС), цитата:
	-- "Таблица  SOCRBASE (AddressObjectType) – содержит перечень полных, сокращённых наименований типов адресных элементов и уровней их классификации."
	--
	-- Содержимое (выложено кем-то, проверил, а архивах также): https://gist.github.com/nalgeon/913b0dd9e2dc87a29d86c0f92b3bd1b6#file-socrbase-csv-L15:
	-- 1 	Чувашия 	Чувашия 	108
	--
	-- So we hack it to normal "Республика"
	-- (Single record to update)
	UPDATE addrob_all
	SET
		shortname = 'Респ' -- Or 'респ.'?? In socrbase present both, but all values in `addrob_all` refer only `Респ`
		,formalname = 'Чувашская'
	WHERE shortname = 'Чувашия' AND formalname = 'Чувашская Республика -';

	ANALYZE;
END
$$
LANGUAGE 'plpgsql';