/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.db;

import java.util.Properties;
import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Pavel Alexeev.
 * @since 2015-12-28 15:23.
 */
public class FlywayUpdater {
    private static final Logger log = LoggerFactory.getLogger(FlywayUpdater.class);

    public final static String MASTER_MIGRATIONS_PATH = "classpath:db.master";
    public final static String FIAS_MIGRATIONS_PATH = "classpath:db.store";

    public static String updateFias(DataSource dataSource) {
        return update(dataSource, FIAS_MIGRATIONS_PATH);
    }

    public static String updateMaster(DataSource dataSource) {
        return update(dataSource, MASTER_MIGRATIONS_PATH);
    }

    /**
     * Main static method which updates provided datasource from set of migrations
     *
     * @param dataSource Target of update. Should be called for each sources we are manage
     * @param migrationsPath Resource (directory or classpath) of migrations to update from
     * @return target version of database after update
     */
    public static String update(DataSource dataSource, String migrationsPath) {
        Properties props = new Properties();
        props.put("flyway.outOfOrder", "true");
        Flyway flyway = new Flyway();
        flyway.configure(props);

        flyway.setDataSource(dataSource);
        flyway.setLocations(migrationsPath);

        try {
            flyway.migrate();
        } catch (FlywayException e) {
            log.warn("Exception happened on flywayUpdate. Trying perform repair and then update again");
            flyway.repair();
            flyway.migrate();
        }
        return flyway.info().current().getVersion().getVersion();
    }
}
