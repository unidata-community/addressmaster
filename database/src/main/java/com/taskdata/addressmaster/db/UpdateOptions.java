/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.db;

import com.beust.jcommander.DynamicParameter;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Pavel Alexeev.
 * @since 2015-12-28 19:46.
 */
public class UpdateOptions {
    public static final String DEFAULT_PROPERTIES_FILE = "gradle.properties";

    @DynamicParameter(names = {"-f", "--fly", "--flyway"}, description = "Flyway options. See options in http://flywaydb.org/documentation/maven/\n" +
        "\tF.e.:\n" +
        "\t\t-f flyway.url=jdbc:postgresql://localhost:5432/db --fly flyway.user=myUser --flyway flyway.password=superPass\n" +
        "\t\tYou may also just provide filename like: @" + DEFAULT_PROPERTIES_FILE + " then such options will be read from there (for backward capability also databaseUrl, databaseUser and databasePassword supported).")
    public Map<String, String> parameters = new HashMap<>();

    @Parameter(description = "Help", hidden = true)
    private List<String> props = new ArrayList<>();

    @Parameter(names = {"-h", "--help"}, description = "Help", help = true)
    public boolean help = false;

    /**
     * For backward capability accept and convert next keys also:
     * databaseUrl as flyway.url
     * databaseUser as flyway.user
     * databasePassword as flyway.password
     */
    protected void backwardCompatibilitySupport() {
        if (parameters.isEmpty() && !props.isEmpty()) { // Direct read from properties file
            props.forEach(it-> {
                    String[] s = it.split("\\s*=\\s*");
                    parameters.put(s[0], s[1]);
                }
            );
        }

        if (parameters.containsKey("databaseUrl") && !parameters.containsKey("flyway.url"))
            parameters.put("flyway.url", parameters.get("databaseUrl"));
        if (parameters.containsKey("databaseUser") && !parameters.containsKey("flyway.user"))
            parameters.put("flyway.user", parameters.get("databaseUser"));
        if (parameters.containsKey("databasePassword") && !parameters.containsKey("flyway.password"))
            parameters.put("flyway.password", parameters.get("databasePassword"));

        validate();
    }

    /**
     * Unfortunately there no interface to check properties set presence in DynamicParameters map
     */
    private void validate() {
        if (!parameters.containsKey("flyway.url") || !parameters.containsKey("flyway.user") || !parameters.containsKey("flyway.password"))
            throw new IllegalArgumentException("At least flyway.url (databaseUrl), flyway.user (databaseUser) and flyway.password (databasePassword) MUST be provided");
    }

    public static Map<String, String> parse(String[] args){
        if (0 == args.length){
            System.out.println("You have invoke me without arguments (see --help), trying loading settings from ./" + DEFAULT_PROPERTIES_FILE + " file for backward compatibility");
            args = new String[]{"@" + DEFAULT_PROPERTIES_FILE};
        }

        UpdateOptions options = new UpdateOptions();
        JCommander jCommander = new JCommander(options, args);
        if (options.help) {
            jCommander.usage();
            System.exit(0);
        }
        options.backwardCompatibilitySupport();

        return options.parameters;
    }
}
