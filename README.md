# addressmaster.ru

Project to easy perform search and validate tasks on [FIAS DB](https://fias.nalog.ru).
Have easy WEB interface and REST API for addresses checking.

Use Lucene to perform fast searches.
Allow background update FIAS database and atomicity switch to new version.

You may see product in action on: http://addressmaster.ru/

# Running

For run application you may just download [docker-compose.yml](packages/container/docker-compose.yml) and run it like:

    podman-compose up -d

> You may use `docker` and `docker-compose` respectively on systems where `podman` is not available.

Then just go to http://localhost:8080/ in a browser.

> **WARNING** To start really using application you need fill FIAS database at least once! Please follow instructions in [REST-API.md]() file.

> **NOTE** To start some different version it may be changed in [docker-compose.yml](). See comments in the file about ways to find available versions.

# Development

## Development requirements

1. java 1.8+
2. Postgres
3. [pgdbf](https://github.com/kstrauser/pgdbf) - temporary, look at [issue#2](https://gitlab.com/unidata-community/addressmaster/-/issues/2)
4. Recommended utilities are: `wget`, `zip`, `podman` (`docker`)

## Documentation

Incoming DB files structure and format documentation (in Russian) you may obtain from cite https://fias.nalog.ru/Updates.aspx => "Сведения о составе информации Федеральной информационной адресной системы"

## Build war

    ./gradlew war

> *Warning* see below recommended way to build docker containers for production use.

## Environment preparation
You need at least 3 Postgres databases. As most easy and convenient way you may run in `podman` (`docker`):

    podman run -d -p 127.0.0.2:5430:5432 -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=addressmaster --name addressmaster-db-master docker.io/postgres:13
    podman run -d -p 127.0.0.2:5431:5432 -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=fias1 --name addressmaster-db-fias1 docker.io/postgres:13
    podman run -d -p 127.0.0.2:5432:5432 -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=fias2 --name addressmaster-db-fias2 docker.io/postgres:13

> **Note** It is highly recommended add to [hosts](https://ru.wikipedia.org/wiki/Hosts) record for local domains to do not change settings of application later:
> It should look like:
>
>     127.0.0.2  addressmaster-db-fias1 addressmaster-db-fias2 addressmaster-db-master
>
> On windows and Mac however you need provide here IP address from output of `docker-machine ip` command (192.168.99.100 by default) instead of 127.0.0.2

## Run and debug

    ./gradlew farmRunDebug

Application will be run and ready to debug. You may connect remotely to port 5005.

If you want run without debug just hit:

    ./gradlew farmRun

## Configuration

For all configuration parameters there are present default values. If you have change some of them you may choose different methods:

### Recommended method - use environment variables

For any spring parameter, used in application you may deduct env variable by conventions [SystemEnvironmentPropertySource](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/core/env/SystemEnvironmentPropertySource.html)

F.e.:

    DATA_DIRECTORY=/data/addressmaster ../gradlew appRunDebug

will set path for `Lucene` data index (logic of updates provided below)

### Legacy configuration method - tomcat context
We generally do not recommend run application directly without a container (like `docker`, `podman`, `kubernetes`). Meantime sometimes approach helpful, especially for development purposes.
When application run on `tomcat` you may use [standard context configuration patterns](https://tomcat.apache.org/tomcat-8.0-doc/config/context).

Example of file `catalina_conf/context.xml.default` provided. In that example also provided list of most available configuration parameters with explanations.

# DB and updates from FIAS
## Logic

Application use 3 databases:
1. `master` - meta-DB. Contains users, update dates, configurations, etc. Have very few tables and very small. Mandatory to operate.
   - Flyway migrations for that placed in [database/db/master]() directory.
2. 2 database with FIAS address information. Same structure. At any time only one used. Second maybe updated for the new version of FIAS data without service interruption. After update Single REST call will switch the used DB.
    - Such logic used because this update is very long by nature (may took 3-8 hours) and not always ended successfully
    - Flyway migrations placed in [database/db/store]()

REST API calls and update examples see in [REST-API.md]() file

## DataBase tuning

For speedup query execution it is highly recommended to slightly tune Postgres from default values. Recommended start values may be like:

    ALTER SYSTEM SET work_mem = '2GB';
    ALTER SYSTEM SET maintenance_work_mem = '4GB';
    ALTER SYSTEM SET shared_buffers = '4GB';
    ALTER SYSTEM SET checkpoint_completion_target = 0.95;
    ALTER SYSTEM SET default_statistics_target = 10000;
    ALTER SYSTEM SET effective_cache_size = '10GB';

Some settings change requires a restart.

# Licensed under GPLv3

You may find text of license in file [LICENSE.GPLv3]()
