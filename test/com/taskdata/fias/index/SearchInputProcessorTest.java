/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.fias.index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.taskdata.addressmaster.common.codecs.Encoder;
import com.taskdata.addressmaster.server.dao.AddressObjectExDao;
import com.taskdata.addressmaster.server.dao.AddressObjectTypeExDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/resources/war/WEB-INF/applicationContext.xml")
public class SearchInputProcessorTest {

    private SearchInputProcessor searchInputProcessor = null;

    @Autowired
    private AddressObjectTypeExDao aoteDao;

    @Autowired
    private AddressObjectExDao aoeDao;

    @Before
    public void setUp() {
        searchInputProcessor = new SearchInputProcessorImpl(
            new SynonymServiceImpl(aoeDao.getSynonyms()),
            new AOTypeServiceImpl(aoteDao.getAlternativeToFormalTypeNames()),
            SearchInputProcessor.HouseNumberParseMode.LEAVE_IN_ADDRESS_STRING
        );
    }

    @Test
    public void testExtractAOAndTypes() {
        testAddress("Спб Лесной пр. 3", Collections.singletonList("Проспект"),
            Collections.singletonList(Encoder.CUSTOM_RUSSIAN_ENCODER.encodeString("Санкт-Петербург")), "Санкт-Петербург Лесной 3", null);
        testAddress("Спб Лесной проспект д. 3", Collections.singletonList("Проспект"),
            Collections.singletonList(Encoder.CUSTOM_RUSSIAN_ENCODER.encodeString("Санкт-Петербург")), "Санкт-Петербург Лесной 3", null);
        testAddress("мсК Лесной БУл 3", Collections.singletonList("Бульвар"),
            Collections.singletonList(Encoder.CUSTOM_RUSSIAN_ENCODER.encodeString("Москва")), "Москва Лесной 3", null);
        testAddress("Петербург 1-я Советская 4", new ArrayList<String>(),
            Collections.singletonList(Encoder.CUSTOM_RUSSIAN_ENCODER.encodeString("Санкт-Петербург")), "Санкт-Петербург 1-я Советская 4", null);
        testAddress("Санкт-Петербург, Василеостровский район, Средний проспект В.О. д. 88", new ArrayList<String>(),
            new ArrayList<String>(), "Санкт-Петербург, Василеостровский Средний В.О. 88", null);
    }

    @Test
    public void testExtractZip() {
        testAddress("Спб Лесной пр. 33А", Collections.singletonList("Проспект"),
            Collections.singletonList(Encoder.CUSTOM_RUSSIAN_ENCODER.encodeString("Санкт-Петербург")), "Санкт-Петербург Лесной 33А", null);
        testAddress("123456 Спб Лесной пр. 33А", Collections.singletonList("Проспект"),
            Collections.singletonList(Encoder.CUSTOM_RUSSIAN_ENCODER.encodeString("Санкт-Петербург")), "Санкт-Петербург Лесной 33А", "123456");
    }

    private SearchInput testAddress(String address, List<String> types, List<String> foundAbs, String resultInputString, String zipCode) {
        MatchContext matchContext = new MatchContext();
        SearchInput searchInput = searchInputProcessor.handleInput(address, matchContext);
        Assert.assertNotNull(searchInput);
        Set<String> addressObjectTypes = searchInput.getAddressObjectTypes();
        Assert.assertNotNull(addressObjectTypes);
        for (String type : types) {
            Assert.assertTrue("Type " + type + " is not found for " + searchInput.getInputStringWithoutZip(), addressObjectTypes.contains(type));
        }

        List<String> aos = searchInput.getMetaphonedTokens();
        for (String abb : foundAbs) {
            Assert.assertTrue("Abbreviate " + abb + " is not found for " + searchInput.getInputStringWithoutZip(), aos.contains(abb));
        }

        Assert.assertEquals(resultInputString, searchInput.getInputStringWithoutZip());
        if (zipCode == null) {
            Assert.assertNull(searchInput.getZipCode());
        } else {
            Assert.assertEquals(zipCode, searchInput.getZipCode());
        }
        return searchInput;
    }
}
