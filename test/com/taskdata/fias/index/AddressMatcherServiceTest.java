/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.fias.index;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Collection;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.taskdata.addressmaster.server.search.impl.MatchContext;
import com.taskdata.addressmaster.server.search.impl.MatchSettings;
import com.taskdata.addressmaster.server.search.model.SearchItem;
import com.taskdata.addressmaster.server.search.model.SearchItemContainer;
import com.taskdata.addressmaster.server.service.AddressMatcherService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/resources/war/WEB-INF/spring-ws-servlet.xml")
public class AddressMatcherServiceTest {

    private static final File SCHEMA = new File("test/com/taskdata/fias/index/AddressTestDataSchema.xsd");

    private static final File BASIC_TESTS = new File("test/com/taskdata/fias/index/BasicAddresses.xml");

    @Autowired
    private AddressMatcherService service;

    @Test
    public void testBasicAddresses() throws Exception {
        testDataFromFile(BASIC_TESTS);
    }

    void testDataFromFile(File testDataFile) throws Exception, java.io.FileNotFoundException, org.xml.sax.SAXException, javax.xml.bind.JAXBException {
        InputStream in = new BufferedInputStream(new FileInputStream(testDataFile));
        String packageName = AddressTestDataGroup.class.getPackage().getName();
        JAXBContext jc = JAXBContext.newInstance(packageName);
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(SCHEMA);
        Unmarshaller u = jc.createUnmarshaller();
        u.setSchema(schema);
        u.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
        AddressTestDataGroup group = (AddressTestDataGroup) u.unmarshal(in);
        Assert.assertNotNull(group);

        for (AddressTestDataItem item : group.getAddressTestDataItems()) {
            String errorPrefix = "Group: " + group.getName() + ". Failed address id = " + item.getId();
            MatchContext matchContext = new MatchContext(MatchSettings.DEFAULT);
            SearchItemContainer container = service.match(item.getRequest(), matchContext);
            Assert.assertNotNull(errorPrefix, container);
            Collection<SearchItem> sortedSearchItems = container.getSearchItems();
            Assert.assertTrue(errorPrefix, sortedSearchItems.size() > 0);
            int position = 1;
            boolean addressFound = false;
            for (SearchItem searchItem : sortedSearchItems) {
                if (item.getResult().equals(searchItem.getAddressInfo().getFullAddress())) {
                    Assert.assertTrue(errorPrefix + ' ' + searchItem.getAggregatedDistance() + ", expected less or equal to "
                            + item.getDistance(),
                        searchItem.getAggregatedDistance() <= item.getDistance());
                    addressFound = true;
                    break;
                }
                Assert.assertTrue(errorPrefix + " lowest position reached trying to find the address. Detected position: " + position, item.getLowestPosition() >= position);
                position++;
            }
            Assert.assertTrue(errorPrefix + " address is not found!", addressFound);
        }
    }

}
