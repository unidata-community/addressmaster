/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.fias.index;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.taskdata.addressmaster.common.codecs.CodecsSuite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    SearchInputProcessorTest.class,
    AddressMatcherServiceTest.class,
    CodecsSuite.class
})
public class IndexSuite {
}
