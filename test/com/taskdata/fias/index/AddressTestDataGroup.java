/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.fias.index;

import java.util.List;

@XmlRootElement(name = "AddressTestDataGroup", namespace = "http://www.taskdata.com/fias/schemas/AddressMatcherTest")
@XmlAccessorType(XmlAccessType.FIELD)
public class AddressTestDataGroup {

    @XmlAttribute(required = true)
    private String name;

    @XmlElement(name = "AddressTestDataItem")
    private List<AddressTestDataItem> addressTestDataItems;

    public List<AddressTestDataItem> getAddressTestDataItems() {
        return addressTestDataItems;
    }

    public void setAddressTestDataItems(List<AddressTestDataItem> addressTestDataItems) {
        this.addressTestDataItems = addressTestDataItems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
