/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.fias.index;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressTestDataItem")
public class AddressTestDataItem {

    @XmlAttribute(required = true)
    private String id;

    @XmlAttribute(required = true)
    private String request;

    @XmlAttribute(required = true)
    private String result;

    @XmlAttribute(required = true)
    private int distance;

    @XmlAttribute(required = true)
    private int lowestPosition;

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLowestPosition() {
        return lowestPosition;
    }

    public void setLowestPosition(int lowestPosition) {
        this.lowestPosition = lowestPosition;
    }
}
