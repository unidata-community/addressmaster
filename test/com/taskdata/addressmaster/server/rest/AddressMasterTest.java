/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.rest;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.taskdata.addressmaster.common.JSONConstants;
import com.taskdata.addressmaster.server.search.model.HouseNumberObject;
import com.taskdata.addressmaster.server.search.model.MatchStatus;
import com.taskdata.addressmaster.server.search.model.SearchResultItem;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/resources/war/WEB-INF/spring-ws-servlet.xml")
public class AddressMasterTest {

    @Autowired
    private AddressMaster addressMaster;

    @Test
    public void testRestAddressmatch() throws Exception {
        int numRecordsInResponse = 5;
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRemoteAddr("somehost");
        String restResponse = addressMaster.addressmatch("средний во 88а", String.valueOf(numRecordsInResponse), null, request);

        List<SearchResultItem> searchResultItems = readSearchResultItemsFromJson(restResponse);
        Assert.assertFalse(searchResultItems.isEmpty());
        Assert.assertEquals(numRecordsInResponse, searchResultItems.size());
        SearchResultItem searchResultItem = searchResultItems.get(0);

        Assert.assertEquals("41528f88-b43f-4b84-bb9b-5001e24f33b0", searchResultItem.getSearchResultItemGuid());
        Assert.assertEquals("a504032a-7239-439d-82bc-59e8c9e3da0d", searchResultItem.getHouseInfo().getHouseGuid());
    }

    @Test
    public void testRestAddressmatchWithMatchTypeFilter() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRemoteAddr("somehost");
        String restResponse = addressMaster.addressmatch("Владимирская область, с.Весь ул Садовая д29", null, MatchStatus.GOOD, request);

        List<SearchResultItem> searchResultItems = readSearchResultItemsFromJson(restResponse);
        Assert.assertFalse(searchResultItems.isEmpty());
        Assert.assertEquals(1, searchResultItems.size());

        SearchResultItem searchResultItem = searchResultItems.get(0);
        Assert.assertEquals("e6a75520-62f1-4ac4-95d8-9b3f9f8291db", searchResultItem.getSearchResultItemGuid());
    }

    @Test
    public void testRestHouseslist() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRemoteAddr("somehost");


        List<HouseNumberObject> houses = getHouses("СПб Средний ВО", request);
        Assert.assertEquals(233, houses.size());

        houses = getHouses("Красноярск Зеленогорск Полярная", request);
        Assert.assertEquals(34, houses.size());

        houses = getHouses("Волгоград ул. им. Достоевского", request);
        Assert.assertEquals(51, houses.size());
    }

    private static List<SearchResultItem> readSearchResultItemsFromJson(String jsonString) {
        List<SearchResultItem> res = new ArrayList<>();
        JSONObject jsonAddressObject = (JSONObject) JSONValue.parse(jsonString);
        JSONArray jsonAddressList = ((JSONArray) jsonAddressObject.get(JSONConstants.JSON_ADDRESSES_KEY));
        for (Object o : jsonAddressList) {
            JSONObject jsonAddress = (JSONObject) o;
            SearchResultItem searchResultItem = new SearchResultItem(jsonAddress);
            res.add(searchResultItem);
        }
        return res;
    }

    private static List<HouseNumberObject> readHouseNumberObjectsFromJson(String jsonString) {
        List<HouseNumberObject> res = new ArrayList<>();
        JSONObject jsonHousesObject = (JSONObject) JSONValue.parse(jsonString);
        JSONArray jsonHousesList = ((JSONArray) jsonHousesObject.get(JSONConstants.JSON_HOUSES_KEY));
        for (Object JSONObject : jsonHousesList) {
            JSONObject houseJSONObject = (JSONObject) JSONObject;
            HouseNumberObject houseNumberObject = new HouseNumberObject(houseJSONObject);
            res.add(houseNumberObject);
        }
        return res;
    }

    private List<HouseNumberObject> getHouses(String address, MockHttpServletRequest request) throws Exception {
        String addressSearchRestResponse = addressMaster.addressmatch(address, "1", null, request);
        List<SearchResultItem> searchResultItems = readSearchResultItemsFromJson(addressSearchRestResponse);
        SearchResultItem searchResultItem = searchResultItems.get(0);
        String searchResultItemId = searchResultItem.getSearchResultItemId();
        String restResponse = addressMaster.houseslist(searchResultItemId, request);
        List<HouseNumberObject> housesList = readHouseNumberObjectsFromJson(restResponse);
        return housesList;
    }
}
