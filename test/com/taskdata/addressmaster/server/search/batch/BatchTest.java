/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.search.batch;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import junit.framework.Assert;
import org.apache.commons.lang.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.taskdata.addressmaster.common.util.FiasProperties;
import com.taskdata.addressmaster.server.search.batch.model.CsvLine;
import com.taskdata.addressmaster.server.search.batch.model.DataLine;
import com.taskdata.addressmaster.server.search.batch.model.HeaderLine;
import com.taskdata.addressmaster.server.search.batch.reader.BatchReader;
import com.taskdata.addressmaster.server.search.impl.MatchContext;
import com.taskdata.addressmaster.server.search.impl.MatchSettings;
import com.taskdata.addressmaster.server.search.model.HouseMatchType;
import com.taskdata.addressmaster.server.search.model.HouseNumberObject;
import com.taskdata.addressmaster.server.search.model.HouseRangeObject;
import com.taskdata.addressmaster.server.search.model.MatchStatus;
import com.taskdata.addressmaster.server.search.model.SearchItem;
import com.taskdata.addressmaster.server.search.model.SearchItemContainer;
import com.taskdata.addressmaster.server.search.model.Structure;
import com.taskdata.addressmaster.server.service.AddressMatcherService;
import com.taskdata.addressmaster.server.util.Log4jManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/resources/war/WEB-INF/spring-ws-servlet.xml")
public class BatchTest {

    private static final String FILE_ENCODING = "UTF8";

    private static final String TEST_INPUT_FILE_NAME = "test-data.csv";
    private static final String TEST_OUTPUT_FILE_NAME = "test-data-out.csv";

    private static final String DISPLAY_ADDRESS_STRING = "DISPLAY_ADDRESS_STRING";
    private static final String FIAS_GUID = "FIAS_GUID";
    private static final String FIAS_HOUSE_GUID = "FIAS_HOUSE_GUID";
    private static final String FIAS_HOUSE_RANGE_GUID = "FIAS_HOUSE_RANGE_GUID";
    private static final String EXPECTED = "EXPECTED";

    @Autowired
    private AddressMatcherService service;

    @BeforeClass
    public static void prepare() {
        Log4jManager.getInstance(); // Initialize logging subsystem
    }

    @Test
    public void testBatch() throws Exception {
        String batchInputFileDir = FiasProperties.getInstance().getBatchFileDir();
        String inputFilePath = batchInputFileDir + File.separator + TEST_INPUT_FILE_NAME;

        InputStream is = new FileInputStream(inputFilePath);
        Reader streamReader = new InputStreamReader(is, FILE_ENCODING);
        CSVReader csvReader = new CSVReader(streamReader, CsvLine.DEFAULT_DELIMITER);

        CSVWriter csvWriter = prepareWriter(batchInputFileDir);

        String[] strings = csvReader.readNext();

        MatchContext matchContext = new MatchContext(MatchSettings.DEFAULT_BATCH);
        String[] rowItems;
        HeaderLine headerLine = new HeaderLine(strings);
        while ((rowItems = csvReader.readNext()) != null) {
            if (rowItems.length != headerLine.getLineItems().length) {
                Assert.fail("ERROR constructWrongLengthMsg " + rowItems);
            }

            DataLine dataLine = new DataLine(rowItems, headerLine);
            String address4test = dataLine.getNamedLineItems().get(DISPLAY_ADDRESS_STRING);
            SearchItemContainer container = service.match(address4test, matchContext);

            Collection<SearchItem> searchItems = container.getSearchItems();

            Map<String, String> namedLineItems = dataLine.getNamedLineItems();
            String fiasGuid = namedLineItems.get(FIAS_GUID);
            String fiasHouseGuid = namedLineItems.get(FIAS_HOUSE_GUID);
            String fiasRangeGuid = namedLineItems.get(FIAS_HOUSE_RANGE_GUID);
            String expected = namedLineItems.get(EXPECTED);

            SearchItem searchItem = searchItems.iterator().next();
            if (expected.equals(ExpectedResult.PASS.toString())) {
                String aoLeafGuid = searchItem.getAoLeafGuid();
                String fullAddress = searchItem.getAddressInfo().getFullAddress();
                MatchStatus matchStatus = searchItem.getMatchStatus();
                int matchScore = searchItem.getMatchScore();
                float luceneScore = searchItem.getLuceneScore();

                HouseNumberObject houseInfo = searchItem.getHouseInfo();
                HouseMatchType houseMatchType = null;
                String houseNumber = null;
                String buildingNumber = null;
                String structureStatusDesc = null;
                String structureNum = null;

                if (houseInfo != null) {
                    houseMatchType = houseInfo.getMatchType();
                    houseNumber = houseInfo.getHouseNumber();
                    buildingNumber = houseInfo.getBuildingNumber();
                    Structure structure = houseInfo.getStructure();
                    if (structure != null) {
                        structureStatusDesc = structure.getStructureStatus().getShortDesc();
                        structureNum = structure.getStructureNum();
                    }
                }

                Assert.assertEquals(address4test + " fiasGuid:", fiasGuid, aoLeafGuid);
                boolean aoGuidsMatched = StringUtils.equals(fiasGuid, aoLeafGuid);

                String retrievedFiasHouseGuid = houseInfo != null ? houseInfo.getHouseGuid() : null;
                Assert.assertEquals(address4test + " fiasHouseGuid:", fiasHouseGuid, retrievedFiasHouseGuid);
                boolean houseGuidsMatched = StringUtils.equals(fiasHouseGuid, houseInfo != null ? houseInfo.getHouseGuid() : null);

                boolean houseRangeGuidsMatched = true;
                if (houseInfo != null) {
                    HouseRangeObject houseRangeInfo = houseInfo.getHouseRangeInfo();
                    Assert.assertEquals(address4test + " fiasRangeGuid:", fiasRangeGuid, houseRangeInfo != null ? houseRangeInfo.getHouseRangeGuid() : null);
                    houseRangeGuidsMatched = StringUtils.equals(fiasRangeGuid, houseRangeInfo != null ? houseRangeInfo.getHouseRangeGuid() : null);
                }

                boolean outcome = aoGuidsMatched && houseGuidsMatched && houseRangeGuidsMatched;
                dumpToFile(csvWriter,
                    outcome,
                    address4test, fullAddress,
                    houseNumber, buildingNumber, structureStatusDesc, structureNum, houseMatchType,
                    fiasGuid, aoLeafGuid,
                    fiasHouseGuid, retrievedFiasHouseGuid,
                    matchStatus, matchScore, luceneScore);
            }
        }
        csvReader.close();
        csvWriter.close();
    }

    private static CSVWriter prepareWriter(String batchInputFileDir) throws IOException {
        String outputFilePath = batchInputFileDir + File.separator + TEST_OUTPUT_FILE_NAME;
        File outputFile = new File(outputFilePath);
        if (!outputFile.exists()) {
            outputFile.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(outputFile, false);
        OutputStreamWriter streamWriter = new OutputStreamWriter(fos, BatchReader.FILE_ENCODING);
        Writer bufferedWriter = new BufferedWriter(streamWriter);
        CSVWriter csvWriter = new CSVWriter(bufferedWriter, CsvLine.DEFAULT_DELIMITER);
        csvWriter.writeNext(new String[]{"Outcome",
            "Input String", "Matched Address",
            "House", "Building", "Structure Type", "Structure", "HouseMatchType",
            //"Expected AO GUID", "Retrieved AO GUID",
            //"Expected House GUID", "Retrieved House GUID",
            "Match Status", "Match Score", "Lucene Score",});
        return csvWriter;
    }

    private static void dumpToFile(CSVWriter csvWriter,
                                   boolean outcome,
                                   String testAddress, String retrievedAddress,
                                   String houseNumber, String buildingNumber, String structureStatusDesc, String structureNum, HouseMatchType houseMatchType,
                                   String expectedFiasAOGuid, String retrievedFiasAOGuid,
                                   String expectedFiasHouseGuid, String retrievedFiasHouseGuid,
                                   MatchStatus matchStatus, int matchScore, float luceneScore) {
        String[] stringArray = {outcome ? "PASS" : "FAIL",
            testAddress, retrievedAddress,
            houseNumber, buildingNumber, structureStatusDesc, structureNum, houseMatchType != null ? houseMatchType.toString() : null,
            //expectedFiasAOGuid, retrievedFiasAOGuid,
            //expectedFiasHouseGuid, retrievedFiasHouseGuid,
            matchStatus.toString(), String.valueOf(matchScore), String.valueOf(luceneScore),
        };
        csvWriter.writeNext(stringArray);
    }

    static enum ExpectedResult {
        PASS, FAIL
    }
}
