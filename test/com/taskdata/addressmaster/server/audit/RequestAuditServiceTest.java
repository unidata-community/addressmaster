/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.dao.master.entity.audit;

import java.util.List;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.taskdata.addressmaster.server.event.RequestEvent;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/resources/war/WEB-INF/applicationContext.xml")
public class RequestAuditServiceTest {

    @Autowired
    private RequestAuditService requestAuditService;

    @Autowired
    private RequestAuditDao requestAuditDao;

    @Test
    public void testAuditRequest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRemoteAddr("qwa");
        String input = String.valueOf(System.currentTimeMillis());
        RequestEvent.Builder eventBuilder = new RequestEvent.Builder(request, RequestEvent.RequestEventType.AUTO_COMPLETE)
            .setUserInput(input)
            .setRows(0)
            .setEventMetric(0);
        requestAuditService.onNewRequest(eventBuilder.build());

        List<RequestAudit> res = requestAuditDao.query("select t from RequestAudit t where t.userInput = ?",
            new Object[]{input}, 100);
        Assert.assertNotNull(res);
        Assert.assertFalse(res.isEmpty());
        Assert.assertEquals(input, res.get(0).getUserInput());
    }
}
