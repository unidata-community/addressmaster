/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server;

import junit.framework.Assert;
import org.junit.Test;

import com.taskdata.addressmaster.common.util.StringUtil;

public class StringUtilTest {


    @Test
    public void testIsEmpty() {
        Assert.assertTrue(StringUtil.isEmpty(null));
        Assert.assertTrue(StringUtil.isEmpty(""));
        Assert.assertFalse(StringUtil.isEmpty("n"));
        Assert.assertFalse(StringUtil.isEmpty("not_empty"));
    }

    @Test
    public void testIsNormalized() {
        Assert.assertTrue(StringUtil.isNormalized("String"));
        Assert.assertFalse(StringUtil.isNormalized(" "));
        Assert.assertTrue(StringUtil.isNormalized("Строка"));
        Assert.assertFalse(StringUtil.isNormalized("string"));
        Assert.assertFalse(StringUtil.isNormalized("строка"));
    }
}
