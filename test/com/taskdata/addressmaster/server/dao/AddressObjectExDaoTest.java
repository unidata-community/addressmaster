/*
 * Addressmaster
 *
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Addressmaster software.
 *
 * Addressmaster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Addressmaster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


package com.taskdata.addressmaster.server.dao;

import java.util.Map;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/resources/war/WEB-INF/applicationContext.xml")
public class AddressObjectExDaoTest {

    @Autowired
    private AddressObjectDao aotDao;

    @Autowired
    private AddressObjectExDao dao;

    @Test
    public void testGetExtensionsMap() {
        Map<String, String> map = dao.getSynonyms();
        Assert.assertNotNull(map);

        Assert.assertTrue(map.containsKey("спб"));
        Assert.assertTrue(map.containsKey("мск"));

//        List<AddressObject> aos = aotDao.query("SELECT t FROM AddressObject t", new String[] {}, 10000);
//        for (AddressObject ao : aos) {
//            if (ao.getId().equals("0")) {
//                continue;
//            }
//            Assert.assertTrue("Unable to find " + ao.getFormalName() + " in extensions", map.containsKey(ao.getFormalName()));
//            Assert.assertTrue("Unable to find " + ao.getShortName() + " in extensions", map.containsKey(ao.getShortName()));
//        }
    }
}
